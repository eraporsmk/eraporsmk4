$('a.confirm_remed').bind('click',function(e) {
	var ini = $(this).parents('tr');
	var itu = $(this).closest('tr');
	e.preventDefault();
	var url = $(this).attr('href');
	swal({
		title: "Anda Yakin?",
		text: "Tindakan ini tidak bisa dikembalikan!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Hapus!",
		showLoaderOnConfirm: true,
		preConfirm: function() {
			return new Promise(function(resolve) {
				$.get(url).done(function(response) {
					var data = $.parseJSON(response);
					console.log(data);
					swal({title:data.title, html:data.text, type:data.type}).then(function() {
						if(data.type == 'success'){
							var nilai_asli = new Array();
							nilai_asli.push('<td>'+data.nama_siswa+'</td>');
							$.each(data.nilai, function (i, item) {
								nilai_asli.push('<td class="text-center"><input name="rerata['+item.siswa_id+'][]" size="10" class="'+item.class+' form-control input-sm" value="'+item.nilai+'" type="text"></td>');
							});
							console.log(nilai_asli);
							nilai_asli.push('<td id="rerata_akhir" class="text-center '+data.bg_rerata_akhir+'"><strong><input type="hidden" id="rerata_akhir_input" name="rerata_akhir[]" value="'+data.rerata_akhir+'" />'+data.rerata_akhir+'</strong></td>');
							nilai_asli.push('<td id="rerata_remedial" class="text-center '+data.bg_rerata_akhir+'"><strong><input type="hidden" id="rerata_remedial_input" name="rerata_remedial[]" value="'+data.rerata_akhir+'" />'+data.rerata_akhir+'</strong></td>');
							nilai_asli.push('<td class="text-center">-</td>');
							var nilai_jadi = '<tr>'+nilai_asli.join('#')+'</tr>';
							nilai_jadi = nilai_jadi.replace('#','');
							console.log(nilai_jadi);
							$(nilai_jadi).insertAfter(itu);
							//$('<tr><td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td></tr>').insertAfter(itu);
							ini.remove();
						}
					});
				})
			})
		}
	});
});
$('a.next_page').bind('click',function(e) {
	e.preventDefault();
	var url = $(this).attr('href');
	var aksi = $(this).data('aksi');
	//var ini = $(this).parents('ul');
	//var li = $(this).closest('li');
	//$(".pagination li").removeClass("active");
	//$(li).addClass('active');
	$('#result').html('');
	$.get(url, function(response) {
		var data = $.parseJSON(response);
		$.ajax({
			url: aksi,
			type: 'post',
			data: {ajaran_id:data.ajaran_id, rombel_id:data.rombel_id, id_mapel:data.id_mapel, kelas:data.kelas, aspek:data.aspek, start:data.start},
			success: function(response){
				//console.log(li);
				$('#result').html(response);
				//$(".pagination li").removeClass("active");
				//$(li).addClass('active');
			}
		});
	});
});