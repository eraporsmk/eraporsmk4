<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Cetak extends Backend_Controller {
	var $B;
	var $I;
	var $U;
	var $HREF; 
	public function __construct(){
		parent::__construct();
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit', '-1'); 
	}
	public function rapor_debug_top_all($kur,$ajaran_id,$rombel_id){
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['kurikulum_id'] = $kur;
		$data_siswa = get_siswa_by_rombel($rombel_id);
		$this->load->view('backend/cetak/rapor_header', $data);
		foreach($data_siswa as $siswa){
			$data['siswa_id'] = $siswa->id;
        	$this->load->view('backend/cetak/rapor_cover', $data);
			$this->load->view('backend/cetak/rapor_identitas_sekolah', $data);
			$this->load->view('backend/cetak/rapor_identitas_siswa', $data);
		}
		$this->load->view('backend/cetak/rapor_footer', $data);
	}
	public function rapor_debug_nilai_all($kur,$ajaran_id,$rombel_id){
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['kurikulum_id'] = $kur;
		$data_siswa = get_siswa_by_rombel($rombel_id);
		$this->load->view('backend/cetak/rapor_header', $data);
		foreach($data_siswa as $siswa){
			$data['siswa_id'] = $siswa->id;
	       	$this->load->view('backend/cetak/rapor_sikap', $data);
			$this->load->view('backend/cetak/rapor_nilai', $data);
		}
		$this->load->view('backend/cetak/rapor_footer', $data);
	}
	public function rapor_debug_bottom_all($kur,$ajaran_id,$rombel_id){
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['kurikulum_id'] = $kur;
		$data_siswa = get_siswa_by_rombel($rombel_id);
		$this->load->view('backend/cetak/rapor_header', $data);
		foreach($data_siswa as $siswa){
			$data['siswa_id'] = $siswa->id;
	    	$this->load->view('backend/cetak/rapor_prakerin', $data);
			$this->load->view('backend/cetak/rapor_ekskul', $data);
			$this->load->view('backend/cetak/rapor_prestasi', $data);
			$this->load->view('backend/cetak/rapor_absen', $data);
			$this->load->view('backend/cetak/rapor_catatan_wali_kelas', $data);
		}
		$this->load->view('backend/cetak/rapor_footer', $data);
	}
	public function rapor_top_all($kur,$ajaran_id,$rombel_id){
        /*$this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['kurikulum_id'] = $kur;
		$rombel = Datarombel::find($rombel_id);
        $pdfFilePath = strtolower(str_replace(' ','_',$rombel->nama)).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		$rapor_header=$this->load->view('backend/cetak/rapor_header', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_header);
		$data_siswa = get_siswa_by_rombel($rombel_id);
		foreach($data_siswa as $siswa){
			$data['siswa_id'] = $siswa->id;
			$this->m_pdf->pdf->SetHTMLFooter('<b style="font-size:8px;"><i>'.$siswa->nama.' - '.$rombel->nama.'<i></b>');
			$rapor_cover=$this->load->view('backend/cetak/rapor_cover', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_cover);
			$this->m_pdf->pdf->AddPage('P');		
			$rapor_identitas_sekolah=$this->load->view('backend/cetak/rapor_identitas_sekolah', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_identitas_sekolah);
			$this->m_pdf->pdf->AddPage('P');
			$rapor_identitas_siswa=$this->load->view('backend/cetak/rapor_identitas_siswa', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_identitas_siswa);
			$this->m_pdf->pdf->AddPage('P');
		}
		$rapor_footer=$this->load->view('backend/cetak/rapor_footer', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_footer);
		//download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I');  */
		$data_rombel = $this->rombongan_belajar->get($rombel_id);
		$loggeduser = $this->ion_auth->user()->row();
		//$this->qr_code($kur,$ajaran_id,$rombel_id,$siswa_id);
        $this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		//$data['siswa_id'] = $siswa_id;
		$data['kurikulum_id'] = $kur;
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        $pdfFilePath = strtolower(str_replace(' ','_',get_nama_siswa($siswa_id))).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		$this->m_pdf->pdf->defaultfooterfontsize=7;
		$this->m_pdf->pdf->defaultfooterline=0;
		$rapor_header=$this->load->view('backend/cetak/rapor_header', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_header);
		$data_siswa = get_siswa_by_rombel($rombel_id);
		$i=0;
		foreach($data_siswa as $siswa){
			$this->m_pdf->pdf->SetFooter(get_nama_siswa($siswa->id).' - '.get_nama_rombel($rombel_id).'|{PAGENO}|Dicetak dari eRaporSMK v.'.get_version());
			$data['siswa_id'] = $siswa->id;
			if($i==0){
			$this->m_pdf->pdf->AddPage('P');
			}
			$rapor_cover=$this->load->view('backend/cetak/rapor_cover', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_cover);
			$this->m_pdf->pdf->AddPage('P');
			$rapor_identitas_sekolah=$this->load->view('backend/cetak/rapor_identitas_sekolah', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_identitas_sekolah);
			$this->m_pdf->pdf->AddPage('P');
			$rapor_identitas_siswa=$this->load->view('backend/cetak/rapor_identitas_siswa', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_identitas_siswa);
			$this->m_pdf->pdf->WriteHTML('<pagebreak resetpagenum="1" pagenumstyle="1" suppress="off" />');
			$i++;
		}
		$rapor_footer=$this->load->view('backend/cetak/rapor_footer', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_footer);
        //download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I');  
	}
	public function rapor_nilai_all($kur,$ajaran_id,$rombel_id){
        $this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['kurikulum_id'] = $kur;
		$rombel = Datarombel::find($rombel_id);
        $pdfFilePath = strtolower(str_replace(' ','_',$rombel->nama)).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		$css_files = array(
			'assets/bootstrap/css/bootstrap.min.css',
			'assets/css/cetak.css',
		);
		for($i = 0; $i < count($css_files); $i++){
			$stylesheet = file_get_contents($css_files[$i]);
			$this->m_pdf->pdf->WriteHTML($stylesheet,1); // 1 para indicar que es CSS
		}
		$data_siswa = get_siswa_by_rombel($rombel_id);
		foreach($data_siswa as $siswa){
			$data['siswa_id'] = $siswa->id;
			$this->m_pdf->pdf->AddPage('L'); // Adds a new page in Landscape orientation
			$this->m_pdf->pdf->SetHTMLFooter('<b style="font-size:8px;"><i>'.$siswa->nama.' - '.$rombel->nama.'<i></b>');
			$rapor_sikap=$this->load->view('backend/cetak/rapor_sikap', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_sikap);
			$this->m_pdf->pdf->AddPage('L'); // Adds a new page in Landscape orientation
			$rapor_nilai=$this->load->view('backend/cetak/'.$kur.'/rapor_nilai', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_nilai);
		}
		$rapor_footer=$this->load->view('backend/cetak/rapor_footer', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_footer);
        //download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I');   
	}
	public function rapor_bottom_all($kur,$ajaran_id,$rombel_id){
		$this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['kurikulum_id'] = $kur;
		$rombel = Datarombel::find($rombel_id);
        $pdfFilePath = strtolower(str_replace(' ','_',$rombel->nama)).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		//$this->m_pdf->pdf->AddPage('L'); // Adds a new page in Landscape orientation
		//$rapor_header=$this->load->view('backend/cetak/rapor_header', $data, true);
        //$this->m_pdf->pdf->WriteHTML($rapor_header);
		$css_files = array(
			'assets/bootstrap/css/bootstrap.min.css',
			'assets/css/cetak.css',
		);
		for($i = 0; $i < count($css_files); $i++){
			$stylesheet = file_get_contents($css_files[$i]);
			$this->m_pdf->pdf->WriteHTML($stylesheet,1); // 1 para indicar que es CSS
		}
		$data_siswa = get_siswa_by_rombel($rombel_id);
		foreach($data_siswa as $siswa){
			$data['siswa_id'] = $siswa->id;
			$this->m_pdf->pdf->AddPage('L'); // Adds a new page in Landscape orientation
			$this->m_pdf->pdf->SetHTMLFooter('<b style="font-size:8px;"><i>'.$siswa->nama.' - '.$rombel->nama.'<i></b>');
			$rapor_prakerin=$this->load->view('backend/cetak/rapor_prakerin', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_prakerin);
			$rapor_ekskul=$this->load->view('backend/cetak/rapor_ekskul', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_ekskul);
			$rapor_prestasi=$this->load->view('backend/cetak/rapor_prestasi', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_prestasi);
			$rapor_absen=$this->load->view('backend/cetak/rapor_absen', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_absen);
			$rapor_catatan_wali_kelas=$this->load->view('backend/cetak/rapor_catatan_wali_kelas', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_catatan_wali_kelas);
		}
		$rapor_footer=$this->load->view('backend/cetak/rapor_footer', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_footer);
        //download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I'); 
	}
	public function rapor_top($kur,$ajaran_id,$rombel_id,$siswa_id){
		$data_rombel = $this->rombongan_belajar->get($rombel_id);
		$loggeduser = $this->ion_auth->user()->row();
		//$this->qr_code($kur,$ajaran_id,$rombel_id,$siswa_id);
        $this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum_id'] = $kur;
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        $pdfFilePath = strtolower(str_replace(' ','_',get_nama_siswa($siswa_id))).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		$this->m_pdf->pdf->defaultfooterfontsize=7;
		$this->m_pdf->pdf->defaultfooterline=0;
		//$this->m_pdf->pdf->SetFooter(get_nama_siswa($siswa_id).' - '.get_nama_rombel($rombel_id).'|{PAGENO}|Dicetak dari eRaporSMK v.'.get_version());
		$this->m_pdf->pdf->SetFooter(get_nama_siswa($siswa_id).' - '.get_nama_rombel($rombel_id).'||Dicetak dari eRaporSMK v.'.get_version());
		$rapor_header=$this->load->view('backend/cetak/rapor_header', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_header);
		$rapor_cover=$this->load->view('backend/cetak/rapor_cover', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_cover);
		$this->m_pdf->pdf->AddPage('P');
		$rapor_identitas_sekolah=$this->load->view('backend/cetak/rapor_identitas_sekolah', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_identitas_sekolah);
		$this->m_pdf->pdf->AddPage('P');
		$rapor_identitas_siswa=$this->load->view('backend/cetak/rapor_identitas_siswa', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_identitas_siswa);
		$rapor_footer=$this->load->view('backend/cetak/rapor_footer', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_footer);
        //download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I');   
	}
	public function rapor_pendukung($kur,$ajaran_id,$rombel_id,$siswa_id){
		$this->load->model('kenaikan_kelas_model', 'kenaikan_kelas');
		$check_2018 = check_2018();
		$data_rombel = $this->rombongan_belajar->get($rombel_id);
		$loggeduser = $this->ion_auth->user()->row();
		//$this->qr_code($kur,$ajaran_id,$rombel_id,$siswa_id);
        $this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum_id'] = $kur;
		$data['sekolah_id'] = $loggeduser->sekolah_id;
		$data['check_2018'] = $check_2018;
        $pdfFilePath = strtolower(str_replace(' ','-',get_nama_siswa($siswa_id).'-'.get_nama_rombel($rombel_id))).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		$this->m_pdf->pdf->defaultfooterfontsize=7;
		$this->m_pdf->pdf->defaultfooterline=0;
		$this->m_pdf->pdf->SetFooter(get_nama_siswa($siswa_id).' - '.get_nama_rombel($rombel_id).'||Dicetak dari eRaporSMK v.'.get_version());
		//$this->m_pdf->pdf->AddPage('P', '', 4);
		/*
		- Kelas X SMT 1 seperti sekarang (start form 4)
		- Kelas X, SMT 2 start dari 1
		- Kelas XI & XII, SMT 1 & 2 semua start dari 1
		*/
		$set_ajaran = $this->semester->get($ajaran_id);
		/*if($check_2018){
			$this->m_pdf->pdf->AddPage('P', '', 7);
		} else {
			if($tingkat == 10 && $set_ajaran->semester == 2 || $tingkat != 10){
				$this->m_pdf->pdf->AddPage('P');
			} else {
				$this->m_pdf->pdf->AddPage('P', '', 4);
			}
		}*/
		$this->m_pdf->pdf->AddPage('P');
		//$this->m_pdf->pdf->WriteHTML('<pagebreak type="NEXT-ODD" pagenumstyle="10" />');
		$rapor_header=$this->load->view('backend/cetak/rapor_header', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_header);
		if($check_2018){
			$rapor_keterangan_2018=$this->load->view('backend/cetak/rapor_keterangan_2018', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_keterangan_2018);
		} else {
			$rapor_keterangan=$this->load->view('backend/cetak/rapor_keterangan', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_keterangan);
		}
		$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		$rapor_keterangan_pindah_keluar=$this->load->view('backend/cetak/rapor_keterangan_pindah_keluar', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_keterangan_pindah_keluar);
		$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		$rapor_keterangan_pindah_masuk=$this->load->view('backend/cetak/rapor_keterangan_pindah_masuk', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_keterangan_pindah_masuk);
		$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		$rapor_catatan_prestasi=$this->load->view('backend/cetak/rapor_catatan_prestasi', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_catatan_prestasi);
		$rapor_footer=$this->load->view('backend/cetak/rapor_footer', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_footer);
        //download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I');   
	}
	public function rapor_pdf($kur,$ajaran_id,$rombel_id,$siswa_id){
		$this->load->model('kenaikan_kelas_model', 'kenaikan_kelas');
		$check_2018 = check_2018();
		$data_rombel = $this->rombongan_belajar->get($rombel_id);
		$loggeduser = $this->ion_auth->user()->row();
		//$this->qr_code($kur,$ajaran_id,$rombel_id,$siswa_id);
        $this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum_id'] = $kur;
		$data['sekolah_id'] = $loggeduser->sekolah_id;
		$data['check_2018'] = $check_2018;
        $pdfFilePath = strtolower(str_replace(' ','-',get_nama_siswa($siswa_id).'-'.get_nama_rombel($rombel_id))).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		$this->m_pdf->pdf->defaultfooterfontsize=7;
		$this->m_pdf->pdf->defaultfooterline=0;
		$this->m_pdf->pdf->SetFooter(get_nama_siswa($siswa_id).' - '.get_nama_rombel($rombel_id).'|{PAGENO}|Dicetak dari eRaporSMK v.'.get_version());
		//$this->m_pdf->pdf->AddPage('P', '', 4);
		/*
		- Kelas X SMT 1 seperti sekarang (start form 4)
		- Kelas X, SMT 2 start dari 1
		- Kelas XI & XII, SMT 1 & 2 semua start dari 1
		*/
		$set_ajaran = $this->semester->get($ajaran_id);
		/*if($check_2018){
			$this->m_pdf->pdf->AddPage('P', '', 4);
		} else {
			if($tingkat == 10 && $set_ajaran->semester == 2 || $tingkat != 10){
				$this->m_pdf->pdf->AddPage('P');
			} else {
				$this->m_pdf->pdf->AddPage('P', '', 4);
			}
		}*/
		$this->m_pdf->pdf->AddPage('P');
		$rapor_header=$this->load->view('backend/cetak/rapor_header', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_header);
		if(!$check_2018){
			$rapor_sikap=$this->load->view('backend/cetak/rapor_sikap', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_sikap);
			$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		} else {
			$rapor_sikap_2018=$this->load->view('backend/cetak/rapor_sikap_2018', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_sikap_2018);
		}
 		$rapor_nilai=$this->load->view('backend/cetak/'.$kur.'/rapor_nilai', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_nilai);
		if($check_2018){
			$rapor_catatan_akademik=$this->load->view('backend/cetak/rapor_catatan_akademik', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_catatan_akademik);
		}
		$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		//if($check_2018){
			//$rapor_sikap_2018=$this->load->view('backend/cetak/rapor_sikap_2018', $data, true);
			//$this->m_pdf->pdf->WriteHTML($rapor_sikap_2018);
		//}
		if($check_2018){
			$rapor_sikap_2018=$this->load->view('backend/cetak/rapor_sikap_2018', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_sikap_2018);
		}
		if($data_rombel->tingkat != 10){
			$rapor_prakerin=$this->load->view('backend/cetak/rapor_prakerin', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_prakerin);
		}
		$rapor_ekskul=$this->load->view('backend/cetak/rapor_ekskul', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_ekskul);
		if(!$check_2018){
			$rapor_prestasi=$this->load->view('backend/cetak/rapor_prestasi', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_prestasi);
		}
		$rapor_absen=$this->load->view('backend/cetak/rapor_absen', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_absen);
		if(!$check_2018){
			$rapor_catatan_wali_kelas=$this->load->view('backend/cetak/rapor_catatan_wali_kelas', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_catatan_wali_kelas);
		} else {
			$rapor_kenaikan_kelas=$this->load->view('backend/cetak/rapor_kenaikan_kelas', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_kenaikan_kelas);
		}
		$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		if($check_2018){
			$rapor_sikap_2018=$this->load->view('backend/cetak/rapor_sikap_2018', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_sikap_2018);
			$rapor_karakter=$this->load->view('backend/cetak/rapor_karakter', $data, true);
			$this->m_pdf->pdf->WriteHTML($rapor_karakter);
		}
		/*$rapor_keterangan=$this->load->view('backend/cetak/rapor_keterangan', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_keterangan);
		$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		$rapor_keterangan_pindah_keluar=$this->load->view('backend/cetak/rapor_keterangan_pindah_keluar', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_keterangan_pindah_keluar);
		$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		$rapor_keterangan_pindah_masuk=$this->load->view('backend/cetak/rapor_keterangan_pindah_masuk', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_keterangan_pindah_masuk);
		$this->m_pdf->pdf->AddPage('P'); // Adds a new page in Landscape orientation
		$rapor_catatan_prestasi=$this->load->view('backend/cetak/rapor_catatan_prestasi', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_catatan_prestasi);*/
		$rapor_footer=$this->load->view('backend/cetak/rapor_footer', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_footer);
        //download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I');   
	}
	public function rapor_debug($kur,$ajaran_id,$rombel_id,$siswa_id){
		$this->load->model('kenaikan_kelas_model', 'kenaikan_kelas');
		$check_2018 = check_2018();
		$loggeduser = $this->ion_auth->user()->row();
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum_id'] = $kur;
		$data['sekolah_id'] = $loggeduser->sekolah_id;
		$data['check_2018'] = $check_2018;
 		$this->load->view('backend/cetak/rapor_header', $data);
        $this->load->view('backend/cetak/rapor_sikap', $data);
		$this->load->view('backend/cetak/'.$kur.'/rapor_nilai', $data);
		$this->load->view('backend/cetak/rapor_prakerin', $data);
		$this->load->view('backend/cetak/rapor_ekskul', $data);
		$this->load->view('backend/cetak/rapor_prestasi', $data);
		$this->load->view('backend/cetak/rapor_absen', $data);
		$this->load->view('backend/cetak/rapor_catatan_wali_kelas', $data);
		if($check_2018){
			$this->load->view('backend/cetak/rapor_keterangan_2018', $data);
		} else {
			$this->load->view('backend/cetak/rapor_keterangan', $data);
		}
		$this->load->view('backend/cetak/rapor_keterangan_pindah_keluar', $data);
		$this->load->view('backend/cetak/rapor_keterangan_pindah_masuk', $data);
		$this->load->view('backend/cetak/rapor_catatan_prestasi', $data);
		$this->load->view('backend/cetak/rapor_footer', $data);
	}
	public function rapor_ppk($kur,$ajaran_id,$rombel_id,$siswa_id){
		$data_rombel = $this->rombongan_belajar->get($rombel_id);
		$loggeduser = $this->ion_auth->user()->row();
		$this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum_id'] = $kur;
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        $pdfFilePath = strtolower(str_replace(' ','_',get_nama_siswa($siswa_id))).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		$this->m_pdf->pdf->defaultfooterfontsize=7;
		$this->m_pdf->pdf->defaultfooterline=0;
		$this->m_pdf->pdf->SetFooter(get_nama_siswa($siswa_id).' - '.get_nama_rombel($rombel_id).'|{PAGENO}|Dicetak dari eRaporSMK v.'.get_version());
		//$this->m_pdf->pdf->AddPage('', '', 5);
		$this->m_pdf->pdf->AddPage('P');
		$rapor_header=$this->load->view('backend/cetak/rapor_header', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_header);
		$rapor_ppk=$this->load->view('backend/cetak/rapor_ppk', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_ppk);
		$rapor_footer=$this->load->view('backend/cetak/rapor_footer', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_footer);
        //download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I');   
	}
	public function cover_ppk($kur,$ajaran_id,$rombel_id,$siswa_id){
		$data_rombel = $this->rombongan_belajar->get($rombel_id);
		$loggeduser = $this->ion_auth->user()->row();
		//$this->qr_code($kur,$ajaran_id,$rombel_id,$siswa_id);
        $this->load->library('m_pdf');
		$data['ajaran_id'] = $ajaran_id;
		$data['rombel_id'] = $rombel_id;
		$data['siswa_id'] = $siswa_id;
		$data['kurikulum_id'] = $kur;
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        $pdfFilePath = strtolower(str_replace(' ','_',get_nama_siswa($siswa_id))).".pdf";
		$wm = base_url() . 'assets/img/logo.png';
		$this->m_pdf->pdf->SetWatermarkImage($wm);
		$this->m_pdf->pdf->showWatermarkImage = false;
		$this->m_pdf->pdf->defaultfooterfontsize=7;
		$this->m_pdf->pdf->defaultfooterline=0;
		$this->m_pdf->pdf->SetFooter(get_nama_siswa($siswa_id).' - '.get_nama_rombel($rombel_id).'|{PAGENO}|Dicetak dari eRaporSMK Bisa');
		$rapor_header=$this->load->view('backend/cetak/rapor_header', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_header);
		//new line start
		$rapor_cover=$this->load->view('backend/cetak/rapor_cover', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_cover);
		$this->m_pdf->pdf->AddPage('P');
		$rapor_identitas_sekolah=$this->load->view('backend/cetak/rapor_identitas_sekolah', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_identitas_sekolah);
		$this->m_pdf->pdf->AddPage('P');
		$rapor_identitas_siswa=$this->load->view('backend/cetak/rapor_identitas_siswa', $data, true);
        $this->m_pdf->pdf->WriteHTML($rapor_identitas_siswa);
		$this->m_pdf->pdf->AddPage();
		$rapor_cover_ppk=$this->load->view('backend/cetak/rapor_cover_ppk', $data, true);
		$this->m_pdf->pdf->WriteHTML($rapor_cover_ppk);
        //download it.
		//Output($file,'I') browser
		//Output($file,'F') simpan di server
		//Output($file,'S') Kirim ke email
		//Output($file,'D') Download
		$this->m_pdf->pdf->Output($pdfFilePath,'I');   
	}
	public function legger($ajaran_id,$rombel_id,$kompetensi_id){
		$check_2018 = check_2018();
		if($check_2018){
			//$this->load->library('excel');
			//$objPHPExcel = new PHPExcel();
			//$objPHPExcel->setActiveSheetIndex(0);
			$data['loggeduser'] = $this->ion_auth->user()->row();
			$data['nama_rombel'] = $this->rombongan_belajar->get($rombel_id);
			$data['data_siswa'] = get_siswa_by_rombel($rombel_id);
			$data['kompetensi_id'] = $kompetensi_id;
			$data['data_mapel'] = $this->pembelajaran->with('mata_pelajaran')->find_all(" semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id'", '*','kelompok_id ASC, no_urut ASC');
			$this->template->title('Administrator Panel')
			->set_layout($this->admin_tpl)
			->set('page_title', 'Cetak Legger')
			->build($this->admin_folder.'/cetak/legger_2018',$data);
			/*$loggeduser = $this->ion_auth->user()->row();
			$nama_rombel = $this->rombongan_belajar->get($rombel_id);
			$data_siswa = get_siswa_by_rombel($rombel_id);
			$data_mapel = $this->pembelajaran->with('mata_pelajaran')->find_all(" semester_id =  $ajaran_id AND  rombongan_belajar_id = '$rombel_id'", '*','kelompok_id ASC, no_urut ASC');
			$nama_kompetensi = 'PENGETAHUAN';
			if($kompetensi_id == 2){
				$nama_kompetensi = 'KETERAMPILAN';
			}
			$huruf = 'D';
			$objPHPExcel->getActiveSheet()->mergeCells('A1:B1');
			$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Nama Siswa');
			$objPHPExcel->getActiveSheet()->setCellValue('C1', 'SKM');
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			foreach($data_siswa['data'] as $siswa){
				$objPHPExcel->getActiveSheet()->setCellValue($huruf. 1, strtoupper($siswa->nama));
				$objPHPExcel->getActiveSheet()->getStyle($huruf. 1)->getAlignment()->setTextRotation(90);
				$objPHPExcel->getActiveSheet()->getColumnDimension($huruf)->setAutoSize(true);
				$huruf++;
			}
			$i=2;
			$a=3;
			foreach($data_mapel as $key=>$mapel){
				$this->db->select('kd_id, id_kompetensi');
				$this->db->from('kd_nilai as a');
				$this->db->join('rencana_penilaian as b', 'a.rencana_penilaian_id = b.rencana_penilaian_id');
				$this->db->where('b.sekolah_id', $loggeduser->sekolah_id);
				$this->db->where('b.semester_id', $mapel->semester_id);
				$this->db->where('b.mata_pelajaran_id', $mapel->mata_pelajaran_id);
				$this->db->where('b.rombongan_belajar_id', $mapel->rombongan_belajar_id);
				$this->db->where('b.kompetensi_id', $kompetensi_id);
				$this->db->order_by('kd_id', 'asc');
				$this->db->group_by('kd_id, id_kompetensi');
				$query = $this->db->get();
				$all_kd_nilai = $query->result();
				if($i>2){
					$i = $i + count($all_kd_nilai);
				}
				$objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':B'.$i);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $mapel->nama_mata_pelajaran);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, get_kkm($mapel->semester_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id));
				//$a= $i + 1;
				foreach($all_kd_nilai as $kd_nilai){
					$get_kd = $this->kompetensi_dasar->get($kd_nilai->kd_id);
					$kompetensi_dasar = ($get_kd) ? $get_kd->kompetensi_dasar : '-';
					//echo $kd_nilai->id_kompetensi.'=>'.$nilai_per_kd.'<br />';
					$objPHPExcel->getActiveSheet()->setCellValue('A'.$a, "'".$kd_nilai->id_kompetensi);
					$objPHPExcel->getActiveSheet()->setCellValue('B'.$a, $kompetensi_dasar);
					$objPHPExcel->getActiveSheet()->setCellValue('C'.$a, count($all_kd_nilai));
					$huruf = 'D';
					foreach($data_siswa['data'] as $siswa){ 
						$nilai_per_kd = get_nilai_siswa_by_kd($kd_nilai->kd_id, $mapel->semester_id, $kompetensi_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id, $siswa->siswa_id);
						$objPHPExcel->getActiveSheet()->setCellValue($huruf.$a, number_format($nilai_per_kd,0));
						//$objPHPExcel->getActiveSheet()->setCellValue($huruf.$a, $huruf.$a);
						$huruf++;
					}
					$a++;
				}
				$i++;
			}
			$filename='LEGGER '.$nama_kompetensi.'_'.str_replace(' ','_',$nama_rombel->nama).'.xlsx'; //save our workbook as this file name
			header('Content-Type: application/vnd.ms-excel'); //mime type
			header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
			header('Cache-Control: max-age=0'); //no cache
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
			$objWriter->save('php://output');
			*/
			/*foreach($data_siswa['data'] as $siswa){
				echo $siswa->nama.'=>mana?=>';
				foreach($data_mapel as $key=>$mapel){
					//test($mapel);
					echo $mapel->nama_mata_pelajaran.'<br />';
					//$all_kd_nilai = $this->kd_nilai->find_all("rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE sekolah_id = '$loggeduser->sekolah_id' AND semester_id = $mapel->semester_id AND mata_pelajaran_id = $mapel->mata_pelajaran_id AND rombongan_belajar_id = '$mapel->rombongan_belajar_id' AND kompetensi_id = $kompetensi_id)");
					//
					$this->db->select('kd_id, id_kompetensi');
					$this->db->from('kd_nilai as a');
					$this->db->join('rencana_penilaian as b', 'a.rencana_penilaian_id = b.rencana_penilaian_id');
					$this->db->where('b.sekolah_id', $loggeduser->sekolah_id);
					$this->db->where('b.semester_id', $mapel->semester_id);
					$this->db->where('b.mata_pelajaran_id', $mapel->mata_pelajaran_id);
					$this->db->where('b.rombongan_belajar_id', $mapel->rombongan_belajar_id);
					$this->db->where('b.kompetensi_id', $kompetensi_id);
					$this->db->order_by('kd_id', 'asc');
					$this->db->group_by('kd_id, id_kompetensi');
					$query = $this->db->get();
					$all_kd_nilai = $query->result();
					foreach($all_kd_nilai as $kd_nilai){
						$nilai_per_kd = get_nilai_siswa_by_kd($kd_nilai->kd_id, $mapel->semester_id, $kompetensi_id, $mapel->rombongan_belajar_id, $mapel->mata_pelajaran_id, $siswa->siswa_id);
						echo $kd_nilai->id_kompetensi.'=>'.$nilai_per_kd.'<br />';
					}
				}
			}*/
		} else {
			$this->cetak_legger($ajaran_id,$rombel_id,$kompetensi_id);
		}
	}
	public function cetak_legger($ajaran_id,$rombel_id,$kompetensi_id){
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($loggeduser->sekolah_id);
		$ajaran = get_ta();
		$nama_rombel = $this->rombongan_belajar->get($rombel_id);
		$get_wali = $this->guru->get($nama_rombel->guru_id);
		$data_siswa = get_siswa_by_rombel($rombel_id);
		$data_mapel = $this->pembelajaran->find_all("semester_id = $ajaran_id and rombongan_belajar_id = '$rombel_id'");
		$this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->setActiveSheetIndex(0);
        $nama_kompetensi = 'PENGETAHUAN';
		if($kompetensi_id == 2){
			$nama_kompetensi = 'KETERAMPILAN';
		}
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->getStyle('A7')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B7')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C7')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('A8')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B')->getNumberFormat()->setFormatCode('0000000000');
		$objPHPExcel->getActiveSheet()->mergeCells('A8:C8');
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'LEGGER '.$nama_kompetensi);
		$objPHPExcel->getActiveSheet()->setCellValue('A2', strtoupper($sekolah->nama));
		$objPHPExcel->getActiveSheet()->setCellValue('A3', 'TAHUN PELAJARAN '.strtoupper($ajaran->tahun));
		$objPHPExcel->getActiveSheet()->setCellValue('C4', 'KELAS');
		$objPHPExcel->getActiveSheet()->setCellValue('C5', 'WALI KELAS');
		$objPHPExcel->getActiveSheet()->setCellValue('D4', $nama_rombel->nama);
		$objPHPExcel->getActiveSheet()->setCellValue('D5', $get_wali->nama);
		$objPHPExcel->getActiveSheet()->getStyle('A8')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue('A8', 'KKM');
		$objPHPExcel->getActiveSheet()->getStyle('A')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue('A7', 'NO.');
		$objPHPExcel->getActiveSheet()->getStyle('B')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue('B7', 'NISN');
		$objPHPExcel->getActiveSheet()->getStyle('B7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$objPHPExcel->getActiveSheet()->setCellValue('C7', 'NAMA SISWA');
        $objPHPExcel->getActiveSheet()->setTitle('LEGGER');
		$row = 9;
		$row_mapel = 7;
		$row_kkm = 8;
		$merger_mapel = 4;
		$merger_wali = 5;
		$x= 'D';
		$plus1 = 'E';
		$plus2 = 'F';
		for($i=0;$i<count($data_mapel);$i++){
			$huruf[] = $x;
			$last = $x;
			$plus_1 = $plus1;
			$plus_2 = $plus2;
			$x++;
			$plus1++;
			$plus2++;
		}
		$i=1;
		foreach($data_siswa['data'] as $siswa){
			$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $i);
			$objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $siswa->nisn);
			$objPHPExcel->getActiveSheet()->setCellValue('C'.$row, strtoupper($siswa->nama));
			foreach($data_mapel as $key=>$mapel){
				$nilai_value	= get_nilai_akhir_siswa($ajaran_id, $kompetensi_id, $rombel_id, $mapel->mata_pelajaran_id, $siswa->siswa_id);
				$objPHPExcel->getActiveSheet()->getStyle($huruf[$key].$row_mapel)->getAlignment()->setTextRotation(90);
				$objPHPExcel->getActiveSheet()->getColumnDimension($huruf[$key])->setAutoSize(true);
				$objPHPExcel->getActiveSheet()->getStyle($huruf[$key].$row_mapel)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$objPHPExcel->getActiveSheet()->setCellValue($huruf[$key].$row_mapel, get_nama_mapel($mapel->mata_pelajaran_id));
				$objPHPExcel->getActiveSheet()->setCellValue($huruf[$key].$row_kkm, get_kkm($ajaran_id,$rombel_id,$mapel->mata_pelajaran_id));
				$objPHPExcel->getActiveSheet()->setCellValue($huruf[$key].$row, $nilai_value);
				$objPHPExcel->getActiveSheet()->setCellValue($plus_1.$row, '=SUM(D'.$row.':'.$huruf[$key].$row.')');
				$objPHPExcel->getActiveSheet()->setCellValue($plus_2.$row, '=AVERAGE(D'.$row.':'.$huruf[$key].$row.')');
				$objPHPExcel->getActiveSheet()->getStyle($plus_1.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
				$objPHPExcel->getActiveSheet()->getStyle($plus_2.$row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
			}
			$i++;
			$row++;
		}
		$objPHPExcel->getActiveSheet()->mergeCells('A1:'.$plus_2.'1');
		$objPHPExcel->getActiveSheet()->mergeCells('A2:'.$plus_2.'2');
		$objPHPExcel->getActiveSheet()->mergeCells('A3:'.$plus_2.'3');
		$objPHPExcel->getActiveSheet()->mergeCells('D4:'.$plus_2.'4');
		$objPHPExcel->getActiveSheet()->mergeCells('D5:'.$plus_2.'5');
		$objPHPExcel->getActiveSheet()->getColumnDimension($plus_1)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension($plus_2)->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getStyle($plus_1.$row_mapel)->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->getStyle($plus_2.$row_mapel)->getAlignment()->setTextRotation(90);
		$objPHPExcel->getActiveSheet()->setCellValue($plus_1.$row_mapel, 'JUMLAH');
		$objPHPExcel->getActiveSheet()->setCellValue($plus_2.$row_mapel, 'RATA-RATA');
		$styleArray = array(
						'borders' => array(
							'allborders' => array(
								'style' => PHPExcel_Style_Border::BORDER_THIN,
								'color' => array('argb' => '00000000'),
							),
						),
					);
		$objPHPExcel->getActiveSheet()->getStyle('A7:'.$plus_2.($row - 1))->applyFromArray($styleArray);
        $filename='LEGGER '.$nama_kompetensi.'_'.str_replace(' ','_',$nama_rombel->nama).'.xlsx'; //save our workbook as this file name
        header('Content-Type: application/vnd.ms-excel'); //mime type
        header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
        header('Cache-Control: max-age=0'); //no cache
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007'); 
        $objWriter->save('php://output');
	}
	public function cek_legger($ajaran_id,$rombel_id,$kompetensi_id){
		$data['loggeduser'] = $this->ion_auth->user()->row();
		$data['sekolah'] = $this->sekolah->get($data['loggeduser']->sekolah_id);
		$data['ajaran'] = get_ta();
		$data['kompetensi_id'] = $kompetensi_id;
		$data['nama_rombel'] 	= $this->rombongan_belajar->get($rombel_id);
		$data['get_wali'] 		= $this->guru->get($data['nama_rombel']->guru_id);
		$data['data_siswa'] 	= get_siswa_by_rombel($rombel_id);
		$data['data_mapel'] 	= $this->pembelajaran->find_all("semester_id = $ajaran_id and rombongan_belajar_id = '$rombel_id' AND kelompok_id IS NOT NULL");
		$this->template->title('Administrator Panel')
		->set_layout($this->blank_tpl)
		->set('page_title', 'Cek Legger')
		->build($this->admin_folder.'/cetak/cek_legger', $data);
	}
}