<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Backup extends Backend_Controller {
	protected $activemenu = 'settings';
	public function __construct(){
		parent::__construct();
		$this->template->set('activemenu', $this->activemenu);
	}
	public function index(){
		$dirname = dirname(__FILE__);
		$dirname = str_replace('\\admin','',$dirname);
		$dirname = str_replace('\\controllers','',$dirname);
		$dirname = str_replace('\\application','',$dirname);
		$html = '';
		$html .= 'cd C:/eRaporSMK2018/pgsql/bin'."\r\n";
		$html .= 'copy "..\..\database\pg_hba.conf" "'.$dirname.'\assets\temp\pg_hba_back.conf"'."\r\n";
		$html .= 'copy "eRaporSMK2018.dfrint" "..\..\database\pg_hba.conf"'."\r\n";
		$html .= 'pg_dump -i -h localhost -p 54132 -U erapor_smk -F c -b -v -f "'.$dirname.'\assets\temp\erapor_backup.backup" erapor_smk'."\r\n";
		$html .= 'copy "'.$dirname.'\assets\temp\pg_hba_back.conf" "..\..\database\pg_hba.conf"'."\r\n";
		$html .= 'del /A /Q "'.$dirname.'\assets\temp\pg_hba_back.conf"'."\r\n";
		$file_bat_backup = "C:/eRaporSMK2018/pgsql/bin/backup.bat";
		file_put_contents($file_bat_backup, $html);
		//chdir('C:\eRaporSMK2018\pgsql\bin');
		//$output = shell_exec('CMD /c '.$file_bat_backup);
		//echo "<pre>$output</pre>";
		//echo $dirname.'<br />';
		//unlink($file_bat_backup);
		//chdir($dirname);
		//echo getcwd();
		//$file_backup = './assets/temp/erapor_backup.backup';
		//if(file_exists($file_backup)){
			//$this->load->helper('download');
			//unlink($file_backup);
			//force_download($file_backup, NULL);
			//$this->zip->download('backup_eRaporSMK2018-'.date('Y-m-d_H:i:s').'.zip');
		//}
	}
}
