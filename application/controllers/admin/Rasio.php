<?php
class Rasio extends Backend_Controller {
	protected $activemenu = 'perencanaan';
	public function __construct() {
		parent::__construct(); 
		$this->template->set('activemenu', $this->activemenu);
	}
	public function index(){
		$ajaran = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$pembelajaran = $this->pembelajaran->find_all("sekolah_id = '$loggeduser->sekolah_id' AND guru_id = '$loggeduser->guru_id' AND semester_id = $ajaran->id");
		if(!$pembelajaran){
			$pembelajaran = $this->pembelajaran->find_all("sekolah_id = '$loggeduser->sekolah_id' AND guru_pengajar_id = '$loggeduser->guru_id' AND semester_id = $ajaran->id");
		}
		$data['all_pembelajaran'] = $pembelajaran;
		$data['semester_id'] = $ajaran->id;
		//$query = $this->pembelajaran->find_all("sekolah_id = '$loggeduser->sekolah_id' AND semester_id = $ajaran->id $filter_data AND ($search_form)", '*','mata_pelajaran_id asc, rombongan_belajar_id desc', $start, $rows);
		// OR guru_pengajar_id = '$loggeduser->guru_id' AND semester_id = $ajaran->id
		$this->template->title('Administrator Panel')
		->set_layout($this->admin_tpl)
		->set('guru_id', $loggeduser->guru_id)
		->set('form_action', 'admin/rasio/simpan_rasio')
		->set('page_title', 'Perencanaan Rasio Nilai Akhir')
		->build($this->admin_folder.'/perencanaan/rasio',$data);
	}
	public function simpan_rasio(){
		test($_POST);
		//die();
		$semester_id = $this->input->post('semester_id');
		$guru_id = $this->input->post('guru_id');
		$mata_pelajaran_id = $this->input->post('mata_pelajaran_id');
		$rasio_p = $this->input->post('rasio_p');
		$rasio_k = $this->input->post('rasio_k');
		if($rasio_p){
			$sukses = 0;
			$gagal = 0;
			$numeric = 0;
			if(is_array($rasio_p)){
				foreach($rasio_p as $key => $value){
					$jumlah_rasio = ($value + $rasio_k[$key]);
					if($jumlah_rasio != 100){
						$gagal++;
						//echo 'skip';
					} else {
						$sukses++;
						if(is_numeric($value) && is_numeric($rasio_k[$key])){
							$mapel_id = $mata_pelajaran_id[$key];
							$update_rasio = array(
								'rasio_p'	=> $value,
								'rasio_k'	=> $rasio_k[$key]
							);
							$find_pembelajaran_guru_id = $this->pembelajaran->find_all("semester_id = $semester_id AND guru_id = '$guru_id' AND mata_pelajaran_id = $mapel_id");
							if($find_pembelajaran_guru_id){
								foreach($find_pembelajaran_guru_id as $p_guru_id){
									$this->pembelajaran->update($p_guru_id->pembelajaran_id, $update_rasio);
								}
							}
							$find_pembelajaran_guru_pengajar_id = $this->pembelajaran->find_all("semester_id = $semester_id AND guru_pengajar_id = '$guru_id' AND mata_pelajaran_id = $mapel_id");
							if($find_pembelajaran_guru_pengajar_id){
								foreach($find_pembelajaran_guru_pengajar_id as $p_guru_pengajar_id){
									$this->pembelajaran->update($p_guru_pengajar_id->pembelajaran_id, $update_rasio);
								}
							}
							//$this->pembelajaran->update($key, $update_rasio);
						} else {
							$numeric++;
						}
					}
				}
			}
			if($sukses){
				$this->session->set_flashdata('success', 'Berhasil menyimpan rasio nilai akhir');
			}
			if($gagal){
				$this->session->set_flashdata('error', 'Gagal menyimpan rasio nilai akhir. Akumulasi rasio harus sama dengan 100 (seratus)');
			}
			if($numeric){
				$this->session->set_flashdata('error', 'Gagal menyimpan rasio nilai akhir. Isian rasio harus berupa angka');
			}
			redirect('admin/rasio');
		}
	}
}