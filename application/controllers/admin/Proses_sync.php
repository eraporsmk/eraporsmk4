<?php
class Proses_sync extends Backend_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('sync','file'));
		$this->load->library('curl');
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit', '-1');
	}
	public function index(){
		$ajaran = get_ta();
		$tahun = $ajaran->tahun;
		$smt = $ajaran->semester;
		$tahun = substr($tahun, 0,4); // returns "d"
		$semester_id = $tahun.$smt;
		$set_semester_id = $ajaran->id;
		$settings = $this->settings->get(1);
		$user = $this->ion_auth->user()->row();
		$sekolah = $this->sekolah->get($user->sekolah_id);
		$sekolah_id = ($sekolah) ? $sekolah->sekolah_id_dapodik : $user->sekolah_id;
		$table_sync = array(
			1	=> 'absen',
			2	=> 'anggota_rombel',
			3	=> 'catatan_ppk',
			4	=> 'catatan_wali',
			5	=> 'deskripsi_mata_pelajaran',
			6	=> 'deskripsi_sikap',
			7	=> 'ekstrakurikuler',
			8	=> 'guru_terdaftar',
			9	=> 'jurusan_sp', //no semester
			10	=> 'kd_nilai', //no semester
			11	=> 'nilai',
			12	=> 'nilai_akhir',
			13	=> 'nilai_ekstrakurikuler',
			14	=> 'nilai_sikap',
			15	=> 'nilai_ukk',
			16	=> 'pembelajaran',
			17	=> 'prakerin',
			18	=> 'prestasi',
			19	=> 'ref_guru', //no semester
			20	=> 'ref_sekolah', //no semester
			21	=> 'ref_sikap', //no semester
			22	=> 'ref_siswa', //no semester
			23	=> 'remedial',
			24	=> 'rencana_penilaian',
			25	=> 'rombongan_belajar',
			26	=> 'teknik_penilaian', //no semester
		);
		$response = '';
		$arr_content = array();
		$total = 0;
		foreach($table_sync as $sync){
			$this->db->select('*');
			$this->db->from($sync);
			$this->db->where('last_sync >=', $settings->last_sync);
			if ($this->db->field_exists('semester_id', $sync)){
				$this->db->where('semester_id', $set_semester_id);
			}
			$result = $this->db->count_all_results();
			if($result){
				$total ++;
			}
		}
		$i=0;
		foreach($table_sync as $sync){
			$this->db->select('*');
			$this->db->from($sync);
			$this->db->where('last_sync >=', $settings->last_sync);
			if ($this->db->field_exists('semester_id', $sync)){
				$this->db->where('semester_id', $set_semester_id);
			}
			$query = $this->db->get();
			$result = $query->result();
			if($result){
				$kirim_data = kirim_data($semester_id, $sekolah_id, $sync, json_encode($result));
				$percent = intval($i/ $total * 100);
				$arr_content['percent'] = $percent;
				$arr_content['message'] = "Mengirim data " .$sync;
				$arr_content['total'] = $total;
				$arr_content['no'] = $i;
				$arr_content['response'] = $kirim_data;
				file_put_contents("./assets/temp/" . session_id() . ".txt", json_encode($arr_content));
				file_put_contents("./assets/temp/" . $sync . ".txt", json_encode($kirim_data));
				$i++;
			}
			sleep(1);
		}
		$this->db->select('*');
		$this->db->from('ref_semester');
		$query = $this->db->get();
		$result = $query->result();
		if($result){
			$kirim_data = kirim_data($semester_id, $sekolah_id, 'ref_semester', json_encode($result));
		}
		$this->settings->update(1, array('last_sync' => date('Y-m-d H:i:s')));
	}
	public function test(){
		echo '5% [Mengirim data siswa]';
	}
}