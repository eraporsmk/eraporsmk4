<?php
class Perencanaan extends Backend_Controller {
	function __construct()  {
         parent::__construct();
    }
	public function index(){
		$ajaran = get_ta();
		$loggeduser = $this->ion_auth->user()->row();
		$sekolah_id = $loggeduser->sekolah_id;
		$guru_id = ($loggeduser->guru_id) ? $loggeduser->guru_id : 0;
		$config['upload_path'] = './assets/files/';
		//$config['file_name'] = $fileName;
		$config['allowed_types'] = 'xls|xlsx';
		$config['overwrite'] = TRUE;
		$this->load->library('upload');
        $this->upload->initialize($config);
		if(!$this->upload->do_upload('import')){
			$status['type'] = 'error';
			$status['text'] = $this->upload->display_errors();
			$status['title'] = 'Import Data Gagal!';
			echo json_encode($status);
			exit();
		}
		$media = $this->upload->data();
		//test($media);
		require_once APPPATH."/third_party/simplexlsx.class.php"; 
		if ( $xlsx = SimpleXLSX::parse('./assets/files/'.$media['file_name'])) {
			$get_rows = $xlsx->rows();
			$get_kd = $get_rows[2];
			$akhir_kolom = count($get_rows[0]) - 1;
			$get_kd_id = $get_rows[3];
			$mata_pelajaran_id 		= $get_rows[2][0];
			$rombongan_belajar_id 	= $get_rows[2][1];
			$kompetensi_id			= $get_rows[2][2];
			$redirect = 'pengetahuan';
			if($kompetensi_id == 2){
				$redirect = 'keterampilan';
			}
			//test($get_rows);
			$find_import_rencana = $this->import_rencana->find("semester_id = $ajaran->id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id AND guru_id = '$guru_id'");
			if($find_import_rencana){
				$status['type'] = 'error';
				$status['text'] = 'Import perencanaan dibatasi 1 (satu) kali per semester. Jika ingin menambah perencanaan, silahkan lakukan secara manual.';
				$status['title'] = 'Import Data Gagal!';
				echo json_encode($status);
				exit;
			} else {
				$insert_import_rencana = array(
					'semester_id'			=> $ajaran->id,
					'kompetensi_id' 		=> $kompetensi_id,
					'rombongan_belajar_id' 	=> $rombongan_belajar_id,
					'mata_pelajaran_id'		=> $mata_pelajaran_id,
					'guru_id'				=> $guru_id,
				);
				$this->import_rencana->insert($insert_import_rencana);
			}
			$pembelajaran_id = $this->pembelajaran->find("sekolah_id = '$sekolah_id' AND semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = '$mata_pelajaran_id'");
			unset($get_rows[0], $get_rows[1], $get_rows[2], $get_rows[3], $get_kd[0], $get_kd[1], $get_kd[2], $get_kd[$akhir_kolom], $get_kd_id[0], $get_kd_id[1], $get_kd_id[2], $get_kd_id[$akhir_kolom]);
			$all_kd = count($get_kd);
			$sukses = 0;
			$gagal = 0;
			foreach($get_rows as $data){
				$get_metode = $this->teknik_penilaian->find("kompetensi_id = $kompetensi_id AND nama = '$data[1]'");
				$metode_id = ($get_metode) ? $get_metode->teknik_penilaian_id : 0;
				if($data[0] && $metode_id){
					$rencana_penilaian_id = gen_uuid();
					$data_insert_rencana = array(
						'rencana_penilaian_id'	=> $rencana_penilaian_id,
						'sekolah_id'			=> $sekolah_id,
						'semester_id'			=> $ajaran->id,
						'mata_pelajaran_id' 	=> $mata_pelajaran_id,
						'rombongan_belajar_id'	=> $rombongan_belajar_id,
						'kompetensi_id'			=> $kompetensi_id,
						'nama_penilaian'		=> trim($data[0]),
						'metode_id'				=> $metode_id,
						'bobot'					=> trim($data[2]),
						'keterangan'			=> trim($data[$akhir_kolom]),
						'pembelajaran_id'		=> ($pembelajaran_id) ? $pembelajaran_id->pembelajaran_id : NULL,
					);
					if($this->rencana_penilaian->insert($data_insert_rencana)){
						$sukses++;
						foreach($get_kd as $k=>$kd){
							if($data[$k]){
								$insert_kd_nilai = array(
									'kd_nilai_id'			=> gen_uuid(),
									'rencana_penilaian_id' 	=> $rencana_penilaian_id,
									'kd_id' 				=> $kd,//$get_post_kd[0],
									'id_kompetensi' 		=> str_replace('kd_','',$get_kd_id[$k]),//$get_post_kd[1],
								);
								$this->kd_nilai->insert($insert_kd_nilai);
							}
						}
					} else {
						$gagal++;
					}
				} else {
					$gagal++;
				}
			}
			$status['redirect'] = site_url('admin/perencanaan/'.$redirect);
			$status['type'] = 'success';
			$status['title'] = 'Import Data Berhasil!';
			$status['text'] = $sukses.' perencanaan berhasil disimpan.';//, '.$gagal.' perencanaan gagal disimpan';
		} else {
			$status['redirect'] = '';
			$status['type'] = 'error';
			$status['text'] = 'Format Import tidak sesuai. Silahkan download template yang telah disediakan.';
			$status['title'] = 'Import Data Gagal!';
		}
		echo json_encode($status);
	}
}