<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title.' ('.$total_rows.' => '.$inserted. ')'; ?></h3>
	</div>
    <div class="box-body">
	<div class="text-center">
		<?php //echo $pagination; ?>
	</div>
	<table class="table table-bordered table-striped table-hover">
            <thead>
				<tr>
					<th class="text-center">No</th>
					<th class="text-center">Nama Siswa</th>
					<th class="text-center">Nama Ekskul</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$no = $this->uri->segment('4') + 1;
				if($dapodik){
					foreach($dapodik as $data){
						$find_anggota_rombel = $this->anggota_rombel->find_by_anggota_rombel_id_dapodik($data->anggota_rombel_id);
						$find_siswa = $this->siswa->find_by_siswa_id_dapodik($data->peserta_didik_id);
						$attributes_update_anggota_rombel = array(
							'semester_id' 				=> $ajaran->id, 
							'rombongan_belajar_id' 		=> $data->rombongan_belajar_id, 
							'siswa_id' 					=> ($find_siswa) ? $find_siswa->siswa_id : strtolower($data->peserta_didik_id),
							'anggota_rombel_id_dapodik'	=> $data->anggota_rombel_id,
						);
						//test($find_anggota_rombel);
						if($find_anggota_rombel && $find_siswa){
							$result = 'update';
							$class = 'table-danger"';
							$this->anggota_rombel->update($find_anggota_rombel->anggota_rombel_id, $attributes_update_anggota_rombel);
							//test($update_ekskul);
							//$this->ekstrakurikuler->update($find_kelas_ekskul->ekstrakurikuler_id, $data_ekskul);
						} else {
							if(!$find_siswa){
								$result = 'abaikan';
								$class = 'table-warning"';
							} else {
								$attributes_update_anggota_rombel['anggota_rombel_id'] = gen_uuid();
								$this->anggota_rombel->insert($attributes_update_anggota_rombel);
								$result = 'insert';
								$class = 'table-success"';
							}
							//$data_ekskul['ekstrakurikuler_id'] = gen_uuid();
							//$this->ekstrakurikuler->insert($data_ekskul);
						}
				?>
					<tr class="<?php echo $class; ?>">
						<td class="text-center"><?php echo $no++; ?></td>
						<td><?php echo $data->nama_siswa; ?></td>
						<td class="text-center"><?php echo $data->nama_rombel; ?></td>
						<td class="text-center"><?php echo $result; ?></td>
					</tr>
				<?php
				//}
				//break; 
					}
				} else { ?>
					<tr>
						<td class="text-center" colspan="4">Tidak ada data untuk ditampilkan</td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'wait'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>