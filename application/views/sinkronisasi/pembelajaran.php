<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title.' ('.$total_rows.' => '.$inserted. ')'; ?></h3>
	</div>
    <div class="box-body">
	<div class="text-center">
		<?php echo $pagination; ?>
	</div>
	<table class="table table-bordered table-striped table-hover">
            <thead>
				<tr>
					<th class="text-center" style="vertical-align: middle;">No</th>
					<th class="text-center">mata_pelajaran</th>
					<th class="text-center">rombongan_belajar</th>
					<th class="text-center">guru_mata_pelajaran</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$no = $this->uri->segment('5') + 1;
				foreach($dapodik as $data){
					//test($data);
					//die();
					$get_rombel = $this->rombongan_belajar->find_by_rombel_id_dapodik($data->rombongan_belajar_id);
					$rombongan_belajar_id = ($get_rombel) ? strtolower($get_rombel->rombongan_belajar_id) : gen_uuid();
					$get_guru = $this->guru->find_by_guru_id_dapodik($data->ptk_id);
					$guru_id = ($get_guru) ? strtolower($get_guru->guru_id) : gen_uuid();
					$mata_pelajaran_id = $data->mata_pelajaran_id;
					$pembelajaran_id = gen_uuid();
					$insert_pembelajaran = array(
						'pembelajaran_id'			=> $pembelajaran_id,
						'semester_id'				=> $ajaran->id,
						'rombongan_belajar_id'		=> $rombongan_belajar_id,
						'guru_id'					=> $guru_id,
						'mata_pelajaran_id'			=> $mata_pelajaran_id,
						'nama_mata_pelajaran'		=> $data->nama_mata_pelajaran,
						'kkm'						=> 0,
						'is_dapodik'				=> 1,
						'pembelajaran_id_dapodik'	=> $data->pembelajaran_id,
					);
					$update_pembelajaran = array(
						'rombongan_belajar_id'		=> $rombongan_belajar_id,
						'guru_id'					=> $guru_id,
						'mata_pelajaran_id'			=> $mata_pelajaran_id,
						'nama_mata_pelajaran'		=> $data->nama_mata_pelajaran,
						'pembelajaran_id_dapodik'	=> $data->pembelajaran_id,
					);
					//$find_pembelajaran = $this->pembelajaran->find("semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' AND guru_id = '$guru_id' AND mata_pelajaran_id = $mata_pelajaran_id AND nama_mata_pelajaran = '$data->nama_mata_pelajaran'");
					$find_pembelajaran = $this->pembelajaran->find_by_pembelajaran_id_dapodik($data->pembelajaran_id);
					//$find_pembelajaran = $this->pembelajaran->find("semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id");
					if($find_pembelajaran){
						$this->pembelajaran->update($find_pembelajaran->pembelajaran_id, $update_pembelajaran);
						$result = 'update';
					} else {
						$id_insert_pembelajaran = $this->pembelajaran->insert($insert_pembelajaran);
						$result = 'insert';
					}
			?>
				<tr>
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo get_nama_mapel($mata_pelajaran_id).' ('.$mata_pelajaran_id.')'; ?></td>
					<td><?php echo get_nama_rombel($rombongan_belajar_id); ?></td>
					<td><?php echo get_nama_guru($guru_id); ?></td>
					<td><?php echo $result; ?></td>
				</tr>
			<?php
			//break; 
			} ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		//$(this).css({cursor: 'none'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>