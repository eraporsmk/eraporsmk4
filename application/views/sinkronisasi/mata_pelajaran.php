<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title; ?></h3>
	</div>
    <div class="box-body">
	<table class="table table-bordered table-striped table-hover">
	<!--table class="table table-bordered table-striped table-hover" style="display:none;"-->
            <thead>
				<tr>
					<th class="text-center" style="vertical-align: middle;">No</th>
					<th class="text-center">mata_pelajaran_id</th>
					<th class="text-center">nama</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
$new_mapel = array(
	array(
		'mata_pelajaran_id' => 400100210, 
		'nama' => 'Perkembangan Nilai Agama dan Moral', 
		'pilihan_sekolah' => 0, 
		'pilihan_buku' => 0, 
		'pilihan_kepengawasan' => 0, 
		'pilihan_evaluasi' => 0, 
		'jurusan_id' => NULL, 
		'created_at' => '2017-05-03 15:00:00', 
		'updated_at' => '2017-05-03 15:30:00', 
		'deleted_at' => NULL, 
		'last_sync' => '1901-01-01 00:00:00'
	),
	array(
		'mata_pelajaran_id' => 400100220, 
		'nama' => 'Perkembangan Motorik', 
		'pilihan_sekolah' => 0, 
		'pilihan_buku' => 0, 
		'pilihan_kepengawasan' => 0, 
		'pilihan_evaluasi' => 0, 
		'jurusan_id' => NULL, 
		'created_at' => '2017-05-03 15:00:00', 
		'updated_at' => '2017-05-03 15:30:00', 
		'deleted_at' => NULL, 
		'last_sync' => '1901-01-01 00:00:00'
	),
	array(
		'mata_pelajaran_id' => 400100230, 
		'nama' => 'Perkembangan Kognitif', 
		'pilihan_sekolah' => 0, 
		'pilihan_buku' => 0, 
		'pilihan_kepengawasan' => 0, 
		'pilihan_evaluasi' => 0, 
		'jurusan_id' => NULL, 
		'created_at' => '2017-05-03 15:00:00', 
		'updated_at' => '2017-05-03 15:30:00', 
		'deleted_at' => NULL, 
		'last_sync' => '1901-01-01 00:00:00'
	),
	array(
		'mata_pelajaran_id' => 400100240, 
		'nama' => 'Perkembangan Sosial Emosional', 
		'pilihan_sekolah' => 0, 
		'pilihan_buku' => 0, 
		'pilihan_kepengawasan' => 0, 
		'pilihan_evaluasi' => 0, 
		'jurusan_id' => NULL, 
		'created_at' => '2017-05-03 15:00:00', 
		'updated_at' => '2017-05-03 15:30:00', 
		'deleted_at' => NULL, 
		'last_sync' => '1901-01-01 00:00:00'
	),
	array(
		'mata_pelajaran_id' => 400100250, 
		'nama' => 'Perkembangan Bahasa', 
		'pilihan_sekolah' => 0, 
		'pilihan_buku' => 0, 
		'pilihan_kepengawasan' => 0, 
		'pilihan_evaluasi' => 0, 
		'jurusan_id' => NULL, 
		'created_at' => '2017-05-03 15:00:00', 
		'updated_at' => '2017-05-03 15:30:00', 
		'deleted_at' => NULL, 
		'last_sync' => '1901-01-01 00:00:00'
	),
	array(
		'mata_pelajaran_id' => 400100260, 
		'nama' => 'Perkembangan Seni', 
		'pilihan_sekolah' => 0, 
		'pilihan_buku' => 0, 
		'pilihan_kepengawasan' => 0, 
		'pilihan_evaluasi' => 0, 
		'jurusan_id' => NULL, 
		'created_at' => '2017-05-03 15:00:00', 
		'updated_at' => '2017-05-03 15:30:00', 
		'deleted_at' => NULL, 
		'last_sync' => '1901-01-01 00:00:00'
	),
	array(
		'mata_pelajaran_id' => 400100270, 
		'nama' => 'Program Pendidikan Khusus', 
		'pilihan_sekolah' => 0, 
		'pilihan_buku' => 0, 
		'pilihan_kepengawasan' => 0, 
		'pilihan_evaluasi' => 0, 
		'jurusan_id' => NULL, 
		'created_at' => '2017-05-03 15:00:00', 
		'updated_at' => '2017-05-03 15:30:00', 
		'deleted_at' => NULL, 
		'last_sync' => '1901-01-01 00:00:00'
	)
);
foreach($new_mapel as $mapel){
	$this->db->select('*');
	$this->db->from('mata_pelajaran');
	$this->db->where('mata_pelajaran_id', $mapel['mata_pelajaran_id']);
	$query_new_mapel = $this->db->get();
	$find_new_mapel = $query_new_mapel->row();
	if(!$find_new_mapel){
		$this->db->insert('mata_pelajaran', $mapel);
	}
}
				$no = $this->uri->segment('4') + 1;
				foreach($dapodik as $data){
					$data->created_at = date('Y-m-d H:i:s', strtotime($data->create_date));
					$data->updated_at = date('Y-m-d H:i:s', strtotime($data->last_update));
					$data->deleted_at = ($data->expired_date) ? date('Y-m-d H:i:s', strtotime($data->expired_date)) : NULL;
					unset($data->create_date, $data->last_update, $data->expired_date);
					//test($data);
					//die();
					//echo $data->kurikulum_id.'=>'.$data->mata_pelajaran_id.'=>'.$data->tingkat_pendidikan_id;
					//$find = $this->mata_pelajaran_kurikulum->find("kurikulum_id = $data->kurikulum_id AND mata_pelajaran_id = $data->mata_pelajaran_id AND tingkat_pendidikan_id = $data->tingkat_pendidikan_id");
					$this->db->select('*');
					$this->db->from('mata_pelajaran');
					$this->db->where('mata_pelajaran_id', $data->mata_pelajaran_id);
					$query = $this->db->get();
					$find = $query->row();
					//test($find);
					if($find){
						//$this->mata_pelajaran->update($find->mata_pelajaran_id, $data);
						$this->db->where('mata_pelajaran_id', $find->mata_pelajaran_id);
						$this->db->update('mata_pelajaran', $data);
						$result = 'update';
						$class = 'table-danger"';
					} else {
						//$this->mata_pelajaran->insert($data);
						$this->db->insert('mata_pelajaran', $data);
						$result = 'insert';
						$class = 'table-success"';
					}
			?>
				<tr class="<?php echo $class; ?>">
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo $data->mata_pelajaran_id; ?></td>
					<td><?php echo $data->nama; ?></td>
					<td class="text-center"><?php echo $result; ?></td>
				</tr>
			<?php
			//}
			//break; 
			} ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'wait'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>