<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title; ?></h3>
	</div>
    <div class="box-body">
	<table class="table table-bordered table-striped table-hover">
	<!--table class="table table-bordered table-striped table-hover" style="display:none;"-->
            <thead>
				<tr>
					<th class="text-center" style="vertical-align: middle;">No</th>
					<th class="text-center">kurikulum_id</th>
					<th class="text-center">nama_kurikulum</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$this->db->select('*');
				$this->db->from('ref_kurikulum');
				$this->db->where('kurikulum_id', 980);
				$query_980 = $this->db->get();
				$find_980 = $query_980->row();
				if(!$find_980){
					$data_980 = array(
						'kurikulum_id'			=> 980,
						'nama_kurikulum'		=> 'TKLB 2013',
						'mulai_berlaku'			=> '2013-01-01',
						'sistem_sks'			=> 0,
						'total_sks'				=> 0,
						'jenjang_pendidikan_id'	=> 2,
						'jurusan_id'			=> NULL,
						'last_sync'				=> '1901-01-01 00:00:00',
						'created_at'			=> '2017-05-02 10:30:00',
						'updated_at'			=> '2017-05-02 11:00:00',
						'deleted_at'			=> NULL
					);
					$this->db->insert('ref_kurikulum', $data_980);
				}
				$no = $this->uri->segment('4') + 1;
				foreach($dapodik as $data){
					$data->created_at = date('Y-m-d H:i:s', strtotime($data->create_date));
					$data->updated_at = date('Y-m-d H:i:s', strtotime($data->last_update));
					$data->deleted_at = ($data->expired_date) ? date('Y-m-d H:i:s', strtotime($data->expired_date)) : NULL;
					unset($data->create_date, $data->last_update, $data->expired_date);
					//test($data);
					//die();
					//die();
					//echo $data->kurikulum_id.'=>'.$data->mata_pelajaran_id.'=>'.$data->tingkat_pendidikan_id;
					//$find = $this->mata_pelajaran_kurikulum->find("kurikulum_id = $data->kurikulum_id AND mata_pelajaran_id = $data->mata_pelajaran_id AND tingkat_pendidikan_id = $data->tingkat_pendidikan_id");
					$this->db->select('*');
					$this->db->from('ref_kurikulum');
					$this->db->where('kurikulum_id', $data->kurikulum_id);
					$query = $this->db->get();
					$find = $query->row();
					//test($find);
					if($find){
						//$this->mata_pelajaran->update($find->mata_pelajaran_id, $data);
						$this->db->where('kurikulum_id', $find->kurikulum_id);
						$this->db->update('ref_kurikulum', $data);
						$result = 'update';
						$class = 'table-danger"';
					} else {
						//$this->mata_pelajaran->insert($data);
						$this->db->insert('ref_kurikulum', $data);
						$result = 'insert';
						$class = 'table-success"';
					}
			?>
				<tr class="<?php echo $class; ?>">
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo $data->kurikulum_id; ?></td>
					<td><?php echo $data->nama_kurikulum; ?></td>
					<td class="text-center"><?php echo $result; ?></td>
				</tr>
			<?php
			//}
			//break; 
			} ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'wait'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>