<style>
    #progress {
      width: 500px;
      border: 1px solid #aaa;
      height: 20px;
    }
    #progress .bar {
      background-color: #ccc;
      height: 20px;
    }
  </style>
<div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-success">
	<div class="box-header">
		<i class="fa fa-bank"></i>
		<h3 class="box-title">Identitas Sekolah</h3>
	</div>
	<div class="box-body">
		<table class="table">
			<tr>
				<th width="40%">NPSN Sekolah</th>
				<th width="1%">:</th>
				<th width="55%"><?php echo ($sekolah) ? $sekolah->npsn : '-'; ?></th>
			</tr>
			<tr>
				<th width="40%">Nama Sekolah</th>
				<th width="1%">:</th>
				<th width="55%"><?php echo ($sekolah) ? $sekolah->nama : '-'; ?></th>
			</tr>
			<tr>
				<th width="40%">Alamat Sekolah</th>
				<th width="1%">:</th>
				<th width="55%"><?php echo ($sekolah) ? $sekolah->alamat : '-'; ?></th>
			</tr>
			<tr>
				<th width="40%">Kepala Sekolah</th>
				<th width="1%">:</th>
				<th width="55%"><?php echo ($sekolah) ? get_nama_guru($sekolah->guru_id) : '-'; ?></th>
			</tr>
		</table> 
    </div><!-- /.box-body -->
</div><!-- /.box -->
<?php
$max_input_vars = ini_get('max_input_vars');
$last_sync = date('Y-m-d H:i:s');
$settings = $this->settings->get(1);
if(!isset($settings->last_sync)){
	if ($this->db->field_exists('last_sync', 'settings')){
		$this->settings->update(1, array('desc_mapel' => 0));
		redirect('admin/sinkronisasi/sync');
	} else {
		$this->load->dbforge();
		$fields = array(
        	'last_sync' => array(
				'type' 		=> 'datetime',
				'null'	=> true,
				'default'	=> '2017-05-18 06:45:06',
			),
		);
		$this->dbforge->add_column('settings', $fields);
	}
} else {
	$last_sync = $settings->last_sync;
}
$status = checkdnsrr('php.net');
$connect = ($status) ? 'bg-green' : 'bg-red';
$text = ($status) ? 'TERHUBUNG' : 'TIDAK TERHUBUNG';
$tombol = ($status) ? 'ajax' : 'disabled';
if($status){
	if(!$status_sync->server){
		$status = FALSE;
		$connect = ($status) ? 'bg-green' : 'bg-red';
		$text = ($status) ? 'TERHUBUNG' : 'Pengiriman data ditutup sementara';
		$tombol = ($status) ? 'ajax' : 'disabled';
	}
}
$table_sync = array(
	1	=> 'absen',
	2	=> 'anggota_rombel',
	3	=> 'catatan_ppk',
	4	=> 'catatan_wali',
	5	=> 'deskripsi_mata_pelajaran',
	6	=> 'deskripsi_sikap',
	7	=> 'ekstrakurikuler',
	8	=> 'guru_terdaftar',
	9	=> 'jurusan_sp', //no semester
	10	=> 'kd_nilai', //no semester
	11	=> 'nilai',
	12	=> 'nilai_akhir',
	13	=> 'nilai_ekstrakurikuler',
	14	=> 'nilai_sikap',
	15	=> 'nilai_ukk',
	16	=> 'pembelajaran',
	17	=> 'prakerin',
	18	=> 'prestasi',
	19	=> 'ref_guru', //no semester
	20	=> 'ref_sekolah', //no semester
	21	=> 'ref_sikap', //no semester
	22	=> 'ref_siswa', //no semester
	23	=> 'remedial',
	24	=> 'rencana_penilaian',
	25	=> 'rombongan_belajar',
	26	=> 'teknik_penilaian', //no semester
);
?>
<div class="box box-warning">
	<div class="box-header">
		<i class="fa fa-signal"></i>
		<h3 class="box-title">STATUS KONEKSI : <span class="label <?php echo $connect; ?>"><?php echo $text; ?></span></h3>
	</div>
	<div class="box-body text-center">
		<p>Pengiriman data dilakukan terakhir <strong><?php echo TanggalIndo($last_sync); ?></strong></p>
		<p><button type="button" class="btn btn-success btn-lrg <?php echo $tombol; ?>" title="KIRIM DATA" data-loading-text="<i class='fa fa-spinner fa-spin '></i> &nbsp; SEDANG PROSES SINKRONISASI">
            <i class="fa fa-refresh"></i>&nbsp; KIRIM DATA
          </button></p>
		<div class="progress">
			<div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"><span class="status_text justify-content-center d-flex position-absolute w-100"></span>
			</div>
		</div>
		<span class="response_text"></span> <span class="response_table"></span>
    </div><!-- /.box-body -->
</div><!-- /.box -->
<div class="box box-primary">
	<div class="box-header">
		<i class="fa fa-reorder"></i>
		<h3 class="box-title">DATA YANG MENGALAMI PERUBAHAN</h3>
	</div>
	<div class="box-body">
		<table class="table table-bordered">
			<tr>
				<th style="width: 10px">No.</th>
				<th>Table</th>
				<th style="width: 100px" class="text-center">Jumlah Data</th>
			</tr>
			<?php
			$i=1;
			$total = 0;
			foreach($table_sync as $sync){
				$this->db->select('*');
				$this->db->from($sync);
				$this->db->where('last_sync >=', $last_sync);
				if ($this->db->field_exists('semester_id', $sync)){
					$this->db->where('semester_id', $semester_id);
				}
				$result = $this->db->count_all_results();
				if($result){
					$total += $result;
			?>
			<tr>
				<td class="text-center"><?php echo $i; ?></td>
				<td><?php echo $sync; ?></td>
				<td class="text-right"><?php echo number_format($result,0, '', '.'); ?></td>
			</tr>
			<?php $i++;
				}
			} 
			if($total){?>
			<tr>
				<td colspan="2" class="text-right"><strong>T O T A L</strong></td>
				<td class="text-right"><strong><?php echo number_format($total,0, '', '.'); ?></strong></td>
			</tr>
			<?php } else { ?>
			<tr>
				<td class="text-center" colspan="3">Tidak ada data yang mengalami perubahan</td>
			</tr>
			<?php } ?>
		</table>    
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>
<script>
var timer;
function refreshProgress() {
	$.ajax({
		url: "<?php echo site_url('assets/temp/checker.php?file='.session_id()); ?>",
		success:function(data){
			console.log(data);
			$('.progress-bar').attr('aria-valuenow', data.percent).css('width',data.percent+'%');
			$(".status_text").html(data.message);
			$(".response_text").html(data.response.text);
			$(".response_table").html(data.response.table);
			if (data.percent == 100) {
				window.clearInterval(timer);
				timer = window.setInterval(completed, 1000);
			}
		}
	});
}
function completed() {
	$("#message").html("Completed");
	$("#content").html('');
	window.clearInterval(timer);
}
$('.ajax').click(function(){
	$('#status').show();
	var btn = $(this);
	btn.button('loading');
	$.ajax({
		url: '<?php echo site_url('admin/proses_sync'); ?>',
		success: function(response){
			console.log(response);
			btn.button('reset');
			window.clearInterval(timer);
			timer = window.setInterval(completed, 1000);
			$('.progress-bar').attr('aria-valuenow', '100').css('width','100%');
			swal({title:"Sukses", text:"Sinkronisasi Selesai", type:"success", allowOutsideClick: false}).then(function() {
				window.location.replace('<?php echo site_url('admin/sinkronisasi/sync'); ?>');
			}).done();
		}
	});
	timer = window.setInterval(refreshProgress, 1000);
});
</script>