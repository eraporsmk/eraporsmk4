<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><div class="row">
<!-- left column -->
<div class="col-md-12">
<div class="box box-info">
	<div class="box-header">
	<h3 class="box-title">Sinkronisasi <?php echo $page_title; ?></h3>
	</div>
    <div class="box-body">
	<table class="table table-bordered table-striped table-hover">
	<!--table class="table table-bordered table-striped table-hover" style="display:none;"-->
            <thead>
				<tr>
					<th class="text-center" style="vertical-align: middle;">No</th>
					<th class="text-center">jurusan_id</th>
					<th class="text-center">jurusan_induk</th>
					<th class="text-center">nama_jurusan</th>
					<th class="text-center">status</th>
	            </tr>
            </thead>
			<tbody>
			<?php
				$no = $this->uri->segment('4') + 1;
				foreach($dapodik as $data){
					//test($data);
					//die();
					$data->created_at = date('Y-m-d H:i:s', strtotime($data->create_date));
					$data->updated_at = date('Y-m-d H:i:s', strtotime($data->last_update));
					$data->deleted_at = ($data->expired_date) ? date('Y-m-d H:i:s', strtotime($data->expired_date)) : NULL;
					unset($data->create_date, $data->last_update, $data->expired_date);
					//test($data);
					//die();
					//die();
					//echo $data->kurikulum_id.'=>'.$data->mata_pelajaran_id.'=>'.$data->tingkat_pendidikan_id;
					//$find = $this->mata_pelajaran_kurikulum->find("kurikulum_id = $data->kurikulum_id AND mata_pelajaran_id = $data->mata_pelajaran_id AND tingkat_pendidikan_id = $data->tingkat_pendidikan_id");
					$this->db->select('*');
					$this->db->from('ref_jurusan');
					$this->db->where("jurusan_id = '$data->jurusan_id'");
					$query = $this->db->get();
					$find = $query->row();
					//test($find);
					if($find){
						//$this->mata_pelajaran->update($find->mata_pelajaran_id, $data);
						$this->db->where('jurusan_id', $find->jurusan_id);
						$this->db->update('ref_jurusan', $data);
						$result = 'update';
						$class = 'table-danger"';
					} else {
						//$this->mata_pelajaran->insert($data);
						$this->db->select('*');
						$this->db->from('ref_jurusan');
						$this->db->where("jurusan_id = '$data->jurusan_induk'");
						$query = $this->db->get();
						$find_induk = $query->row();
						if($find_induk){
							$this->db->insert('ref_jurusan', $data);
							$result = 'insert';
							$class = 'table-success"';
						} else {
							$this->db->insert('ref_jurusan', $data);
							$result = 'ignore';
							$class = 'table-warning"';
						}
					}
			?>
				<tr class="<?php echo $class; ?>">
					<td class="text-center"><?php echo $no++; ?></td>
					<td><?php echo $data->jurusan_id; ?></td>
					<td><?php echo $data->jurusan_induk; ?></td>
					<td><?php echo $data->nama_jurusan; ?></td>
					<td class="text-center"><?php echo $result; ?></td>
				</tr>
			<?php
			//}
			//break; 
			} ?>
			</tbody>
		</table>
    </div><!-- /.box-body -->
	<div class="box-footer text-center">
		<?php echo $pagination; ?>
	</div>
</div><!-- /.box -->
</div>
<script>
$(document).ready(function(){
	$('body').mouseover(function(){
		$(this).css({cursor: 'wait'});
	});
	var cari = $('body').find('.next');
	if(cari.length>0){
		var cari_a = $(cari).find('a');
		var url = $(cari_a).attr('href');
		window.location.replace(url);
	} else {
		window.location.replace('<?php echo site_url('admin/sinkronisasi'); ?>');
	}
})
</script>