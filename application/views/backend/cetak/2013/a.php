<?php
if($mapel_a){
if($check_2018){
	$colspan = 6;
} else {
	$colspan = 10;
}
?>
<tr>
	<td colspan="<?php echo $colspan; ?>" class="strong">Kelompok A</td>
</tr>
<?php
	$i=1;
	foreach($mapel_a as $mapela){
		$nilai_pengetahuan_value	= get_nilai_akhir_siswa($ajaran_id, 1, $rombel_id, $mapela, $s->siswa_id);
		$nilai_keterampilan_value	= get_nilai_akhir_siswa($ajaran_id, 2, $rombel_id, $mapela, $s->siswa_id);
		$deskripsi_pengetahuan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapela, $s->siswa_id,1);
		$deskripsi_keterampilan		= get_deskripsi_nilai($ajaran_id, $rombel_id, $mapela, $s->siswa_id,2);
?>
<tr>
	<td align="center" valign="top"><?php echo $i; ?></td>
	<td valign="top"><?php echo get_nama_mapel_alias($rombel_id, $mapela); ?></td>
	<?php if($check_2018){ 
		$bobot_pengetahuan 			= get_bobot_mapel($ajaran_id, $mapela, $rombel_id, 1);
		$bobot_keterampilan			= get_bobot_mapel($ajaran_id, $mapela, $rombel_id, 2);
		$nilai_akhir_pengetahuan	= $nilai_pengetahuan_value * $bobot_pengetahuan;
		$nilai_akhir_keterampilan	= $nilai_keterampilan_value * $bobot_keterampilan;
		$nilai_akhir				= ($nilai_akhir_pengetahuan + $nilai_akhir_keterampilan) / 100;
		$nilai_akhir				= ($nilai_akhir) ? number_format($nilai_akhir,0) : 0;
	?>
	<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
	<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
	<td valign="top" align="center"><?php echo $nilai_akhir; ?></td>
	<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_akhir); ?></td>
	<?php } else { ?>
	<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapela); ?></td>
	<td valign="top" align="center"><?php echo $nilai_pengetahuan_value; ?></td>
	<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_pengetahuan_value); ?></td>
	<td valign="top"><?php echo $deskripsi_pengetahuan; ?></td>
	<td valign="top" align="center"><?php echo get_kkm($ajaran_id,$rombel_id,$mapela); ?></td>
	<td valign="top" align="center"><?php echo $nilai_keterampilan_value; ?></td>
	<td valign="top" align="center"><?php echo konversi_huruf(get_kkm($ajaran_id,$rombel_id,$mapela),$nilai_keterampilan_value); ?></td>
	<td valign="top"><?php echo $deskripsi_keterampilan; ?></td>
	<?php } ?>
</tr>
<?php
	$i++;
	}
}
?>