<div class="strong text-center">CATATAN PRESTASI YANG PERNAH DICAPAI</div>
<br />
<?php 
$sekolah = $this->sekolah->get($sekolah_id);
$s = $this->siswa->get($siswa_id);
?>
<table border="0" width="100%" id="alamat">
  <tr>
    <td style="width: 25%;padding:5px;">Nama Peserta Didik (Lengkap)</td>
    <td style="width: 5%;">:</td>
    <td style="width: 65%"><?php echo $s->nama; ?></td>
  </tr>
  <tr>
    <td style="width: 25%;padding:5px;">Nama Sekolah</td>
    <td style="width: 5%">:</td>
    <td style="width: 65%"><?php echo $sekolah->nama; ?></td>
  </tr>
  <tr>
    <td style="width: 25%;padding:5px;">Nomor Induk/NISN</td>
    <td style="width: 5%;">:</td>
    <td style="width: 65%"><?php echo $s->no_induk.' / '.$s->nisn; ?></td>
  </tr>
</table>
<table border="1" width="100%" class="table">
	<thead>
		<tr>
			<th width="5%" class="text-center">No.</th>
			<th width="30%">Prestasi yang Pernah Dicapai</th>
			<th width="65%">Keterangan</th>
		</tr>
	</thead>
	<tbody>
		<?php $prestasi = array('Kurikuler', 'Ekstra Kurikuler', 'Catatan Khusus Lainnya');
		$i=1;
		foreach($prestasi as $pres){
			$prestasi = 0;
			if($check_2018){
				$get_prestasi = $this->prestasi->find_all("semester_id = $ajaran_id AND rombongan_belajar_id = '$rombel_id' AND siswa_id = '$siswa_id' AND jenis_prestasi='$pres'");
			}
		?>
		<tr>
			<td class="text-center"><?php echo $i; ?></td>
			<td><?php echo $pres; ?></td>
			<td><br />
				<?php 
				$no = 1;
				if($get_prestasi){
					foreach($get_prestasi as $get_pres){
						echo '<p><u>'.$get_pres->keterangan_prestasi.'</u></p><br />';
						$no++;
					}
				}
				for($a=$no;$a<=9;$a++){
				?>
				<p>________________________________________________________________________________________________</p><br />
				<?php } /*?>
				<p>________________________________________________________________________________________________</p><br />
				<p>________________________________________________________________________________________________</p><br />
				<p>________________________________________________________________________________________________</p><br />
				<p>________________________________________________________________________________________________</p><br />
				<p>________________________________________________________________________________________________</p><br />
				<p>________________________________________________________________________________________________</p><br />
				<p>________________________________________________________________________________________________</p><br />
				<p>________________________________________________________________________________________________</p><br />
				*/ ?>
				<br />
			</td>
		</tr>
		<?php $i++;} ?>
	</tbody>
</table>