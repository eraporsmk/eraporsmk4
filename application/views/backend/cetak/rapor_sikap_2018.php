<?php
$uri = $this->uri->segment_array();
if(isset($uri[3])){
	if($uri[3] == 'review_rapor'){
		$atribute = ' class="table table-bordered"';
		$atribute_2 = ' class="table table-bordered"';
	} else {
		$atribute = ' border="0" width="100%"';
		$atribute_2 = ' width="100%" border="1" style="margin-left:20px;"';
	}
}
$sekolah = $this->sekolah->get($sekolah_id);
$setting = $this->settings->get(1);
$s = $this->siswa->get($siswa_id);
$rombel = $this->rombongan_belajar->get($rombel_id);
$ajaran = $this->semester->get($ajaran_id);
?>
<table<?php echo $atribute; ?>>
	<tr>
    	<td style="width: 25%;padding-top:5px; padding-bottom:5px;">Nama Peserta Didik</td>
		<td style="width: 1%;" class="text-center">:</td>
		<td style="width: 74%"><?php echo $s->nama; ?></td>
	</tr>
	<tr>
		<td>Nomor Induk/NISN</td>
		<td class="text-center">:</td>
		<td><?php echo $s->no_induk.' / '.$s->nisn; ?></td>
	</tr>
	<tr>
		<td>Kelas</td>
		<td class="text-center">:</td>
		<td><?php echo $rombel->nama; ?></td>
	</tr>
	<tr>
		<td>Tahun Pelajaran</td>
		<td class="text-center">:</td>
		<td><?php echo $ajaran->tahun; ?></td>
	</tr>
	<tr>
		<td>Semester</td>
		<td class="text-center">:</td>
		<td><?php echo ($ajaran->semester == 1) ? 'Ganjil' : 'Genap'; ?></td>
	</tr>
	<?php /*
  <tr>
    <td style="width: 20%;padding-top:5px; padding-bottom:5px;">Nama Sekolah</td>
    <td style="width: 1%;" class="text-center">:</td>
    <td style="width: 35%"><?php echo $sekolah->nama; ?></td>
	
  </tr>
  <tr>
    <td style="width: 20%;padding-top:5px; padding-bottom:5px;">Alamat</td>
    <td style="width: 1%;" class="text-center">:</td>
    <td style="width: 35%"><?php echo $sekolah->alamat; ?></td>
	<td style="width: 20%;">Semester</td>
    <td style="width: 1%;" class="text-center">:</td>
    <td style="width: 25%"><?php echo ($ajaran->semester == 1) ? '1 (Satu)' : '2 (Dua)'; ?></td>
  </tr>
  <tr>
    <td style="width: 20%;padding-top:5px; padding-bottom:5px;">Nomor Induk/NISN</td>
    <td style="width: 1%;" class="text-center">:</td>
    <td style="width: 35%"><?php echo $s->no_induk.' / '.$s->nisn; ?></td>
	<td style="width: 20%;"><?php if($rombel->tingkat == 10){ ?>Kompetensi Keahlian<?php } else { ?>Program Keahian<?php } ?></td>
    <td style="width: 1%;" class="text-center">:</td>
    <td style="width: 25%"><?php echo get_jurusan($rombel->jurusan_id); ?></td>
  </tr>
  */ ?>
</table><br>