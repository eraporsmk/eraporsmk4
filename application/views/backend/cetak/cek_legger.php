<?php
$nama_kompetensi = 'PENGETAHUAN';
if($kompetensi_id == 2){
	$nama_kompetensi = 'KETERAMPILAN';
}
$filename='LEGGER_'.$nama_kompetensi.'_'.str_replace(' ','_',$nama_rombel->nama).'.xls'; //save our workbook as this file name
//echo $filename;
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=hasil.xls");
//header("Content-Disposition: attachment; filename=$filename");
?>
	<table class="table table-bordered table-hover" border="1" width="100%">
		<thead>
			<tr>
				<th class="text-center">NO</th>
				<th class="text-center">NISN</th>
				<th class="text-center">NAMA PD</th>
				<?php foreach($data_mapel as $mapel){ ?>
				<th class="text-center"><?php echo get_nama_mapel($mapel->mata_pelajaran_id); ?></th>
				<?php } ?>
				<th class="text-center" rowspan="2">JUMLAH</th>
				<th class="text-center" rowspan="2">RATA-RATA</th>
			</tr>
			<tr>
				<th colspan="3" class="text-center">KKM</th>
				<?php foreach($data_mapel as $mapel){ ?>
				<th class="text-center"><?php echo get_kkm($ajaran->id,$nama_rombel->rombongan_belajar_id,$mapel->mata_pelajaran_id); ?></th>
				<?php } ?>
			</tr>
		</thead>
		<tbody>
			<?php
			$i=1;
			foreach($data_siswa['data'] as $siswa){
			?>
			<tr>
				<td><?php echo $i; ?></td>
				<td>'<?php echo $siswa->nisn; ?></td>
				<td><?php echo strtoupper($siswa->nama); ?></td>
				<?php 
				$total_nilai = 0;
				$jumlah_mapel = 0;
				foreach($data_mapel as $key=>$mapel){
					$nilai_value	= get_nilai_akhir_siswa($ajaran->id, $kompetensi_id, $nama_rombel->rombongan_belajar_id, $mapel->mata_pelajaran_id, $siswa->siswa_id);
					$total_nilai += $nilai_value;
					if($nilai_value){
						$jumlah_mapel++;
					}
				?>
				<td><?php echo $nilai_value ?></td>
				<?php } ?>
				<td><?php echo number_format($total_nilai,0,',','.'); ?></td>
				<td><?php echo number_format($total_nilai / $jumlah_mapel,0,',','.'); ?></td>
			</tr>
			<?php $i++;
			} ?>
		</tbody>
	</table>