<style>
.spasi_setengah{margin-bottom:5px;}
.lurus{text-align:justify;}
ol.kosong{margin-left:0px;}
</style>
<div class="strong text-center">PETUNJUK PENGISIAN</div>
<br />
<div class="lurus">
<ol class="kosong">
	<li class="spasi_setengah">Rapor merupakan ringkasan hasil penilaian terhadap seluruh aktivitas pembelajaran yang dilakukan peserta didik dalam kurun waktu tertentu;</li>
	<li class="spasi_setengah">Rapor dipergunakan selama peserta didik yang bersangkutan mengikuti seluruh program pembelajaran di Sekolah Menengah Kejuruan tersebut;</li>
	<li class="spasi_setengah">Identitas Sekolah diisi dengan data yang sesuai dengan keberadaan Sekolah Menengah Kejuruan, penulisan nama sekolah ditulis menggunakan dengan Kapital Ondercast di setiap awal kata contoh (SMK Nusa Bangsa), untuk halaman depan di tulis dengan huruf kapital;</li>
	<li class="spasi_setengah">Keterangan tentang diri  Peserta didik diisi lengkap sesuai ijazah sebelumnya atau akta kelahiran;</li>
	<li class="spasi_setengah">Rapor harus dilengkapi dengan pas foto berwarna dengan latar belakang merah (3 x 4) serta menggunakan baju putih seragam dan pengisiannya dilakukan oleh Wali Kelas;</li>
	<li class="spasi_setengah">Capaian peserta didik dalam kompetensi pengetahuan dan kompetensi keterampilan ditulis dalam bentuk angka dan predikat untuk masing-masing mata pelajaran;</li>
	<li class="spasi_setengah">Predikat ditulis dalam bentuk huruf sesuai kriteria;</li>
	<li class="spasi_setengah">Catatan akademik ditulis dengan kalimat positif sesuai capaian yang diperoleh peserta didik;</li>
	<li class="spasi_setengah">Penjelasan lebih detil mengenai capaian kompetensi peserta didik dapat dilihat pada leger</li>
	<li class="spasi_setengah">Laporan Praktik Kerja Lapangan diisi berdasarkan kegiatan praktik kerja yang diikuti oleh peserta didik di industri/perusahaan mitra;</li>
	<li class="spasi_setengah">Laporan Ekstrakurikuler diisi berdasarkan kegiatan ekstrakurikuler yang diikuti oleh peserta didik;</li>
	<li class="spasi_setengah">Ketidakhadiran diisi dengan data akumulasi ketidakhadiran peserta didik karena sakit, izin, atau tanpa keterangan selama satu semester.</li>
	<li class="spasi_setengah">Keterangan kenaikan kelas diisi dengan putusan apakah peserta didik naik kelas yang ditentukan melalui rapat dewan guru.</li>
	<li class="spasi_setengah">Deskripsi perkembangan karakter diisi dengan simpulan perkembangan peserta didik terkait penumbuhan karakter baik yang dilakukan secara terprogram oleh sekolah maupun yang muncul secara spontan dari peserta didik</li>
	<li class="spasi_setengah">Catatan perkembangan karakter diisikan hal-hal yang tidak tercantum pada deskripsi perkembangan karakter termasuk prestasi yang diraih peserta didik pada semester berjalan dan simpulan dari perkembangan karakter peserta didik pada semester berjalan jika dikomparasi dengan semester sebelumnya</li>
</ol>
</div>