<?php
$data_siswa = get_siswa_by_rombel($ekskul_id);
$get_ekskul = $this->ekstrakurikuler->find_by_rombongan_belajar_id($ekskul_id);
$get_all_rombel = $this->rombongan_belajar->find_all("sekolah_id = '$get_ekskul->sekolah_id' AND semester_id = $get_ekskul->semester_id AND rombongan_belajar_id IN(SELECT rombongan_belajar_id FROM anggota_rombel WHERE sekolah_id = '$get_ekskul->sekolah_id' AND semester_id = $get_ekskul->semester_id AND siswa_id IN(SELECT siswa_id FROM anggota_rombel WHERE sekolah_id = '$get_ekskul->sekolah_id' AND semester_id = $get_ekskul->semester_id AND rombongan_belajar_id = '$ekskul_id'))", '*', 'tingkat asc, kurikulum_id asc');
//test($get_all_rombel);
?>
<input type="hidden" id="rombel_ekskul_id" value="<?php echo $ekskul_id; ?>" />
<div class="row" style="margin-bottom:10px;">
	<div class="col-lg-6">
		<select id="rombel_reguler" class="form-control select2">
			<option value="">Semua Rombel</option>
			<?php foreach($get_all_rombel as $rombel){?>
			<option value="<?php echo $rombel->rombongan_belajar_id; ?>"><?php echo $rombel->nama; ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="table-responsive no-padding">
	<table class="table table-bordered table-hover" id="result_2">
		<thead>
			<tr>
				<th width="20%">Nama Siswa</th>
				<th width="10%">Kelas</th>
				<th width="20%">Predikat</th>
				<th width="50%">Deskripsi</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			if($data_siswa){
				//test($get_ekskul);
				$nama_guru = ($get_ekskul) ? get_nama_guru($get_ekskul->guru_id) : 0;
				echo '<input type="hidden" id="nama_guru" value="('.$nama_guru.')" />';
				foreach($data_siswa['data'] as $siswa){
					//test($siswa);
					//die();
					$siswa_id = $siswa->siswa_id;
					//$find_anggota_rombel = $this->anggota_rombel->find("semester_id = $ajaran_id AND siswa_id = '$siswa_id'");
					//test(find_anggota_rombel);
					$nilai_ekskul = $this->nilai_ekstrakurikuler->find("semester_id = $ajaran_id AND ekstrakurikuler_id = '$get_ekskul->ekstrakurikuler_id' AND siswa_id = '$siswa_id'");
			?>
			<tr>
				<td>
					<input type="hidden" name="siswa_id[]" value="<?php echo $siswa->siswa_id; ?>" />
					<?php echo strtoupper($siswa->nama); ?>
				</td>
				<td class="text-center">
					<input type="hidden" name="rombel_id[]" value="<?php echo get_rombel_siswa($get_ekskul->semester_id, $siswa->siswa_id, 'id'); ?>" />
					<?php echo get_rombel_siswa($get_ekskul->semester_id, $siswa->siswa_id); ?>
				</td>
				<td>
					<select name="nilai[]" class="form-control" id="nilai_ekskul">
						<option value="">== Pilih Predikat ==</option>
						<option value="1"<?php echo (isset($nilai_ekskul->nilai) && $nilai_ekskul->nilai == 1) ? 'selected="selected"' : ''; ?>>Sangat Baik</option>
						<option value="2"<?php echo (isset($nilai_ekskul->nilai) && $nilai_ekskul->nilai == 2) ? 'selected="selected"' : ''; ?>>Baik</option>
						<option value="3"<?php echo (isset($nilai_ekskul->nilai) && $nilai_ekskul->nilai == 3) ? 'selected="selected"' : ''; ?>>Cukup</option>
						<option value="4"<?php echo (isset($nilai_ekskul->nilai) && $nilai_ekskul->nilai == 4) ? 'selected="selected"' : ''; ?>>Kurang</option>
					</select>
				</td>
				<td><input type="text" class="form-control" id="deskripsi_ekskul" name="deskripsi_ekskul[]" value="<?php echo isset($nilai_ekskul->deskripsi_ekskul) ? $nilai_ekskul->deskripsi_ekskul : ''; ?>" /></td>
			</tr>
			<?php
				}
			} else {
			?>
			<tr>
				<td colspan="4" class="text-center">Tidak ada data siswa di rombongan belajar terpilih</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>
<script>
$('select#nilai_ekskul').change(function(e) {
	e.preventDefault();
	var ini = $(this).val();
	var nama_guru = $('#nama_guru').val();
	//nama_guru = nama_guru.toLowerCase();
	//console.log(nama_guru);
	var nama_ekskul = $("#ekskul option:selected").text();
	nama_ekskul = nama_ekskul.replace(nama_guru, '');
	nama_ekskul = nama_ekskul.toLowerCase();
	var nilai_ekskul = $(this).find("option:selected").text();
	nilai_ekskul = nilai_ekskul.toLowerCase();
	if(ini == ''){
		$(this).closest('td').next('td').find('input').val('');
	} else {
		$(this).closest('td').next('td').find('input').val('Melaksanakan kegiatan '+nama_ekskul+' dengan '+nilai_ekskul);
	}
});
$('#rombel_reguler').change(function(e) {
	e.preventDefault();
	var ini = $(this).val();
	var rombel_ekskul_id = $('#rombel_ekskul_id').val();
	console.log(ini);
	console.log(rombel_ekskul_id);
	$.ajax({
		url: '<?php echo site_url('admin/ajax/filter_rombel_ekskul'); ?>',
		type: 'post',
		data: {rombongan_belajar_id:ini, rombel_ekskul_id:rombel_ekskul_id},
		success: function(response){
			$('#result_2').html(response);
		}
	});
});
</script>