<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
		<!-- form start -->
            <?php 
			$attributes = array('class' => 'form-horizontal', 'id' => 'myform');
			echo form_open($form_action,$attributes);
			$ajaran = get_ta();
			$loggeduser = $this->ion_auth->user()->row();
			$data_rombel = $this->rombongan_belajar->find("guru_id = '$guru_id' AND semester_id = $ajaran->id");
			//Datarombel::find_by_guru_id_and_ajaran_id($guru_id, $ajaran->id);
			$data_siswa = get_siswa_by_rombel($data_rombel->rombongan_belajar_id);
			?>
			<input type="hidden" name="ajaran_id" value="<?php echo $ajaran->id; ?>" />
			<input type="hidden" name="rombel_id" value="<?php echo $data_rombel->rombongan_belajar_id; ?>" />
			<div class="table-responsive no-padding">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th width="30%">Nama Peserta Didik</th>
							<th width="70%">Deskripsi</th>
						</tr>
					</thead>
					<tbody>
					<?php
					if($data_siswa){
						$style = '';
						foreach($data_siswa['data'] as $siswa){
						$siswa_id = $siswa->siswa_id;
					?>
					<?php
					$catatan_wali = $this->catatan_wali->find("semester_id = $ajaran->id AND rombongan_belajar_id = '$data_rombel->rombongan_belajar_id' AND siswa_id = '$siswa_id'");
					//Catatanwali::find_by_ajaran_id_and_rombel_id_and_siswa_id($ajaran->id,$data_rombel->id,$siswa->id);
					$data_deskripsi = '';
					if($catatan_wali){
						$data_deskripsi .= $catatan_wali->uraian_deskripsi;
					}
					?>
					<tr>
						<td>
							<input type="hidden" name="siswa_id[]" value="<?php echo $siswa->siswa_id; ?>" /> 
							<?php echo strtoupper($siswa->nama).'<br />'; ?>
							<?php echo $siswa->nisn.'<br />'; ?>
							<?php $date = date_create($siswa->tanggal_lahir);
							echo date_format($date,'d/m/Y'); ?><br />
							<?php
							$this->db->select('*');
							$this->db->from('nilai_akhir as a');
							$this->db->join('mata_pelajaran as b', 'b.mata_pelajaran_id = a.mata_pelajaran_id');
							$this->db->where('a.sekolah_id', $sekolah_id);
							$this->db->where('a.semester_id', $semester_id);
							$this->db->where('a.rombongan_belajar_id', $rombongan_belajar_id);
							$this->db->where('a.siswa_id', $siswa_id);
							$this->db->where('a.kompetensi_id', 1);
							$this->db->where('a.deleted_at IS NULL');
							$this->db->order_by('a.nilai', 'asc');
							$this->db->limit(3);
							$query_nilai_akhir = $this->db->get();
							$all_nilai_akhir = $query_nilai_akhir->result();
							?>
							<span class="label label-success">3 (Tiga) Nilai Akhir Terendah</span>
							<table class="table table-bordered">
								<thead>
									<tr>
										<th style="vertical-align:middle;">Mata Pelajaran</th>
										<th class="text-center">Nilai</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									if($all_nilai_akhir){
										foreach($all_nilai_akhir as $nilai_akhir){
										$get_pembelajaran = $this->pembelajaran->find("sekolah_id = '$sekolah_id' AND semester_id = $ajaran->id AND rombongan_belajar_id = '$nilai_akhir->rombongan_belajar_id' AND mata_pelajaran_id = $nilai_akhir->mata_pelajaran_id");
										$rasio_p = ($get_pembelajaran) ? $get_pembelajaran->rasio_p : 50;
										$rasio_k = ($get_pembelajaran) ? $get_pembelajaran->rasio_k : 50;
										$nilai_akhir_keterampilan = $this->nilai_akhir->find("sekolah_id = '$sekolah_id' AND semester_id = $ajaran->id AND kompetensi_id = 2 AND rombongan_belajar_id = '$nilai_akhir->rombongan_belajar_id' AND mata_pelajaran_id = $nilai_akhir->mata_pelajaran_id AND siswa_id = '$siswa_id'");
										$nilai_keterampilan = ($nilai_akhir_keterampilan) ? $nilai_akhir_keterampilan->nilai : 0;
										$nilai_pengetahuan = $nilai_akhir->nilai;
										$nilai_akhir_pengetahuan	= $nilai_pengetahuan * $rasio_p;
										$nilai_akhir_keterampilan	= $nilai_keterampilan * $rasio_k;
										$nilai_akhir_result			= ($nilai_akhir_pengetahuan + $nilai_akhir_keterampilan) / 100;
										$nilai_akhir_result			= ($nilai_akhir_result) ? number_format($nilai_akhir_result,0) : 0;
									?>
									<tr>
										<td width="75%"><?php echo $nilai_akhir->nama; ?></td>
										<td width="15%" class="text-center"><?php echo $nilai_akhir_result; ?></td>
									</tr>
									<?php
										}
									} else {
									?>
									<tr>
										<td class="text-center" colspan="2">Belum dilakukan penilaian</td>
									</tr>
									<?php
									}
									?>
								</tbody>
							</table>
						</td>
						<td>
							<textarea name="uraian_deskripsi[]" class="editor" style="width: 100%; height: 100%; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?php echo $data_deskripsi; ?></textarea>
						</td>
					</tr>
				<?php } } else { 
						$style = ' style="display:none;"';
				?>
					<tr>
						<td colspan="2">Belum ada anggota rombel di kelas <?php echo get_nama_rombel($data_rombel->id); ?></td>
					</tr>
				<?php } ?>
				</tbody>
			</table>
		</div>
              <!-- /.box-body -->
			<div class="box-footer">
				<button type="submit" class="btn btn-success"<?php echo $style; ?>>Simpan</button>
			</div>
            <?php echo form_close();  ?>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>