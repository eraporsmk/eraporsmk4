<?php
if($check_2018){
	$text_kkm = 'SKM';
} else {
	$text_kkm = 'SKM';
}
?>
<div class="row">
	<div class="col-lg-12 col-xs-12">
		<section id="mata-pelajaran">
			<h4 class="page-header">Progres Perencanaan dan Penilaian</h4>
			<div class="row" style="margin-bottom:10px;">
		<?php 
			$tingkat_pendidikan = $this->tingkat_pendidikan->find_all("tingkat_pendidikan_id IN (10,11,12,13)");
			$get_jurusan = $this->jurusan_sp->find_all("sekolah_id = '$sekolah_id'");
		?>
			<div class="col-md-4">
				<select id="filter_jurusan" class="form-control">
					<option value="">==Filter Berdasar Kompetensi Keahlian==</option>
					<?php foreach($get_jurusan as $jurusan){ ?>
					<option value="<?php echo $jurusan->jurusan_id; ?>"><?php echo get_jurusan($jurusan->jurusan_id); ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-4">
				<select id="filter_tingkat" class="form-control" style="display:none;">
					<option value="">==Filter Berdasar Tingkat==</option>
					<?php foreach($tingkat_pendidikan as $tingkat){ ?>
					<option value="<?php echo $tingkat->tingkat_pendidikan_id; ?>"><?php echo $tingkat->nama; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-4">
				<select id="filter_rombel" class="form-control" style="display:none;"></select>
			</div>
		</div>
			<div class="row">
				<div class="col-lg-12 col-xs-12" style="margin-bottom:20px;">
					<table id="datatable" class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th style="vertical-align:middle;" class="text-center" rowspan="2">Rombel</th>
								<th rowspan="2" style="vertical-align:middle;">Mata Pelajaran</th>
								<th rowspan="2" style="vertical-align:middle;">Guru Mata Pelajaran</th>
								<th class="text-center" rowspan="2" style="vertical-align:middle;"><?php echo $text_kkm; ?></th>
								<th class="text-center" colspan="2">Jumlah Rencana Penilaian</th>
								<th class="text-center" colspan="2">Jumlah Rencana Telah Dinilai</th>
							</tr>
							<tr>
								<th class="text-center">Pengetahuan</th>
								<th class="text-center">Keterampilan</th>
								<th class="text-center">Pengetahuan</th>
								<th class="text-center">Keterampilan</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</div>
</div>