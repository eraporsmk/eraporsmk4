<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
<?php
$attributes = array('class' => 'form-horizontal', 'id' => 'myform');
echo form_open($form_action,$attributes);
?>
<input type="hidden" class="form-control" name="guru_id" value="<?php echo $guru_id; ?>" />
<input type="hidden" class="form-control" name="semester_id" value="<?php echo $semester_id; ?>" />
<div class="table-responsive no-padding">
	<table class="table table-bordered table-hover">
		<thead>
			<tr>
				<!--th rowspan="2" style="vertical-align:middle">Rombongan Belajar</th-->
				<th rowspan="2" style="vertical-align:middle">Nama Mata Pelajaran</th>
				<th width="10%" colspan="2" class="text-center">Rasio</th>
			</tr>
			<tr>
				<th>Pengetahuan</th>
				<th>Keterampilan</th>
			</tr>
		</thead>
		<tbody>
			<?php 
			if($all_pembelajaran){
				foreach($all_pembelajaran as $pembelajaran){
					$all_mapel[$pembelajaran->mata_pelajaran_id][] = get_nama_mapel($pembelajaran->mata_pelajaran_id);
					$all_rasio_p[$pembelajaran->mata_pelajaran_id] = $pembelajaran->rasio_p;
					$all_rasio_k[$pembelajaran->mata_pelajaran_id] = $pembelajaran->rasio_k;
				}
				//test($all_mapel);
				//test($all_rasio_p);
				//test($all_rasio_k);
				foreach($all_mapel as $key=>$mapel){
				//$nama_rombel = get_nama_rombel($pembelajaran->rombongan_belajar_id);
				//if($nama_rombel == '-'){
					//$this->pembelajaran->delete($pembelajaran->pembelajaran_id);
				//}
			?>
			<tr>
				<!--td><?php //echo $nama_rombel; ?></td-->
				<td>
					<?php echo get_nama_mapel($key); ?>
					<input type="hidden" class="form-control" name="mata_pelajaran_id[<?php echo $key; ?>]" value="<?php echo $key; ?>" />
				</td>
				<td><input type="text" class="form-control" name="rasio_p[<?php echo $key; ?>]" value="<?php echo $all_rasio_p[$key]; ?>" /></td>
				<td><input type="text" class="form-control" name="rasio_k[<?php echo $key; ?>]" value="<?php echo $all_rasio_k[$key]; ?>" /></td>
			</tr>
			<?php
				}
			} else {
			?>
			<tr>
				<td colspan="4" class="text-center">Anda tidak mengampu mata pelajaran</td>
			</tr>
			<?php
			}
			?>
		</tbody>
	</table>
</div>
			<div class="box-footer">
				<p>Jumlah rasio pengetahuan dan keterampilan harus sama dengan 100 (seratus)</p>
				<button type="submit" class="btn btn-success">Simpan</button>
			</div>
            <?php echo form_close();  ?>
</div>
</div>
</div>