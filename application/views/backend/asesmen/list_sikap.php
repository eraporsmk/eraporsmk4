<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
		<table id="datatable" class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th style="width: 20%">Nama PD</th>
				<th style="width: 10%">Rombel/Tingkat</th>
				<!--th style="width: 20%">Mata Pelajaran</th-->
                <th style="width: 10%" class="text-center">Butir Sikap</th>
                <th style="width: 10%" class="text-center">Opsi Sikap</th>
                <th style="width: 35%" class="text-center">Uraian Sikap</th>
            </tr>
            </thead>
			<tbody>
			</tbody>
		</table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>