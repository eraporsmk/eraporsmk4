<?php
$get_gelar_depan_guru = $this->gelar_ptk->find_all("guru_id = '$guru->guru_id' AND gelar_akademik_id IN (SELECT gelar_akademik_id FROM ref_gelar WHERE posisi_gelar = 1)");
if($get_gelar_depan_guru){
	foreach($get_gelar_depan_guru as $gelar_depan){
		$gelar_depan_guru[] = $gelar_depan->gelar_akademik_id;
	}
} else {
	$gelar_depan_guru = array();
}
$get_gelar_belakang_guru = $this->gelar_ptk->find_all("guru_id = '$guru->guru_id' AND gelar_akademik_id IN (SELECT gelar_akademik_id FROM ref_gelar WHERE posisi_gelar = 2)");
if($get_gelar_belakang_guru){
	foreach($get_gelar_belakang_guru as $gelar_belakang){
		$gelar_belakang_guru[] = $gelar_belakang->gelar_akademik_id;
	}
} else {
	$gelar_belakang_guru = array();
}
?>
<form id="form_gelar">
<input type="hidden" name="edit_gelar" value="1" />
<div class="row">
	<div class="form-group col-xs-12">
		<label>Gelar Depan</label>
		<select name="gelar_depan[]" multiple="multiple" class="form-control select2" style="width:100%">
			<option value="">== Pilih Gelar Depan ==</option>
			<?php
			$gelar_depan = $this->gelar->find_all('posisi_gelar = 1');
			foreach ($gelar_depan as $depan): ?>
			<option value="<?php echo $depan->gelar_akademik_id;?>"<?php if(in_array($depan->gelar_akademik_id, $gelar_depan_guru)){ echo ' selected'; } else { echo ''; } ?>><?php echo $depan->kode;?> (<?php echo $depan->nama;?>)</option>
			<?php endforeach?>
		</select>
	</div>
	<div class="form-group col-xs-12">
		<label>Gelar Belakang</label>
		<select name="gelar_belakang[]" multiple="multiple" class="form-control required select2" style="width:100%">
			<option value="">== Pilih Gelar Belakang ==</option>
			<?php
			$gelar_depan = $this->gelar->find_all('posisi_gelar = 2');
			foreach ($gelar_depan as $depan): ?>
			<option value="<?php echo $depan->gelar_akademik_id;?>"<?php if(in_array($depan->gelar_akademik_id, $gelar_belakang_guru)){ echo ' selected'; } else { echo ''; } ?>><?php echo $depan->kode;?> (<?php echo $depan->nama;?>)</option>
			<?php endforeach?>
		</select>
	</div>
</div>
</form>
<script>
$('.select2').select2();
$('a.edit_gelar').click(function(){
	$.ajax({
		url: '<?php echo site_url('admin/data_guru/edit_gelar/'.$guru->guru_id); ?>',
		type: 'post',
		data: $("#form_gelar").serialize(),
		success: function(response){
			$('#modal_content').modal('hide');
			swal({title:"Berhasil!",text:"Gelar berhasil diperbaharui.",type:"success"}).then(function() {
				$('#datatable').dataTable().fnReloadAjax();
			});
		}
	});
});
</script>