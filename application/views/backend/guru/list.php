<?php
$admin_group = array(1,2);
?>
<div class="row">
<!-- left column -->
<div class="col-md-12">
<?php echo ($this->session->flashdata('error')) ? error_msg($this->session->flashdata('error')) : ''; ?>
<?php echo ($this->session->flashdata('success')) ? success_msg($this->session->flashdata('success')) : ''; ?>
<div class="box box-info">
    <div class="box-body">
        <table id="datatable" class="table table-bordered table-striped table-hover">
            <thead>
            <tr>
                <th width="30%">Nama</th>
                <th style="width: 3%" class="text-center">L/P</th>
                <th width="15%">Tempat, Tanggal Lahir</th>
                <!--th style="width: 10%" class="text-center">Mata Pelajaran</th-->
				<th style="width: 15%" class="text-center">Email</th>
                <th style="width: 10%" class="text-center">Tindakan</th>
            </tr>
            </thead>
			<tbody>
			</tbody>
        </table>
    </div><!-- /.box-body -->
</div><!-- /.box -->
</div>
</div>