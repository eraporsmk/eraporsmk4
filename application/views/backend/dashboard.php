<?php
$setting = $this->settings->get(1);
$sekolah = $this->sekolah->get($sekolah_id);
?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-solid" style="padding-bottom:25px;">
		    <div class="box-header with-border">
        		<h3 class="text-center box-title">Selamat Datang <?php echo $detil_user->username; ?></h3>
	    	</div><!-- /.box-header -->
    		<div class="box-body">
			<?php
			$super_admin = array(1,2);
			if($this->ion_auth->in_group($super_admin)){
				$this->load->view('backend/dashboard/admin');
			}
			if ($this->ion_auth->in_group('siswa')){
				$this->load->view('backend/dashboard/siswa');
			}
			if ($this->ion_auth->in_group('guru')){
				$this->load->view('backend/dashboard/guru');
			} 
			if ($this->ion_auth->in_group('bk')){
				$this->load->view('backend/dashboard/bk');
			} 
			if ($this->ion_auth->in_group('kasek')){
				$this->load->view('backend/dashboard/kasek');
			} ?>
			</div><!-- /.box-body -->
			<div style="clear:both;"></div>
		</div><!--/-->
		<h5>Aplikasi <strong>e-Rapor SMK BISA</strong> ini dibuat dan dikembangkan oleh Direktorat Pembinaan Sekolah Menengah Kejuruan</h5>
		<h5>Kementerian Pendidikan dan Kebudayaan Republik Indonesia</h5>
	</div>
</div>