<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Kd_nilai_model extends MY_Model{
	public $_table = 'kd_nilai';
	public $primary_key = 'kd_nilai_id';
	public $belongs_to = array(
		'kompetensi_dasar' => array('model' => 'kompetensi_dasar_model', 'primary_key' => 'kompetensi_dasar_id'),
	);//1 ke 1
	public $before_create = array( 'timestamps' );
	public $before_update = array( 'timestamps' );
    protected function timestamps($data){
        $loggeduser = $this->ion_auth->user()->row();
        $data['last_sync'] = date('Y-m-d H:i:s');
		$data['sekolah_id'] = $loggeduser->sekolah_id;
        return $data;
    }
}