<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_mst_wilayah_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$sql = "CREATE TABLE mst_wilayah (
				kode_wilayah character(8) NOT NULL,
				nama character varying(60) NOT NULL,
				id_level_wilayah smallint NOT NULL,
				mst_kode_wilayah character(8),
				negara_id character(2) NOT NULL,
				asal_wilayah character(8),
				kode_bps character(7),
				kode_dagri character(7),
				kode_keu character varying(10),
				created_at timestamp(0) without time zone NOT NULL,
				updated_at timestamp(0) without time zone NOT NULL,
				deleted_at timestamp(0) without time zone,
				last_sync timestamp(0) without time zone NOT NULL,
				CONSTRAINT mst_wilayah_pkey PRIMARY KEY (kode_wilayah),
				CONSTRAINT mst_wilayah_id_level_wilayah_fkey FOREIGN KEY (id_level_wilayah)
					REFERENCES level_wilayah (id_level_wilayah) MATCH SIMPLE
					ON UPDATE NO ACTION ON DELETE NO ACTION,
				CONSTRAINT mst_wilayah_mst_kode_wilayah_fkey FOREIGN KEY (mst_kode_wilayah)
					REFERENCES mst_wilayah (kode_wilayah) MATCH SIMPLE
					ON UPDATE NO ACTION ON DELETE NO ACTION,
				CONSTRAINT mst_wilayah_negara_id_fkey FOREIGN KEY (negara_id)
					REFERENCES negara (negara_id) MATCH SIMPLE
					ON UPDATE NO ACTION ON DELETE NO ACTION
				)
				WITH ( OIDS=FALSE );";
		$this->db->query($sql);
		$kurikulum = array('000000', '030817AN', '050911AA', '060713AB', '072605AK', '111011AN', '150918AD', '196204AG', '241601AI', '270721');
		foreach($kurikulum as $key => $kur){
			$this->db->select('*');
			$this->db->from('mst_wilayah');
			$this->db->where('kode_wilayah',$kur);
			$query = $this->db->get();
			$result = $query->row();
			if(!$result){
				include_once APPPATH."/migrations/referensi/mst_wilayah($key).php";
				$this->db->insert_batch('mst_wilayah', $mst_wilayah);
			}
			sleep(1);
		}
	}
	public function down(){
		$this->dbforge->drop_table('mst_wilayah', TRUE);
	}
}