<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_jurusan_sp_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'jurusan_sp_id'	=> array(
				'type' => 'uuid',
			),
			'jurusan_sp_id_dapodik'	=> array(
				'type' => 'uuid',
			),
			'sekolah_id'	=> array(
				'type' => 'uuid',
			),
			'jurusan_id'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 25
			),
			'nama_jurusan_sp'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 60
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('jurusan_sp_id', TRUE);
		$this->dbforge->create_table('jurusan_sp',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('jurusan_sp', TRUE);
	}
}