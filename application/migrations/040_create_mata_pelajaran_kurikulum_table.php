<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_mata_pelajaran_kurikulum_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$sql = "CREATE TABLE mata_pelajaran_kurikulum (
					kurikulum_id smallint NOT NULL,
					mata_pelajaran_id integer NOT NULL,
					tingkat_pendidikan_id numeric(2,0) NOT NULL,
					jumlah_jam numeric(2,0) NOT NULL,
					jumlah_jam_maksimum numeric(2,0) NOT NULL,
					wajib numeric(1,0) NOT NULL,
					sks numeric(2,0) NOT NULL,
					a_peminatan numeric(1,0) NOT NULL,
					area_kompetensi character(1) NOT NULL,
					gmp_id character(36),
					created_at timestamp(0) without time zone NOT NULL,
					updated_at timestamp(0) without time zone NOT NULL,
					deleted_at timestamp(0) without time zone,
					last_sync timestamp(0) without time zone NOT NULL,
					CONSTRAINT mata_pelajaran_kurikulum_pkey PRIMARY KEY (kurikulum_id, mata_pelajaran_id, tingkat_pendidikan_id),
					CONSTRAINT mata_pelajaran_kurikulum_kurikulum_id_fkey FOREIGN KEY (kurikulum_id)
						REFERENCES ref_kurikulum (kurikulum_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION,
					CONSTRAINT mata_pelajaran_kurikulum_mata_pelajaran_id_fkey FOREIGN KEY (mata_pelajaran_id)
						REFERENCES mata_pelajaran (mata_pelajaran_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION,
					CONSTRAINT mata_pelajaran_kurikulum_tingkat_pendidikan_id_fkey FOREIGN KEY (tingkat_pendidikan_id)
						REFERENCES ref_tingkat_pendidikan (tingkat_pendidikan_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION
				)
				WITH ( OIDS=FALSE );";
		$this->db->query($sql);
		$kurikulum = array(
			1 => array(
				'kurikulum_id' 			=> 1,
				'mata_pelajaran_id'		=> 1100,
				'tingkat_pendidikan_id'	=> 1
				),
			2 => array(
				'kurikulum_id' 			=> 175,
				'mata_pelajaran_id'		=> 401231000,
				'tingkat_pendidikan_id'	=> 11
				),
			3 => array(
				'kurikulum_id' 			=> 555,
				'mata_pelajaran_id'		=> 100015000,
				'tingkat_pendidikan_id'	=> 10
				),
			4 => array(
				'kurikulum_id' 			=> 142,
				'mata_pelajaran_id'		=> 300311900,
				'tingkat_pendidikan_id'	=> 10
				),
			5 => array(
				'kurikulum_id' 			=> 356,
				'mata_pelajaran_id'		=> 300311900,
				'tingkat_pendidikan_id'	=> 11
				),
		);
		foreach($kurikulum as $key => $kur){
			$this->db->select('*');
			$this->db->from('mata_pelajaran_kurikulum');
			$this->db->where('kurikulum_id',$kur['kurikulum_id']);
			$this->db->where('mata_pelajaran_id',$kur['mata_pelajaran_id']);
			$this->db->where('tingkat_pendidikan_id',$kur['tingkat_pendidikan_id']);
			$query = $this->db->get();
			$result = $query->row();
			if(!$result){
				include_once APPPATH."/migrations/referensi/mata_pelajaran_kurikulum($key).php";
				$this->db->insert_batch('mata_pelajaran_kurikulum', $mata_pelajaran_kurikulum);
			}
			sleep(1);
		}
	}
	public function down(){
		$this->dbforge->drop_table('mata_pelajaran_kurikulum', TRUE);
	}
}