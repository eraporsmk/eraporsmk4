<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_users_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'user_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'nisn' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'nuptk'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null'	=> true
			),
			'ip_address'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'username'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'password'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 80
			),
			'password_dapo'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 80
			),
			'salt'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null'	=> true
			),
			'email'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 100
			),
			'activation_code'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null'	=> true
			),
			'forgotten_password_code'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null'	=> true
			),
			'forgotten_password_time'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'remember_code'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null'	=> true
			),
			'last_login'	=> array(
				'type' => 'INT',
				'constraint' => 40,
				'null'	=> true
			),
			'active'	=> array(
				'type' => 'INT',
				'constraint' => 11,
				'null'	=> true
			),
			'photo'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 40,
				'null'	=> true
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
				'null'	=> true
			),
			'guru_id'	=> array(
				'type' => 'uuid',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('user_id', TRUE);
		$this->dbforge->create_table('users',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('users', TRUE);
	}
}