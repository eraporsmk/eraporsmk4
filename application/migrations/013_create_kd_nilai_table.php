<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_kd_nilai_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'kd_nilai_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'rencana_penilaian_id' => array(
				'type' => 'uuid',
			),
			'kd_id'	=> array(
				'type' => 'INT',
				'constraint' => 11
			),
			'id_kompetensi' => array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('kd_nilai_id', TRUE);
		$this->dbforge->create_table('kd_nilai',TRUE);
	}
	public function down(){
		$this->dbforge->drop_table('kd_nilai', TRUE);
	}
}