<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_users_groups_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'user_id' => array(
				'type' => 'uuid',
			),
			'group_id'	=> array(
				'type' => 'INT',
				'constraint' => 11,
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users_groups',TRUE);
	}
	public function down(){
		$this->dbforge->drop_table('users_groups', TRUE);
	}
}