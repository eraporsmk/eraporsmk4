<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_tingkat_pendidikan_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		/*$fields = array(
			'tingkat_pendidikan_id' => array(
				'type' => 'INT',
				'constraint' => 2,
			),
			'kode' => array(
				'type' => 'VARCHAR',
				'constraint' => 5
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 20
			),
			'jenjang_pendidikan_id' => array(
				'type' => 'INT',
				'constraint' => 2,
			),
      		'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('tingkat_pendidikan_id',TRUE);
		$this->dbforge->create_table('ref_tingkat_pendidikan',TRUE);*/
		$sql = "CREATE TABLE ref_tingkat_pendidikan(
				tingkat_pendidikan_id numeric(2,0) NOT NULL,
				kode character varying(5) NOT NULL,
				nama character varying(20) NOT NULL,
				jenjang_pendidikan_id numeric(2,0) NOT NULL,
				created_at timestamp(0) without time zone NOT NULL,
				updated_at timestamp(0) without time zone NOT NULL,
				deleted_at timestamp(0) without time zone,
				last_sync timestamp(0) without time zone NOT NULL,
				CONSTRAINT tingkat_pendidikan_pkey PRIMARY KEY (tingkat_pendidikan_id))
				WITH ( OIDS=FALSE );";
		$this->db->query($sql);
		$this->db->select('*');
		$this->db->from('ref_tingkat_pendidikan');
		$this->db->where('tingkat_pendidikan_id',1);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			include_once APPPATH."/migrations/referensi/ref_tingkat_pendidikan.php";
			$this->db->insert_batch('ref_tingkat_pendidikan', $ref_tingkat_pendidikan);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_tingkat_pendidikan', TRUE);
	}
}