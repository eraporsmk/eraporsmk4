<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_kurikulum_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$sql = "CREATE TABLE ref_kurikulum (
					kurikulum_id smallint NOT NULL,
					nama_kurikulum character varying(120) NOT NULL,
					mulai_berlaku date NOT NULL,
					sistem_sks numeric(1,0) NOT NULL,
					total_sks numeric(3,0) NOT NULL,
					jenjang_pendidikan_id numeric(2,0) NOT NULL,
					jurusan_id character varying(25),
					created_at timestamp(0) without time zone NOT NULL,
					updated_at timestamp(0) without time zone NOT NULL,
					deleted_at timestamp(0) without time zone,
					last_sync timestamp(0) without time zone NOT NULL,
					CONSTRAINT kurikulum_pkey PRIMARY KEY (kurikulum_id),
					CONSTRAINT kurikulum_jurusan_id_fkey FOREIGN KEY (jurusan_id)
						REFERENCES ref_jurusan (jurusan_id) MATCH SIMPLE
						ON UPDATE NO ACTION ON DELETE NO ACTION
				)
				WITH ( OIDS=FALSE );";
		$this->db->query($sql);
		$this->db->select('*');
		$this->db->from('ref_kurikulum');
		$this->db->where('kurikulum_id',1);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			include_once APPPATH."/migrations/referensi/ref_kurikulum.php";
			$this->db->insert_batch('ref_kurikulum', $ref_kurikulum);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_kurikulum', TRUE);
	}
}