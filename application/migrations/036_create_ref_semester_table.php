<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_semester_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'tahun'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'semester' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'nama'	=> array(
				'type' => 'VARCHAR',
				'constraint' => 255
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('ref_semester',TRUE);
		$this->db->select('*');
		$this->db->from('ref_semester');
		$this->db->where('id',1);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			$ref_semester = array(
				array('tahun' => '2017/2018','semester' => '1','nama' => ' Semester Ganjil','created_at' => '2017-11-24 03:17:15','updated_at' => '2017-11-24 03:17:15'),
				array('tahun' => '2017/2018','semester' => '2','nama' => ' Semester Genap','created_at' => '2017-12-28 00:58:58','updated_at' => '2017-12-28 00:58:58')
			);
			$this->db->insert_batch('ref_semester', $ref_semester);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_semester', TRUE);
	}
}