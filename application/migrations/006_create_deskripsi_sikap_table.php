<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_deskripsi_sikap_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'deskripsi_sikap_id' => array(
				'type' => 'uuid',
			),
			'sekolah_id' => array(
				'type' => 'uuid',
			),
			'semester_id' => array(
				'type' => 'INT',
				'constraint' => 11
			),
			'rombongan_belajar_id'	=> array(
				'type' => 'uuid',
			),
			'siswa_id'	=> array(
				'type' => 'uuid',
			),
			'uraian_deskripsi_spiritual' => array(
				'type' => 'TEXT',
			),
			'uraian_deskripsi_sosial' => array(
				'type' => 'TEXT',
			),
			'predikat_spiritual'	=> array(
				'type' => 'character varying(3)',
			),
			'predikat_sosial'	=> array(
				'type' => 'character varying(3)',
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('deskripsi_sikap',TRUE); 
	}
	public function down(){
		$this->dbforge->drop_table('deskripsi_sikap', TRUE);
	}
}