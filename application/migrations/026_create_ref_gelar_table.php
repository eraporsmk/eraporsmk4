<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_gelar_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'gelar_akademik_id' => array(
				'type' => 'INT',
			),
			'kode' => array(
				'type' => 'VARCHAR',
				'constraint' => 10
			),
      		'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 40
			),
      		'posisi_gelar' => array(
				'type' => 'numeric(1,0)',
			),
      		'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('gelar_akademik_id',TRUE);
		$this->dbforge->create_table('ref_gelar',TRUE);
		$this->db->select('*');
		$this->db->from('ref_gelar');
		$this->db->where('gelar_akademik_id',1);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			include_once APPPATH."/migrations/referensi/ref_gelar.php";
			$this->db->insert_batch('ref_gelar', $ref_gelar);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_gelar', TRUE);
	}
}