<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_negara_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$sql = "CREATE TABLE negara (
					negara_id character(2) NOT NULL,
					nama character varying(45) NOT NULL,
					luar_negeri numeric(1,0) NOT NULL,
					created_at timestamp(0) without time zone NOT NULL,
					updated_at timestamp(0) without time zone NOT NULL,
					deleted_at timestamp(0) without time zone,
					last_sync timestamp(0) without time zone NOT NULL,
					CONSTRAINT negara_pkey PRIMARY KEY (negara_id)
				)
				WITH ( OIDS=FALSE );";
		$this->db->query($sql);
		$this->db->select('*');
		$this->db->from('negara');
		$this->db->where('negara_id','AD');
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			include_once APPPATH."/migrations/referensi/negara.php";
			$this->db->insert_batch('negara', $negara);
		}
	}
	public function down(){
		$this->dbforge->drop_table('negara', TRUE);
	}
}