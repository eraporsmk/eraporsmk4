<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_ref_pekerjaan_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'pekerjaan_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'nama' => array(
				'type' => 'VARCHAR',
				'constraint' => 25,
				'null' => true
			),
			'deleted_at' => array(
				'type' => 'timestamp(0) without time zone',
				'null'	=> true
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone',
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('pekerjaan_id', TRUE);
		$this->dbforge->create_table('ref_pekerjaan',TRUE); 
		$ref_pekerjaan = array(
			array('pekerjaan_id' => '1','nama' => 'Tidak bekerja','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '2','nama' => 'Nelayan','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '3','nama' => 'Petani','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '4','nama' => 'Peternak','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '5','nama' => 'PNS/TNI/Polri','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '6','nama' => 'Karyawan Swasta','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '7','nama' => 'Pedagang Kecil','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '8','nama' => 'Pedagang Besar','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '9','nama' => 'Wiraswasta','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '10','nama' => 'Wirausaha','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '11','nama' => 'Buruh','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '12','nama' => 'Pensiunan','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '13','nama' => 'Tenaga Kerja Indonesia','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '90','nama' => 'Tidak dapat diterapkan','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '98','nama' => 'Sudah Meninggal','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('pekerjaan_id' => '99','nama' => 'Lainnya','created_at' => '2017-11-14 14:07:49','updated_at' => '2017-11-14 14:07:49','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00')
		);
		$this->db->select('*');
		$this->db->from('ref_pekerjaan');
		$this->db->where('pekerjaan_id',1);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			$this->db->insert_batch('ref_pekerjaan', $ref_pekerjaan);
		}
	}
	public function down(){
		$this->dbforge->drop_table('ref_pekerjaan', TRUE);
	}
}