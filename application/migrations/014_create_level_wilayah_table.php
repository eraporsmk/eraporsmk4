<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_level_wilayah_table extends CI_Migration {
	public function __construct(){
		parent::__construct();
		$this->load->dbforge();
	}
	public function up(){
		$fields = array(
			'id_level_wilayah' => array(
				'type' => 'smallint'
			),
			'level_wilayah' => array(
				'type' => 'VARCHAR',
				'constraint' => 15
			),
			'created_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'updated_at' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			),
			'deleted_at' => array(
				'type' 		=> 'timestamp(0) without time zone',
				'null'	=> true
			),
			'last_sync' => array(
				'type' => 'timestamp(0) without time zone NOT NULL'
			)
		);
		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id_level_wilayah', TRUE);
		$this->dbforge->create_table('level_wilayah',TRUE);
		$level_wilayah = array(
			array('id_level_wilayah' => '0','level_wilayah' => 'Indonesia','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('id_level_wilayah' => '1','level_wilayah' => 'Propinsi','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('id_level_wilayah' => '2','level_wilayah' => 'Kab / Kota','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('id_level_wilayah' => '3','level_wilayah' => 'Kecamatan','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
			array('id_level_wilayah' => '4','level_wilayah' => 'Desa','created_at' => '2015-07-31 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00')
		);
		$this->db->select('*');
		$this->db->from('level_wilayah');
		$this->db->where('id_level_wilayah',0);
		$query = $this->db->get();
		$result = $query->row();
		if(!$result){
			$this->db->insert_batch('level_wilayah', $level_wilayah);
		}
	}
	public function down(){
		$this->dbforge->drop_table('level_wilayah', TRUE);
	}
}