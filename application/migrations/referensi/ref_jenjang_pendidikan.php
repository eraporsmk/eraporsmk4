<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `menu`
//

// `menu`.`ref_jenjang_pendidikan`
$ref_jenjang_pendidikan = array(
  array('nama' => 'Tidak sekolah','jenjang_lembaga' => '0','jenjang_pendidikan_id' => '0','jenjang_orang' => '1','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'PAUD','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '1','jenjang_orang' => '0','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'TK / sederajat','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '2','jenjang_orang' => '0','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'Putus SD','jenjang_lembaga' => '0','jenjang_pendidikan_id' => '3','jenjang_orang' => '1','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'SD / sederajat','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '4','jenjang_orang' => '1','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'SMP / sederajat','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '5','jenjang_orang' => '1','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'SMA / sederajat','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '6','jenjang_orang' => '1','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'Paket A','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '7','jenjang_orang' => '0','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'Paket B','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '8','jenjang_orang' => '0','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'Paket C','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '9','jenjang_orang' => '0','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'D1','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '20','jenjang_orang' => '1','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'D2','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '21','jenjang_orang' => '1','created_at' => '2013-05-13 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'D3','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '22','jenjang_orang' => '1','created_at' => '2013-05-14 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'D4','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '23','jenjang_orang' => '1','created_at' => '2013-05-14 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'S1','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '30','jenjang_orang' => '1','created_at' => '2013-05-14 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'S2','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '35','jenjang_orang' => '1','created_at' => '2013-05-14 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'S3','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '40','jenjang_orang' => '1','created_at' => '2013-05-14 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'Non formal','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '90','jenjang_orang' => '0','created_at' => '2013-05-14 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'Informal','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '91','jenjang_orang' => '0','created_at' => '2013-05-14 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00'),
  array('nama' => '(tidak diisi)','jenjang_lembaga' => '0','jenjang_pendidikan_id' => '98','jenjang_orang' => '0','created_at' => '2013-05-25 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => '2013-05-25 00:00:00','last_sync' => '1901-01-01 00:00:00'),
  array('nama' => 'Lainnya','jenjang_lembaga' => '1','jenjang_pendidikan_id' => '99','jenjang_orang' => '0','created_at' => '2013-05-14 00:00:00','updated_at' => '2016-07-22 06:00:00','deleted_at' => NULL,'last_sync' => '1901-01-01 00:00:00')
);
