<?php
ini_set('max_execution_time', 0); 
ini_set('memory_limit', '-1'); 
//ini_set('display_errors', 0);
// function to check if the system has been installed
$CI =& get_instance();
function check_installer(){
	$CI = & get_instance();
	$CI->load->database();
	$CI->load->dbutil();
	$uri = $CI->uri->segment_array();
	if(!file_exists('.htaccess')){
		redirect('index.php/install/write_htaccess');
	}
	if ($CI->db->hostname == "") {
		if(isset($uri[1]) && $uri[1] == 'install'){
		} else {
			redirect('install');
		}
	}
	set_time_zone();
	$database = array(
		1	=> 'absen',
		2	=> 'anggota_rombel',
		3	=> 'catatan_ppk',
		4	=> 'catatan_wali',
		5	=> 'deskripsi_mata_pelajaran',
		6	=> 'deskripsi_sikap',
		7	=> 'ekstrakurikuler',
		8	=> 'gelar_ptk',
		9	=> 'groups',
		10	=> 'guru_terdaftar',
		11	=> 'import_rencana',
		12	=> 'jurusan_sp',
		13	=> 'kd_nilai',
		14	=> 'level_wilayah',
		15	=> 'ref_jurusan',
		16	=> 'ref_kurikulum',
		17	=> 'negara',
		18	=> 'mst_wilayah',
		19	=> 'nilai',
		20	=> 'nilai_ekstrakurikuler',
		21	=> 'nilai_sikap',
		22	=> 'pembelajaran',
		23	=> 'prakerin',
		24	=> 'prestasi',
		25	=> 'ref_agama',
		26	=> 'ref_gelar',
		27	=> 'ref_guru',
		28	=> 'ref_jenis_ptk',
		29	=> 'mata_pelajaran',
		30	=> 'ref_kelompok',
		31	=> 'ref_kompetensi',
		32	=> 'ref_kompetensi_dasar',
		33	=> 'ref_pekerjaan',
		34	=> 'ref_tingkat_pendidikan',
		35	=> 'ref_sekolah',
		36	=> 'ref_semester',
		37	=> 'ref_sikap',
		38	=> 'ref_siswa',
		39	=> 'ref_status_kepegawaian',
		40	=> 'mata_pelajaran_kurikulum',
		41	=> 'remedial',
		42	=> 'rencana_penilaian',
		43	=> 'rombongan_belajar',
		44	=> 'settings',
		45	=> 'teknik_penilaian',
		46	=> 'users',
		47	=> 'users_groups',
		48	=> 'nilai_akhir',
		49	=> 'nilai_ukk',
		50	=> 'nilai_karakter',
		51	=> 'indikator_karakter',
		52	=> 'kenaikan_kelas',
		53	=> 'nilai_rapor',
		54	=> 'bobot_keterampilan'
	);
	migrasi($database);
	//update_sync();
}
function update_jurusan_sp($sekolah_id){
	$CI = & get_instance();
	$find_jurusan_sp = $CI->jurusan_sp->find_all("sekolah_id != '$sekolah_id'");
	if($find_jurusan_sp){
		foreach($find_jurusan_sp as $jsp){
			$CI->jurusan_sp->update($jsp->jurusan_sp_id, array("sekolah_id" => $sekolah_id));
		}
	}
}
function update_rombongan_belajar($rombel_id, $jurusan_sp_id){
	$CI = & get_instance();
	if($jurusan_sp_id){
		$find_rombel = $CI->rombongan_belajar->get($rombel_id);
		if($find_rombel && $find_rombel->jurusan_sp_id != $jurusan_sp_id){
			//test($find_rombel);
			$CI->rombongan_belajar->update($find_rombel->rombongan_belajar_id, array("jurusan_sp_id" => $jurusan_sp_id, "jurusan_id" => $find_rombel->jurusan_sp_id));
		}
	}
}
function update_pembelajaran(){
	$CI = & get_instance();
	if(isset($CI->pembelajaran)){
		$pembelajaran = $CI->pembelajaran->find_all("mata_pelajaran_id NOT IN (SELECT mata_pelajaran_id FROM mata_pelajaran WHERE deleted_at IS NULL)");
		if($pembelajaran){
			$query = $CI->db->query("UPDATE pembelajaran INNER JOIN ref_mata_pelajaran ON pembelajaran.mata_pelajaran_id = ref_mata_pelajaran.id SET pembelajaran.mata_pelajaran_id = ref_mata_pelajaran.id_nasional;");
		}
	}
}
function update_nilai(){
	$CI = & get_instance();
	if(isset($CI->nilai)){
		$nilai = $CI->nilai->find_all("mata_pelajaran_id NOT IN (SELECT mata_pelajaran_id FROM mata_pelajaran WHERE deleted_at IS NULL)", '*','id ASC', 0, 1);
		if($nilai){
			$query = $CI->db->query("UPDATE nilai INNER JOIN ref_mata_pelajaran ON nilai.mata_pelajaran_id = ref_mata_pelajaran.id SET nilai.mata_pelajaran_id = ref_mata_pelajaran.id_nasional;");
		}
	}
}
function update_rencana(){
	$CI = & get_instance();
	if(isset($CI->rencana_penilaian)){
		$nilai = $CI->rencana_penilaian->find_all("mata_pelajaran_id NOT IN (SELECT mata_pelajaran_id FROM mata_pelajaran WHERE deleted_at IS NULL)", '*','id ASC', 0, 1);
		if($nilai){
			$query = $CI->db->query("UPDATE rencana_penilaian INNER JOIN ref_mata_pelajaran ON rencana_penilaian.mata_pelajaran_id = ref_mata_pelajaran.id SET rencana_penilaian.mata_pelajaran_id = ref_mata_pelajaran.id_nasional;");
		}
	}
}
function update_remedial(){
	$CI = & get_instance();
	if(isset($CI->remedial)){
		$remedial = $CI->remedial->find_all("mata_pelajaran_id NOT IN (SELECT mata_pelajaran_id FROM mata_pelajaran WHERE deleted_at IS NULL)", '*','id ASC', 0, 1);
		if($remedial){
			$query = $CI->db->query("UPDATE remedial INNER JOIN ref_mata_pelajaran ON remedial.mata_pelajaran_id = ref_mata_pelajaran.id SET remedial.mata_pelajaran_id = ref_mata_pelajaran.id_nasional;");
		}
	}
}
function update_deskripsi_mata_pelajaran(){
	$CI = & get_instance();
	if(isset($CI->deskripsi_mata_pelajaran)){
		$deskripsi_mata_pelajaran = $CI->deskripsi_mata_pelajaran->find_all("mata_pelajaran_id NOT IN (SELECT mata_pelajaran_id FROM mata_pelajaran WHERE deleted_at IS NULL)", '*','id ASC', 0, 1);
		if($deskripsi_mata_pelajaran){
			$query = $CI->db->query("UPDATE deskripsi_mata_pelajaran INNER JOIN ref_mata_pelajaran ON deskripsi_mata_pelajaran.mata_pelajaran_id = ref_mata_pelajaran.id SET deskripsi_mata_pelajaran.mata_pelajaran_id = ref_mata_pelajaran.id_nasional;");
		}
	}
}
function update_import_rencana(){
	$CI = & get_instance();
	if(isset($CI->import_rencana)){
		$import_rencana = $CI->import_rencana->find_all("mata_pelajaran_id NOT IN (SELECT mata_pelajaran_id FROM mata_pelajaran WHERE deleted_at IS NULL)", '*','id ASC', 0, 1);
		if($import_rencana){
			$query = $CI->db->query("UPDATE import_rencana INNER JOIN ref_mata_pelajaran ON import_rencana.mata_pelajaran_id = ref_mata_pelajaran.id SET import_rencana.mata_pelajaran_id = ref_mata_pelajaran.id_nasional;");
		}
	}
}
function update_sikap(){
	$CI = & get_instance();
	if(isset($CI->import_rencana)){
		$import_rencana = $CI->nilai_sikap->find_all("mata_pelajaran_id NOT IN (SELECT mata_pelajaran_id FROM mata_pelajaran WHERE deleted_at IS NULL)", '*','id ASC', 0, 1);
		if($import_rencana){
			$query = $CI->db->query("UPDATE nilai_sikap INNER JOIN ref_mata_pelajaran ON nilai_sikap.mata_pelajaran_id = ref_mata_pelajaran.id SET nilai_sikap.mata_pelajaran_id = ref_mata_pelajaran.id_nasional;");
		}
	}
}
function migrasi($database){
	$CI =& get_instance();
	$CI->load->database();
	$CI->load->dbutil();
	foreach($database as $db){
		if (!$CI->db->table_exists($db)){
			redirect('migrate');
		}
	}
}
function get_last_id($table){
	$CI = & get_instance();
	$query = $CI->db->query("SELECT max(id) AS last_id FROM $table")->row();
	return $query;
}
function get_ta(){
	$CI =& get_instance();
	//if ($CI->db->table_exists('settings')){
		$settings = $CI->settings->get(1);
		$strings = $settings->periode;
		$strings = explode('|',$strings);
		$tapel = str_replace(' ','',$strings[0]);
		$semester = $strings[1];
		$semester = str_replace('Semester','',$semester);
		$semester = str_replace(' ','',$semester);
		$smt = 2;
		if($semester == 'Ganjil'){
			$smt = 1;
		}
		//if ($CI->db->table_exists('semester')){
			$ajarans = $CI->semester->find("tahun = '$tapel' AND semester = $smt");
			if($ajarans){
				return $ajarans;
			} else {
				$data_ajarans = array(
					'tahun'		=> $tapel,
					'semester' 	=> $smt,
					'nama' 		=> $strings[1]
				);
				$CI->semester->insert($data_ajarans);
			}
		//}
	//}
}
function get_next_ta(){
	global $CI;
	$semester = get_ta();
	$tahun = $semester->tahun;
	$tahun_plus_1 = date('Y') + 1;
	$tapel = $tahun + 1 .'/'.$tahun_plus_1;
	$get_semester = $CI->semester->find("tahun = '$tapel' AND semester = 1");
	if($get_semester){
		$semester_id = $get_semester->id;
	} else {
		$semester_id = create_ta();
	}
	return $semester_id;
}
function create_ta(){
	global $CI;
	$semester = get_ta();
	$tahun = $semester->tahun;
	$tahun_plus_1 = date('Y') + 1;
	$tapel = $tahun + 1 .'/'.$tahun_plus_1;
	$data_semester = array(
		'tahun'	=> $tapel,
		'semester' 	=> 1,
		'nama' 	=> 'Semester Ganjil'
	);
	$get_semester = $CI->semester->insert($data_semester);
	return $get_semester;
}
function test($var){
	echo '<pre>';
	print_r($var);
	echo '</pre>';
}
function success_msg($msg){
	$display = '<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Success! </b> ' .$msg. '
                </div>';
	return $display;
}

function error_msg($msg){
	$display = '<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Error! </b> ' .$msg. '
                </div>';
	return $display;
}
function hak_akses($group){
	$CI = & get_instance();
    // You may need to load the model if it hasn't been pre-loaded
	//$this->load->library('someclass');
    $CI->load->library('ion_auth');
    // Call a function of the model
    if(!$CI->ion_auth->in_group($group)){
		return show_error('Akses ditolak');
	}
}
function get_jabatan($user_id){
	global $CI;
	$user_groups = $CI->ion_auth->get_users_groups($user_id)->result();
	foreach($user_groups as $user_group){
		$nama_group[] = $user_group->name; 
	}
	return $nama_group;
}
function get_akses($user_id){
	$CI = & get_instance();
	$find_akses = $CI->ion_auth->get_users_groups($user_id)->result();
	$loggeduser = $CI->ion_auth->user($user_id)->row();
	//test($find_akses);
	//test($loggeduser);
	//$find_akses = Usergroup::find_all_by_user_id($user_id);
	foreach($find_akses as $akses){
		$get_nama_akses = $CI->group->get($akses->id);
		if($get_nama_akses->id == 3){
			$find_user_id = $loggeduser->guru_id;
		} elseif($get_nama_akses->id == 4) {
			$find_user_id = $loggeduser->siswa_id;
		} else {
			$find_user_id = $user_id;
		}
		$data['id'][] = $find_user_id;
		$data['name'][] = $get_nama_akses->name;
	}
	return $data;
}
function TanggalIndo($date){
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	$tahun = substr($date, 0, 4);
	$bulan = substr($date, 5, 2);
	$tgl   = substr($date, 8, 2);
	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun; 
	return($result);
}
function status_label($status){
	if($status == '1') : 
		$label = '<span class="btn btn-xs btn-success"> Aktif </span>';
	elseif ($status == '0') : 
		$label = '<span class="btn btn-xs btn-danger"> Non Aktif </span>';
	endif;
	return $label;
}
function status_password($status){
    if($status == '1') :
        $label = '<span class="btn btn-xs btn-success"> Custom </span>';
    elseif ($status == '0') :
        $label = '<span class="btn btn-xs btn-danger"> Default </span>';
    endif;
    return $label;
}
function status_kepegawaian(){
	global $CI;
	$status = $CI->status_kepegawaian->get_all();
	return $status;
}
function jenis_ptk(){
	global $CI;
	$status = $CI->jenis_ptk->get_all();
	return $status;
}
function agama(){
	global $CI;
	$status = $CI->agama->get_all();
	return $status;
}
function GenerateEmail($length = 6) {
	$characters = 'abcdefghijklmnopqrstuvwxyz';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}
function GenerateID($length = 10) {
	$characters = '0123456789';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}
function GenerateNISN($length = 12) {
	$characters = '0123456789';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}
function get_agama($a){
	global $CI;
	if(is_numeric($a)){
		$get_agama = $CI->agama->get($a);
		$agama = ($get_agama) ? $get_agama->nama : '-';
		$agama = str_replace('Budha','Buddha',$agama);
	} else {
		$agama = str_replace('Budha','Buddha',$a);
	}
	return $agama;
}
function get_status_kepegawaian($id){
	global $CI;
	$get_status_kepegawaian = $CI->status_kepegawaian->get($id);
	$status_kepegawaian = ($get_status_kepegawaian) ? $get_status_kepegawaian->nama : '-';
	return $status_kepegawaian;
}
function get_jenis_ptk($id){
	global $CI;
	$get_jenis_ptk = $CI->jenis_ptk->get($id);
	$jenis_ptk = ($get_jenis_ptk) ? $get_jenis_ptk->nama : '-';
	return $jenis_ptk;
}
function set_log($table,$query,$user){
	global $CI;
	$settings = $CI->settings->get(1);
	if($settings->zona == 1){ // WIB
		date_default_timezone_set('Asia/Jakarta');
	}
	if($settings->zona == 2){ //WITA
		date_default_timezone_set('Asia/Makassar');
	}
	if($settings->zona == 3){ //WIT
		date_default_timezone_set('Asia/Jayapura');
	}
	$file = './log.txt';
	$message = $table.", ".$query. ", User: ".$user.", Waktu :".date('d-m-y H:i:s').PHP_EOL;
	file_put_contents($file, $message, FILE_APPEND | LOCK_EX);
}
function after_create_log($query,$id){
	global $CI;
	$CI->load->library('ion_auth');
	$loggeduser = $CI->ion_auth->user()->row();
	set_log('Tambah data '.$query,'ID:'.$id,$loggeduser->username);
}
function after_update_log($query,$id){
	global $CI;
	$CI->load->library('ion_auth');
	$loggeduser = $CI->ion_auth->user()->row();
	set_log('Update data '.$query,'ID:'.$id,$loggeduser->username);
}
function before_destroy($query,$table,$id){
	global $CI;
	$CI->load->library('ion_auth');
	$loggeduser = $CI->ion_auth->user()->row();
	set_log('Hapus data '.$query,'ID:'.$id,$loggeduser->username);
}
function get_kurikulum($kurikulum_id,$query='nama'){
	global $CI;
	$get_kurikulum = $CI->kurikulum->find_by_kurikulum_id($kurikulum_id);
	//$jurusan = $CI->ref_jurusan->find_by_jurusan_id($kurikulum_id);
	/*$nama_kompetensi = ($kompetensi) ? $kompetensi->nama_kurikulum : 0;
	if (strpos($nama_kompetensi, 'SMK 2013') !== false) {
		$get_nama_kompetensi['nama'] = str_replace('SMK 2013','',$nama_kompetensi.' (2013)');
		$get_nama_kompetensi['id'] = str_replace('SMK 2013','','2013');
	}
	if (strpos($nama_kompetensi, 'SMK KTSP') !== false) {
		$get_nama_kompetensi['nama'] = str_replace('SMK KTSP','',$nama_kompetensi.' (KTSP)');
		$get_nama_kompetensi['id'] = str_replace('SMK KTSP','','KTSP');
	}
	$result = isset($get_nama_kompetensi[$query]) ? $get_nama_kompetensi[$query] : 'Kompetensi Keahlian tidak ditemukan';
	*/
	$result = ($get_kurikulum) ? $get_kurikulum->nama_kurikulum: 'Kurikulum tidak ditemukan('.$kurikulum_id.')';
	return $result;
}
function get_wali_kelas($id_rombel){
	global $CI;
	$rombel = $CI->rombongan_belajar->get($id_rombel);
	$guru_id = ($rombel) ? $rombel->guru_id : gen_uuid();
	$nama_guru = get_nama_guru($guru_id);
	return $nama_guru;
}
function get_nama_guru($id_guru){
	global $CI;
	$CI->load->model('gelar_model', 'gelar');
	$guru = $CI->guru->with('gelar_ptk')->get($id_guru);
	$nama_guru = isset($guru->nama) ? $guru->nama : '-';
	$nama_guru = strtoupper($nama_guru);
	//$ptk_id = ($guru) ? $guru->guru_id_dapodik : gen_uuid();
	$CI->db->select('a.gelar_akademik_id, b.posisi_gelar, b.kode');
	$CI->db->from('gelar_ptk as a');
	$CI->db->join('ref_gelar as b', 'a.gelar_akademik_id = b.gelar_akademik_id');
	//$CI->db->where('a.ptk_id', $ptk_id);
	$CI->db->where('a.guru_id', $id_guru);
	$CI->db->where('a.deleted_at IS NULL');
	$CI->db->where('b.posisi_gelar', 1);
	$CI->db->order_by('b.gelar_akademik_id', 'ASC');
	$query_depan = $CI->db->get();
	$gelar_ptk_depan = $query_depan->result_array();
	$set_gelar_depan = '';
	if($gelar_ptk_depan){
		$set_gelar_depan = implode('. ', array_map(function ($entry_depan) {
		  return $entry_depan['kode'];
		}, $gelar_ptk_depan)).'. ';
	}
	$CI->db->select('a.gelar_akademik_id, b.posisi_gelar, b.kode');
	$CI->db->from('gelar_ptk as a');
	$CI->db->join('ref_gelar as b', 'a.gelar_akademik_id = b.gelar_akademik_id');
	//$CI->db->where('a.ptk_id', $ptk_id);
	$CI->db->where('a.guru_id', $id_guru);
	$CI->db->where('a.deleted_at IS NULL');
	$CI->db->where('b.posisi_gelar', 2);
	$CI->db->where('b.gelar_akademik_id != 99999');
	$CI->db->order_by('b.gelar_akademik_id', 'ASC');
	$query_belakang = $CI->db->get();
	$gelar_ptk_belakang = $query_belakang->result_array();
	$set_gelar_belakang = '';
	if($gelar_ptk_belakang){
		$set_gelar_belakang = ', '.implode(', ', array_map(function ($entry_belakang) {
		  return $entry_belakang['kode'];
		}, $gelar_ptk_belakang));
	}
	$nama_guru = $set_gelar_depan.$nama_guru.$set_gelar_belakang;
	return $nama_guru;
}
function get_nip_guru($id_guru){
	global $CI;
	$guru = $CI->guru->get($id_guru);
	$nip_guru = isset($guru->nip) ? $guru->nip : '-';
	return $nip_guru;
}
function get_nama_mapel($mata_pelajaran_id){
	global $CI;
	$get_nama_mapel = $CI->mata_pelajaran->get($mata_pelajaran_id);
	$nama_mapel = ($get_nama_mapel) ? $get_nama_mapel->nama : '-';
	return $nama_mapel;
}
function get_nama_mapel_alias($rombel_id,$mata_pelajaran_id){
	global $CI;
	$get_nama_mapel_alias = $CI->pembelajaran->find("rombongan_belajar_id = '$rombel_id' AND mata_pelajaran_id = $mata_pelajaran_id");
	/*if($get_nama_mapel_alias){
		$nama_mapel_alias = ($get_nama_mapel_alias->nama_mata_pelajaran) ? $get_nama_mapel_alias->nama_mata_pelajaran : get_nama_mapel($mata_pelajaran_id);
	} else {
		$nama_mapel_alias = get_nama_mapel($mata_pelajaran_id);
	}*/
	$nama_mapel_alias = ($get_nama_mapel_alias) ? ($get_nama_mapel_alias->nama_mata_pelajaran) ? $get_nama_mapel_alias->nama_mata_pelajaran : get_nama_mapel($mata_pelajaran_id) : get_nama_mapel($mata_pelajaran_id);
	return $nama_mapel_alias;
}
function get_kelompok_id($rombel_id,$mata_pelajaran_id){
	global $CI;
	$get_kelompok_id = $CI->pembelajaran->find("rombongan_belajar_id = '$rombel_id' AND mata_pelajaran_id = $mata_pelajaran_id");
	$kelompok_id = ($get_kelompok_id) ? $get_kelompok_id->kelompok_id : '';
	return $kelompok_id;
}
function get_no_urut($rombel_id,$mata_pelajaran_id){
	global $CI;
	$get_no_urut = $CI->pembelajaran->find("rombongan_belajar_id = '$rombel_id' AND mata_pelajaran_id = $mata_pelajaran_id");
	$no_urut = ($get_no_urut) ? $get_no_urut->no_urut : '';
	return $no_urut;
}
function get_start() {
	$start = 0;
	if (isset($_GET['iDisplayStart'])) {
		$start = intval($_GET['iDisplayStart']);
		if ($start < 0)
			$start = 0;
	}
	return $start;
}
function get_rows() {
	$rows = 10;
	if (isset($_GET['iDisplayLength'])) {
		$rows = intval($_GET['iDisplayLength']);
		if ($rows < 5 || $rows > 500) {
			$rows = 10;
		}
	}
	return $rows;
}
function get_sort_dir() {
	$sort_dir = "ASC";
	$sdir = strip_tags($_GET['sSortDir_0']);
	if (isset($sdir)) {
		if ($sdir != "asc" ) {
			$sort_dir = "DESC";
		}
	}
	return $sort_dir;
}
function get_guru_mapel($semester_id, $rombongan_belajar_id, $id_mapel, $query = 'nama'){
	global $CI;
	$get_mapel = $CI->pembelajaran->find("semester_id = $semester_id and rombongan_belajar_id = '$rombongan_belajar_id' and mata_pelajaran_id = $id_mapel");
	$nama_guru_mapel['id'] = ($get_mapel) ? $get_mapel->guru_id : 0;
	$nama_guru_mapel['nama'] = ($get_mapel) ? get_nama_guru($get_mapel->guru_id) : 0;
	return $nama_guru_mapel[$query];
}
function get_guru_pengajar($semester_id, $rombongan_belajar_id, $id_mapel, $query = 'nama'){
	global $CI;
	$get_mapel = $CI->pembelajaran->find("semester_id = $semester_id and rombongan_belajar_id = '$rombongan_belajar_id' and mata_pelajaran_id = $id_mapel");
	$nama_guru_mapel['id'] = ($get_mapel) ? $get_mapel->guru_pengajar_id : 0;
	$nama_guru_mapel['nama'] = ($get_mapel) ? get_nama_guru($get_mapel->guru_pengajar_id) : 0;
	return $nama_guru_mapel[$query];
}
function get_nama_rombel($id_rombel){
	global $CI;
	if($id_rombel){
		$rombel = $CI->rombongan_belajar->get($id_rombel);
		$nama_rombel = isset($rombel->nama) ? $rombel->nama : '-';
	} else {
		$nama_rombel = '-';
	}
	return $nama_rombel;
}
function get_bidang_keahlian($id){
	global $CI;
	$get_bidang_keahlian = $CI->bidang_keahlian->get($id);
	$bidang_keahlian = ($get_bidang_keahlian) ? $get_bidang_keahlian->nama : '-';
	return $bidang_keahlian;
}
function get_program_keahlian($id){
	global $CI;
	$get_program_keahlian = $CI->program_keahlian->get($id);
	$program_keahlian = ($get_program_keahlian) ? $get_program_keahlian->nama : '-';
	return $program_keahlian;
}
function get_kompetensi_keahlian($id){
	global $CI;
	$get_kompetensi_keahlian = $CI->kompetensi_keahlian->get($id);
	$kompetensi_keahlian = ($get_kompetensi_keahlian) ? $get_kompetensi_keahlian->nama : '-';
	return $kompetensi_keahlian;
}
function get_jumlah_siswa($id_rombel, $nama_mapel = NULL){
	global $CI;
	if($nama_mapel){
		$agamas = $CI->agama->get_all();
		foreach($agamas as $key=>$value){
			$nama_agama = str_replace('Budha','Buddha',$value->nama);
			$nama_agama = str_replace('Kong Hu Chu','Konghuchu',$nama_agama);
			$agama_id[$value->id] = $nama_agama;
		}
		$nama_mapel = str_replace('Pendidikan Agama','',$nama_mapel);
		$nama_mapel = str_replace('dan Budi Pekerti','',$nama_mapel);
		$nama_mapel = trim($nama_mapel);
		if (in_array($nama_mapel, $agama_id, true)) {
			$nama_mapel = str_replace('Buddha','Budha',$nama_mapel);
			$nama_mapel = strtolower($nama_mapel);
			$get_agama_id = $CI->agama->find("lower(nama) = '$nama_mapel'");
			$agama_id = ($get_agama_id) ? $get_agama_id->id : 0;
			$conditions = "AND siswa_id IN (SELECT siswa_id FROM ref_siswa WHERE agama_id = $agama_id)";
			$jumlah_siswa = get_siswa_by_rombel($id_rombel,$conditions);
			//echo $nama_mapel;
			//test($jumlah_siswa);
		} else {
			$jumlah_siswa = get_siswa_by_rombel($id_rombel);
		}
	} else {
		$jumlah_siswa = get_siswa_by_rombel($id_rombel);
	}
	return count($jumlah_siswa['data']);
}
function get_rombel_siswa($semester_id, $siswa_id, $query = 'nama'){
	global $CI;
	$anggota_rombel = $CI->anggota_rombel->find("semester_id = $semester_id AND siswa_id = '$siswa_id'");
	//test($anggota_rombel);
	$rombongan_belajar_id = ($anggota_rombel) ? $anggota_rombel->rombongan_belajar_id : gen_uuid();
	$rombongan_belajar = $CI->rombongan_belajar->get($rombongan_belajar_id);
	$rombel = ($rombongan_belajar) ? $rombongan_belajar->nama : '-';
	if($query == 'id'){
		$rombel = ($rombongan_belajar) ? $rombongan_belajar->rombongan_belajar_id : gen_uuid();
	}
	return $rombel;
}
function get_jumlah_penilaian($semester_id, $rombongan_belajar_id, $mapel_id, $kompetensi_id){
	global $CI;
	$all_rencana = $CI->rencana_penilaian->find_count("semester_id = $semester_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mapel_id AND kompetensi_id = $kompetensi_id");
	return status_penilaian($all_rencana);
}
function get_jumlah_penilaian_telah_dinilai($semester_id, $rombongan_belajar_id, $mapel_id, $kompetensi_id){
	global $CI;
	$all_rencana = $CI->rencana_penilaian->find_count("semester_id = $semester_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mapel_id AND kompetensi_id = $kompetensi_id AND rencana_penilaian_id IN(SELECT rencana_penilaian_id FROM nilai WHERE semester_id = $semester_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mapel_id AND kompetensi_id = $kompetensi_id)");
	return status_penilaian($all_rencana);
}
function status_penilaian($status){
	if($status > 0) {
		$label = '<span class="label label-success"> '.ucwords($status).' </span>';
	} else {
		$label = '<span class="label label-danger">'.ucwords($status).' </span>';
	}
	return $label;
}
function get_teknik_penilaian($id){
	global $CI;
	$get_teknik_penilaian = $CI->teknik_penilaian->get($id);
	$teknik_penilaian = ($get_teknik_penilaian) ? $get_teknik_penilaian->nama : '-';
	return $teknik_penilaian;
}
function get_siswa_by_rombel($rombongan_belajar_id,$conditions = NULL, $start = 0, $rows = NULL){
	global $CI;
	$ajaran = get_ta();
	if($rows){
		if($start){
			$start = $start - 1;
			$start = $start * $rows;
		}
		$data_siswa['data'] = $CI->siswa->find_all("siswa_id IN (SELECT siswa_id FROM anggota_rombel WHERE semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' AND deleted_at IS NULL $conditions)", '*', 'lower(nama) ASC', $start, $rows);
		//$CI->anggota_rombel->with('siswa')->find_all("semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' $conditions", '*', 'siswa_id ASC', $start, $rows);
		$data_siswa['count'] = $CI->siswa->find_count("siswa_id IN (SELECT siswa_id FROM anggota_rombel WHERE semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' AND deleted_at IS NULL $conditions)");
		//$CI->anggota_rombel->with('siswa')->find_count("semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' $conditions", '*', 'siswa_id ASC');
		$data_siswa['dari'] = ($start) ? $start : 1;
		$data_siswa['ke'] = ($start) ? count($data_siswa['data']) + $start : $rows;
		$data_siswa['page'] = ceil($data_siswa['count']/$rows);
	} else {
		//$data_siswa['data'] = $CI->anggota_rombel->with('siswa')->find_all("semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' $conditions", '*', 'siswa_id ASC');
		$data_siswa['data'] = $CI->siswa->find_all("siswa_id IN (SELECT siswa_id FROM anggota_rombel WHERE semester_id = $ajaran->id AND rombongan_belajar_id = '$rombongan_belajar_id' AND deleted_at IS NULL $conditions)", '*', 'lower(nama) ASC');
	}
	//number_format($data_siswa['count'] / $rows,0);
	return $data_siswa;
}
function filter_agama_siswa($nama_mapel,$rombongan_belajar_id, $start = 0, $rows = NULL){
	global $CI;
	$agamas = $CI->agama->get_all();
	foreach($agamas as $key=>$value){
		$nama_agama = str_replace('Budha','Buddha',$value->nama);
		$agama_id[$value->id] = $nama_agama;
	}
	$nama_mapel = str_replace('Pendidikan Agama','',$nama_mapel);
	$nama_mapel = str_replace('dan Budi Pekerti','',$nama_mapel);
	$nama_mapel = trim($nama_mapel);
	if (in_array($nama_mapel, $agama_id, true)) {
		$nama_mapel = str_replace('Buddha','Budha',$nama_mapel);
		$nama_mapel = strtolower($nama_mapel);
		$get_agama_id = $CI->agama->find("lower(nama) = '$nama_mapel'");
		$agama_id = ($get_agama_id) ? $get_agama_id->id : 0;
    	$conditions = "AND siswa_id IN (SELECT siswa_id FROM ref_siswa WHERE agama_id = $agama_id)";
		$data_siswa = get_siswa_by_rombel($rombongan_belajar_id,$conditions, $start, $rows);
	} else {
		$data_siswa = get_siswa_by_rombel($rombongan_belajar_id, NULL, $start, $rows);
	}
	//$data_siswa = get_siswa_by_rombel($rombongan_belajar_id, NULL, $start, $rows);
	/*$pos = strpos($nama_mapel, $nama_agama);
	if ($pos === false) {
		
		break;
	} else {
		
		break;
	}*/
	return $data_siswa;
}
function butir_sikap($id){
	global $CI;
	$result = '';
	if($id){
		$data_sikap = $CI->sikap->get($id);
		$result = isset($data_sikap->butir_sikap) ? $data_sikap->butir_sikap : '';
	}
	return $result;
}
function opsi_sikap($opsi,$num = NULL){
	if($num){
		if($opsi == 1) : 
			$label = 'Positif';
		elseif ($opsi == 2) : 
			$label = 'Negatif';
		endif;
	} else {
		if($opsi == 1) : 
			$label = '<span class="label label-success"> Positif </span>';
		elseif ($opsi == 2) : 
			$label = '<span class="label label-danger"> Negatif </span>';
		endif;
	}
	return $label;
}
function filter_table($jurusan, $tingkat, $find_akses, $nama_group){
	global $CI;
	$ajaran = get_ta();
	$output = '';
	if($jurusan && $tingkat){
		if(in_array('guru',$find_akses) && !in_array('waka',$nama_group)){
			$get_all_rombel = $CI->rombongan_belajar->find_all("jurusan_id = $jurusan AND tingkat = $tingkat AND semester_id = $ajaran->id");
		} else {
			$get_all_rombel = $CI->rombongan_belajar->find_all("jurusan_id = $jurusan AND tingkat = $tingkat AND semester_id = $ajaran->id");
		}
		foreach($get_all_rombel as $allrombel){
			$all_rombel= array();
			$all_rombel['value'] = $allrombel->rombongan_belajar_id;
			$all_rombel['text'] = $allrombel->nama;
			$output[] = $all_rombel;
		}
	}
	return $output;
}
function where($find_akses, $nama_group, $guru_id){
	$where = '';
	if(in_array('guru',$find_akses) && !in_array('waka',$nama_group)){
		$where = "AND mata_pelajaran_id IN(SELECT mata_pelajaran_id FROM pembelajaran WHERE guru_id = '$guru_id')";
	}
	return $where;
}
function joint($jurusan, $tingkat, $rombel){
	$join = '';
	if($jurusan && $tingkat == NULL && $rombel == NULL){
		$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan)";
	}elseif($jurusan && $tingkat && $rombel == NULL){
		$join = "AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE jurusan_id = $jurusan) AND rombongan_belajar_id IN (SELECT rombongan_belajar_id FROM rombongan_belajar WHERE tingkat = $tingkat)";
	} elseif($jurusan && $tingkat && $rombel){
		$join = "AND rombongan_belajar_id = '$rombel'";
	}
	return $join;
}
function check_great_than_one_fn($val,$satu, $redirect){
	$CI = & get_instance();
	if($val > 100){
  		$CI->session->set_flashdata('error', 'Tambah data nilai '.$redirect.' gagal. Nilai harus tidak lebih besar dari 100');
		//redirect('admin/asesmen/remedial');
		redirect('admin/asesmen/'.$redirect);
	}
	//if($val == 0){
  		//$CI->session->set_flashdata('error', 'Tambah data nilai '.$redirect.' gagal. Nilai harus tidak kurang atau sama dengan 0 (nol)');
		//redirect('admin/asesmen/'.$redirect);
	//}
}
function check_numeric($val,$form, $redirect){
	//test($val);
	//die();
	$CI = & get_instance();
	$nilai = ($val) ? $val[0] : 0;
	$get_redirect = strstr($redirect, '/');
	$get_redirect = str_replace('/','',$get_redirect);
	$form_entry = 'Nilai';
	if($get_redirect == 'prakerin'){
		$form_entry = 'Lamanya (bulan)';
	}
	if(is_numeric($nilai)){
		if($val >= 0){
		} else {
			$CI->session->set_flashdata('error', 'Tambah data nilai '.$get_redirect.' gagal. '.$form_entry.' tidak boleh minus');
			redirect('admin/'.$redirect);
		}
	} else {
		$CI->session->set_flashdata('error', 'Tambah data nilai '.$get_redirect.' gagal. '.$form_entry.' harus berupa angka');
		redirect('admin/'.$redirect);
	}
}
function bilanganKecil($a){
	$MAX_VALUE=100;
	for($i=0;$i< count($a) ;$i++){
		if($a[$i] <	$MAX_VALUE){
			$MAX_VALUE=$a[$i];
		}
	}
	return $MAX_VALUE;
}
function bilanganBesar($a){
	$MAX_VALUE=0;
	for($i=0;$i < count($a) ;$i++){
		if($a[$i] > $MAX_VALUE){
			$MAX_VALUE=$a[$i];
		}
	}
	return $MAX_VALUE;
}
function get_nilai_akhir_siswa($semester_id, $kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $siswa_id){
	global $CI;
	$CI->load->model('nilai_akhir_model','nilai_akhir');
	$get_nilai_akhir = $CI->nilai_akhir->find("semester_id = $semester_id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mata_pelajaran_id AND siswa_id = '$siswa_id'");
	$nilai_akhir = ($get_nilai_akhir) ? $get_nilai_akhir->nilai : 0;
	return $nilai_akhir;
}
function get_nilai_siswa($semester_id, $kompetensi_id, $rombongan_belajar_id, $mapel_id, $siswa_id){
	global $CI;
	$nilai_value = 0;
	$rombongan_belajar = $CI->rombongan_belajar->get($rombongan_belajar_id);
	$kelas = ($rombongan_belajar) ? $rombongan_belajar->tingkat : 0;
	$all_nilai_remedial = $CI->remedial->find("semester_id = $semester_id and kompetensi_id = $kompetensi_id and rombongan_belajar_id = '$rombongan_belajar_id' and mata_pelajaran_id = $mapel_id and siswa_id = '$siswa_id'");
	if($all_nilai_remedial){
		$all_nilai = unserialize($all_nilai_remedial->nilai);
		$set_nilai = 0;
		foreach($all_nilai as $nilai){
			$set_nilai += $nilai;
		}
		$pembagi = array_filter($all_nilai);
		$nilai_value = number_format($set_nilai / count($pembagi), 0);
	} else {
		$kkm = get_kkm($semester_id, $rombongan_belajar_id, $mapel_id);
		$aspek = ($kompetensi_id == 1) ? 'P' : 'K';
		$get_all_kd = $CI->kompetensi_dasar->find_all("mata_pelajaran_id = $mapel_id AND kelas = $kelas AND aspek = '$aspek'");
		if(!$get_all_kd){
			$get_all_kd = $CI->kompetensi_dasar->find_all("aspek = 'PK' AND mata_pelajaran_id = $mapel_id AND kelas = $kelas");
		}
		$get_all_kd_finish = count($get_all_kd);
		$set_rerata = 0;
		$pembagi = 0;
		$skor_akhir = 0;
		foreach($get_all_kd as $all_kd){
			$all_nilai = $CI->nilai->with('rencana_penilaian')->find_all("semester_id = $semester_id AND kompetensi_id = $kompetensi_id AND siswa_id = '$siswa_id' AND kompetensi_dasar_id = $all_kd->id AND mata_pelajaran_id = $mapel_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND rencana_penilaian_id IN (SELECT rencana_penilaian_id FROM rencana_penilaian WHERE semester_id = $semester_id AND mata_pelajaran_id = $mapel_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND kompetensi_id = $kompetensi_id AND deleted_at IS NULL)");
			//$all_nilai = $CI->nilai->with('rencana_penilaian')->find_all("semester_id = $semester_id AND kompetensi_id = $kompetensi_id AND siswa_id = '$siswa_id' AND kompetensi_dasar_id = $all_kd->id AND mata_pelajaran_id = $mapel_id AND rombongan_belajar_id = '$rombongan_belajar_id'");
			if($all_nilai){
				//test($all_nilai);
				$pembagi++;
				if($kompetensi_id == 1){
					$nilai_kd = 0;
					$total_bobot = 0;
					foreach($all_nilai as $set_nilai){
						$bobot_kd = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->bobot : 1;
						$nilai_kd += $set_nilai->nilai * $bobot_kd;
						$total_bobot += $bobot_kd;
					}
					$skor_akhir = ($nilai_kd / $total_bobot);
				} else {
					$nilai_kd = 0;
					$nilai_siswa = array();
					foreach($all_nilai as $set_nilai){
						/*$bobot_kd = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->bobot : 1;
						if(is_array($set_nilai->rencana_penilaian)){
							 $metode_id = $set_nilai->rencana_penilaian->metode_id;
						} else {
							$metode_id = GenerateID();
						}
						$nilai_siswa[$set_nilai->kompetensi_dasar_id.'_'.$metode_id][] = $set_nilai->nilai;
						$set_bobot_baru[$set_nilai->kompetensi_dasar_id.'_'.$metode_id][] = $bobot_kd;*/
						$bobot_kd = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->bobot : 1;
						$metode_id = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->metode_id : gen_uuid();
						$find_teknik = $CI->teknik_penilaian->get($metode_id);
						//test($find_teknik);
						$nama_teknik = ($find_teknik) ? strtolower(str_replace('_',' ',$find_teknik->nama)) : '_';
						//$nilai_siswa[$get_kd->id_kompetensi.'_'.$metode_id][] = $set_nilai->nilai;
						//$set_bobot_baru[$get_kd->id_kompetensi.'_'.$metode_id][] = $bobot_kd;
						$nilai_siswa[$all_kd->id_kompetensi.'_'.$nama_teknik][] = $set_nilai->nilai;
						$set_bobot_baru[$all_kd->id_kompetensi.'_'.$nama_teknik][] = $bobot_kd;
					}
					ksort($nilai_siswa, SORT_NUMERIC);
					$n_s_final = 0;
					$total_bobot = 0;
					foreach($nilai_siswa as $key => $n_s){
						$total_bobot += $set_bobot_baru[$key][0];
						if(count($n_s) > 1){
							$nilai_kd += max($n_s) * $set_bobot_baru[$key][0];
						} else {
							$nilai_kd += $n_s[0] * $set_bobot_baru[$key][0];
						}
					}
					$set_total_bobot = ($total_bobot) ? $total_bobot : 1;
					$skor_akhir = ($nilai_kd / $set_total_bobot);
				}
				$set_rerata += $skor_akhir;
			}
			if($set_rerata){
				$nilai_value = number_format($set_rerata / $pembagi,0);//.'=>'.$set_rerata.'=>'.$pembagi;
			} else {
				$nilai_value = '';
			}
		}
	}
	return $nilai_value;
}
function get_nilai_siswa_by_rencana($semester_id, $kompetensi_id, $rombongan_belajar_id, $mapel_id, $siswa_id, $rencana_penilaian_id){
	global $CI;
	$nilai_akhir = 0;
	$all_nilai = $CI->nilai->find_all("semester_id = $semester_id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mapel_id AND siswa_id = '$siswa_id' AND rencana_penilaian_id = '$rencana_penilaian_id'");
	$skor_akhir = 0;
	if($all_nilai){
		foreach($all_nilai as $nilai){
			$skor_akhir += $nilai->nilai;
		}
	}
	$result = number_format($skor_akhir / count($all_nilai), 0);
	return $result;
}
function sebaran($input, $a,$b){
	$range_data = range($a,$b);	
	$output = array_intersect($input , $range_data);
	return $output;
}
function sebaran_tooltip($input, $a,$b,$c){
	$CI = & get_instance();
	$range_data = range($a,$b);
	$output = array_intersect($input , $range_data);
	$data = array();
	$nama_siswa = '';
	foreach($output as $k=>$v){
		$data[] = get_nama_siswa($k);
	}
	if(count($output) == 0){
		$result = count($output);
	} else {
		$result = '<a class="tooltip-'.$c.'" href="javascript:void(0)" title="'.implode('<br />',$data).'" data-html="true">'.count($output).'</a>';
	}
	return $result;
}
function get_nama_siswa($id){
	global $CI;
	if($id){
		$query = $CI->siswa->get($id);
		$nama = isset($query->nama) ? strtoupper($query->nama) : '-';
	} else {
		$nama = '-';
	}
	return $nama;
}
function get_nilai_siswa_by_kd_new($semester_id, $kompetensi_id, $rombongan_belajar_id, $mapel_id, $siswa_id, $rencana_penilaian_id, $kompetensi_dasar_id){
	global $CI;
	$nilai_akhir = 0;
	$all_nilai = $CI->nilai->find("semester_id = $semester_id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $mapel_id AND siswa_id = '$siswa_id' AND rencana_penilaian_id = $rencana_penilaian_id AND kompetensi_dasar_id = $kompetensi_dasar_id");
	$result = ($all_nilai) ? $all_nilai->nilai : 0;
	return $result;
}
function get_nilai_siswa_by_kd($id_kd, $semester_id, $kompetensi_id, $rombongan_belajar_id, $mapel_id, $siswa_id, $query = 'asli'){
//get_nilai_siswa_by_kd(count($get_all_kd), $all_kd->id, $semester_id, $kompetensi_id, $siswa_id, 'asli');
	global $CI;
	$get_kd = $CI->kompetensi_dasar->get($id_kd);
	$set_nilai_akhir = 0;
	$nilai_akhir = 0;
	if($query == 'asli'){
		$all_nilai = $CI->nilai->with('rencana_penilaian')->find_all("semester_id = $semester_id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND siswa_id = '$siswa_id' AND kompetensi_dasar_id = $id_kd");
		if($all_nilai){
			if($kompetensi_id == 1){
				/*foreach($all_nilai as $set_nilai){
					$nilai_siswa[] = $set_nilai->nilai;
				}
				$nilai_akhir = array_sum($nilai_siswa) / count($nilai_siswa); */
				$nilai_kd = 0;
				$total_bobot = 0;
				foreach($all_nilai as $set_nilai){
					$bobot_kd = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->bobot : 1;
					//$nilai_kd += $set_nilai->nilai * $set_nilai->rencana_penilaian->bobot;
					//$total_bobot += $set_nilai->rencana_penilaian->bobot;
					$nilai_kd += $set_nilai->nilai * $bobot_kd;
					$total_bobot += $bobot_kd;
				}
				$nilai_akhir = ($nilai_kd / $total_bobot);
			} else {
				/*$nilai_siswa = array();
				foreach($all_nilai as $set_nilai){
					$nilai_siswa[$set_nilai->kompetensi_dasar_id.'_'.$set_nilai->rencana_penilaian->metode_id][] = $set_nilai->nilai;
				}
				ksort($nilai_siswa, SORT_NUMERIC);
				$n_s_final = 0;
				foreach($nilai_siswa as $n_s){
					if(count($n_s) > 1){
						$n_s_final += max($n_s) / count($nilai_siswa);
					} else {
						$n_s_final += array_sum($n_s) / count($nilai_siswa); 
					}
				}
				$nilai_akhir = $n_s_final;*/
				$nilai_siswa = array();
				foreach($all_nilai as $set_nilai){
					//test($set_nilai->rencana_penilaian);
					$bobot_kd = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->bobot : 1;
					$metode_id = ($set_nilai->rencana_penilaian) ? $set_nilai->rencana_penilaian->metode_id : gen_uuid();
					$find_teknik = $CI->teknik_penilaian->get($metode_id);
					//test($find_teknik);
					$nama_teknik = ($find_teknik) ? strtolower(str_replace('_',' ',$find_teknik->nama)) : '_';
					//$nilai_siswa[$get_kd->id_kompetensi.'_'.$metode_id][] = $set_nilai->nilai;
					//$set_bobot_baru[$get_kd->id_kompetensi.'_'.$metode_id][] = $bobot_kd;
					$nilai_siswa[$get_kd->id_kompetensi.'_'.$nama_teknik][] = $set_nilai->nilai;
					$set_bobot_baru[$get_kd->id_kompetensi.'_'.$nama_teknik][] = $bobot_kd;
				}
				//test($nilai_siswa);
				ksort($nilai_siswa, SORT_NUMERIC);
				$n_s_final = 0;
				$total_bobot = 0;
				$nilai_kd = 0;
				foreach($nilai_siswa as $key => $n_s){
					$total_bobot += $set_bobot_baru[$key][0];
					if(count($n_s) > 1){
						//echo 'max=>'.max($n_s).'=>'.$total_bobot;
						$nilai_kd += max($n_s) * $set_bobot_baru[$key][0];
						//test($n_s);
					} else {
						//echo 'rata=>'.$n_s[0].'=>'.$set_bobot_baru[$key][0];
						$nilai_kd += $n_s[0] * $set_bobot_baru[$key][0];
					}
					//if($nilai_kd){
						//echo '=>'.$total_bobot.'=>'.$nilai_kd;
						//test($set_bobot_baru[$key]);
					//}
				}
				$nilai_akhir = ($nilai_kd / $total_bobot);
			}
			$set_nilai_akhir += $nilai_akhir;
		}
		if($set_nilai_akhir){
			$nilai_akhir = $set_nilai_akhir;
			//number_format($set_nilai_akhir / $pembagi,0);
		} else {
			$nilai_akhir = '';
		}
	} else {
		$remedial = $CI->remedial->find("semester_id = $semester_id and kompetensi_id = $kompetensi_id and rombongan_belajar_id = '$rombongan_belajar_id' and mata_pelajaran_id = $mapel_id and siswa_id = '$siswa_id'");
		if($remedial){
			$nilai_akhir = unserialize($remedial->nilai);
		}
	}
	return $nilai_akhir;
}
function get_butir_sikap($id){
	global $CI;
	$butir_sikap = 'Butir sikap tidak ditemukan';
	if($id){
		$get_butir_sikap = $CI->sikap->get($id);
		$butir_sikap = ($get_butir_sikap) ? $get_butir_sikap->butir_sikap : '-';
	}
	return $butir_sikap;
}
function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}
function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}
function load_extensions(){
	$extension = 0;
	if (!extension_loaded('pdo_odbc')) {
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$extension = 'php_pdo_odbc.dll';
		} else {
			$extension = 'pdo_odbc.so';
		}
	} elseif (!extension_loaded('pdo_pgsql')) {
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$extension = 'php_pdo_pgsql.dll';
		} else {
			$extension = 'pdo_pgsql.so';
		}
	} elseif (!extension_loaded('pdo_sqlite')) {
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$extension = 'php_pdo_sqlite.dll';
		} else {
			$extension = 'pdo_sqlite.so';
		}
	} elseif (!extension_loaded('pgsql')) {
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$extension = 'php_pgsql.dll';
		} else {
			$extension = 'pgsql.so';
		}
	}
	return $extension;
}
function check_is_dapodik(){
	if (!extension_loaded('curl')) {
		if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
			$extension = 'php_curl.dll';
		} else {
			$extension = 'php_curl.so';
		}
		echo '<h1 style="text-align:center">Ekstensi '.$extension.' belum aktif.<br />
Silahkan aktifkan terlebih dahulu ekstensi tersebut di php.ini kemudian restart Apache!</h1>';
		die();
	}
	$root = "http://".$_SERVER['HTTP_HOST'];
	$parse = parse_url($root);
	$url = $parse['scheme'].'://'.$parse['host'].':5774/';
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_exec($ch);
	$details = curl_getinfo($ch);
	curl_close($ch);
	$respon = $details['http_code'];
	return $respon;
}
function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}
function get_jurusan($id){
	$CI = & get_instance();
	$CI->load->database();
	$CI->load->dbutil();
	$get_jurusan = $CI->jurusan->find("jurusan_id = '$id'");
	$result = ($get_jurusan) ? $get_jurusan->nama_jurusan : '-';
	return $result;
}
function get_ekskul($id){
	global $CI;
	$get_ekskul= $CI->ekstrakurikuler->get($id);
	$result = ($get_ekskul) ? $get_ekskul->nama_ekskul: '-';
	return $result;
}
function filter_agama_mapel($ajaran_id,$get_id_mapel, $all_mapel,$agama_siswa){
	global $CI;
	$get_mapel_agama = $CI->mata_pelajaran->get_many($get_id_mapel);
	foreach($get_mapel_agama as $agama){
		if (strpos(get_nama_mapel($agama->mata_pelajaran_id),get_agama($agama_siswa)) === false) {
			$mapel_agama_jadi[] = $agama->mata_pelajaran_id;
		}
	}
	if(isset($mapel_agama_jadi) && $all_mapel){
		$all_mapel = array_diff($all_mapel, $mapel_agama_jadi);
	}
	return $all_mapel;
}
function konversi_huruf($kkm_value, $n, $produktif = NULL, $show='predikat'){
$predikat	= 0;
$sikap		= 0;
$sikap_full	= 0;
$check_2018 = check_2018();
if($check_2018){
	$show = 'predikat';
	$a = predikat($kkm_value,'A') + 1;
	$a_min = predikat($kkm_value,'A-') + 1;
	$b_plus = predikat($kkm_value,'B+') + 1;
	$b = predikat($kkm_value,'B') + 1;
	$b_min = predikat($kkm_value,'B-') + 1;
	$c = predikat($kkm_value,'C') + 1;
	$d = predikat($kkm_value,'D', $produktif) + 1;
	if($n == 0){
		$predikat 	= '-';
	} elseif($n >= $a){//$settings->a_min){ //86
		$predikat 	= 'A+';
	} elseif($n >= $a_min){//$settings->a_min){ //86
		$predikat 	= 'A';
	} elseif($n >= $b_plus){//$settings->a_min){ //86
		$predikat 	= 'A-';
	} elseif($n >= $b){//$settings->a_min){ //86
		$predikat 	= 'B+';
	} elseif($n >= $b_min){//$settings->a_min){ //86
		$predikat 	= 'B';
	} elseif($n >= $c){//$settings->a_min){ //86
		$predikat 	= 'B-';
	} elseif($n >= $d){//$settings->a_min){ //86
		$predikat 	= 'C';
	} elseif($n < $d){//$settings->a_min){ //86
		$predikat 	= 'D';
	}
} else {
	$b = predikat($kkm_value,'b') + 1;
	$c = predikat($kkm_value,'c') + 1;
	$d = predikat($kkm_value,'d') + 1;
	if($n == 0){
		$predikat 	= '-';
		$sikap		= '-';
		$sikap_full	= '-';
	} elseif($n >= $b){//$settings->a_min){ //86
		$predikat 	= 'A';
		$sikap		= 'SB';
		$sikap_full	= 'Sangat Baik';
	} elseif($n >= $c){ //71
		$predikat 	= 'B';
		$sikap		= 'B';
		$sikap_full	= 'Baik';
	} elseif($n >= $d){ //56
		$predikat 	= 'C';
		$sikap		= 'C';
		$sikap_full	= 'Cukup';
	} elseif($n < $d){ //56
		$predikat 	= 'D';
		$sikap		= 'K';
		$sikap_full	= 'Kurang';
	}
}
	if($show == 'predikat'){
		$html = $predikat;
	} elseif($show == 'sikap'){
		$html = $sikap;
	} elseif($show == 'sikap_full'){
		$html = $sikap_full;
	} else {
		$html = 'Unknow';
	}
	return $html;
}
function predikat($kkm, $a, $produktif = NULL){
	//(100-65)/3 = 35/3 = 11,67 = 12
	$check_2018 = check_2018();
	if($check_2018){
		if($produktif){
			$result = array(
				'A+'	=> 100, // 95 - 100
				'A'		=> 94, // 90 - 94
				'A-'	=> 89, // 85 - 89
				'B+'	=> 84, // 80 - 84
				'B'		=> 79, // 75 - 79
				'B-'	=> 74, // 70 - 74
				'C'		=> 69, // 65 - 69
				'D'		=> 64, // 0 - 59
			);
		} else {
			$result = array(
				'A+'	=> 100, // 95 - 100
				'A'		=> 94, // 90 - 94
				'A-'	=> 89, // 85 - 89
				'B+'	=> 84, // 80 - 84
				'B'		=> 79, // 75 - 79
				'B-'	=> 74, // 70 - 74
				'C'		=> 69, // 60 - 69
				'D'		=> 59, // 0 - 59
			);
		}
	} else {
		$rumus = (100-$kkm) / 3;
		$rumus = number_format($rumus,0);
		$result = array(
					'd'	=> $kkm - 1,
					'c'	=> $kkm + $rumus,
					'b'	=> $kkm + ($rumus * 2),
					'a'	=> $kkm + ($rumus * 3),
					);
		/*$result = array(
					'd'	=> 55, // 0 - 55 D
					'c'	=> 70, // 56 - 70 C
					'b'	=> 85, // 71 - 85 D
					'a'	=> 100, // 86 - 100 A
					);*/
	}
	if($result[$a] > 100)
		$result[$a] = 100;
	return $result[$a];
}
function get_kkm($semester_id, $rombongan_belajar_id, $id){
	global $CI;
	$check_2018 = check_2018();
	if($check_2018){
		$get_kkm = $CI->pembelajaran->find("semester_id = $semester_id and rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $id");
		if($get_kkm){
			$kelompok_id = $get_kkm->kelompok_id;
			$produktif = array(4,5,9,10,13);
			$non_produktif = array(1,2,3,6,7,8,11,12,99);
			if(in_array($kelompok_id,$produktif)){
				$kkm = 65;
			} elseif(in_array($kelompok_id,$non_produktif)) {
				$kkm = 60;
			} else {
				$kkm = 0;
			}
		} else {
			$kkm = 0;
		}
		return $kkm;
	} else {
		$get_kkm = $CI->pembelajaran->find("semester_id = $semester_id and rombongan_belajar_id = '$rombongan_belajar_id' AND mata_pelajaran_id = $id");
		$kkm = isset($get_kkm->kkm) && $get_kkm->kkm ? $get_kkm->kkm : 0;
		return $kkm;
	}
}
function get_deskripsi_nilai($ajaran_id, $rombel_id, $mapel_id, $siswa_id,$query){
	global $CI;
	$deskripsi = $CI->deskripsi_mata_pelajaran->find("semester_id = $ajaran_id AND rombongan_belajar_id = '$rombel_id' AND mata_pelajaran_id = $mapel_id AND siswa_id = '$siswa_id'");
	//Deskripsi::find_by_ajaran_id_and_rombel_id_and_mapel_id_and_siswa_id($ajaran_id, $rombel_id, $mapel_id, $siswa_id);
	$set_deskripsi[1] = '';
	$set_deskripsi[2] = '';
	if($deskripsi){
		$set_deskripsi[1] = $deskripsi->deskripsi_pengetahuan;
		$set_deskripsi[2] = $deskripsi->deskripsi_keterampilan;
	}
	return $set_deskripsi[$query];
}
function indo_date($date = ''){
	$formated_date = date('Y-m-d', strtotime($date));
	$BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
	    $tahun = substr($formated_date, 0, 4);
    	$bulan = substr($formated_date, 5, 2);
	    $tgl   = substr($formated_date, 8, 2);
    	$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun; 
    	return($result);
	//return $formated_date;
}
function get_ppk_siswa($ppk_id, $siswa_id){
	global $CI;
	$catatan_ppk = $CI->catatan_ppk->find("ppk_id = $ppk_id AND siswa_id = '$siswa_id'");
	//1Catatanppk::find_by_ppk_id_and_siswa_id($ppk_id, $siswa_id);
	$result = ($catatan_ppk) ? $catatan_ppk->catatan : '';
	return $result;
}
function get_kompetensi_dasar($kompetensi_dasar_id,$query='nama'){
	global $CI;
	$get_kompetensi_dasar = $CI->kompetensi_dasar->get($kompetensi_dasar_id);
	$result['id'] 	= ($get_kompetensi_dasar) ? $get_kompetensi_dasar->id_kompetensi : 0;
	$result['nama'] = ($get_kompetensi_dasar) ? ($get_kompetensi_dasar->kompetensi_dasar_alias) ? $get_kompetensi_dasar->kompetensi_dasar_alias : $get_kompetensi_dasar->kompetensi_dasar : 0;
	return $result[$query];
}
function clean($strings){
	$string = preg_replace('/[^a-zA-Z0-9\']/', ' ', $strings);
	$string = str_replace("'", '', $string);
	$string = trim($string);
	return $string;
}
function get_nama_ppk($id){
	global $CI;
	$get_nama_ppk = $CI->ref_ppk->get($id);
	$result = ($get_nama_ppk) ? $get_nama_ppk->nama : '0';
	return $result;
}
function get_kelompok($id){
	global $CI;
	$get_kelompok = $CI->kelompok->get($id);
	$result = ($get_kelompok) ? $get_kelompok->nama_kelompok : '0';
	return $result;
}
function find_jurusan($jurusan_id){
	global $CI;
	$find_ref_jurusan = $CI->jurusan->find_by_jurusan_id($jurusan_id);
	if(!$find_ref_jurusan && $jurusan_id){
		$query_get_ref_jurusan_dapo = $CI->_database->get_where('ref.jurusan', array('jurusan_id' => $jurusan_id));
		$get_ref_jurusan_dapo = $query_get_ref_jurusan_dapo->row();
		$jurusan_induk = ($get_ref_jurusan_dapo->jurusan_induk) ? $get_ref_jurusan_dapo->jurusan_induk : 0;
		$insert_jurusan = array(
			'jurusan_id'		=> $get_ref_jurusan_dapo->jurusan_id,
			'jurusan_induk'		=> $jurusan_induk,
			'nama_jurusan'		=> $get_ref_jurusan_dapo->nama_jurusan,
			'level_bidang_id'	=> $get_ref_jurusan_dapo->level_bidang_id,
		);
		$CI->jurusan->insert($insert_jurusan);
		return find_jurusan($jurusan_induk);
	} else {
		return true;
	}
}
function migrasi_field($field, $table){
	$CI =& get_instance();
	$CI->load->database();
	$CI->load->dbutil();
	if (!$CI->db->field_exists($field, $table)){
		redirect('migrate');
	}
}
function migrasi_jurusan_sp(){
	$CI =& get_instance();
	$CI->load->database();
	$CI->load->dbutil();
	$get_jurusan = $CI->jurusan_sp->get_all();
	foreach($get_jurusan as $jurusan){
		$find_jurusan = get_jurusan($jurusan->kurikulum_id);
		if($find_jurusan == '-'){
			$get_kurikulum = $CI->kurikulum->find_by_kurikulum_id($jurusan->kurikulum_id);
			if($get_kurikulum){
				$CI->jurusan_sp->update($jurusan->id, array('kurikulum_id' => $get_kurikulum->jurusan_id));
			}
		}
	}
}
function check_mapel_ganda(){
	$CI =& get_instance();
	$CI->load->database();
	$CI->load->dbutil();
	$query = $CI->db->query("SELECT id_nasional FROM ref_mata_pelajaran WHERE id_nasional != 0 group by id_nasional having count(*) >= 2");
	$result = $query->result();
	if($result){
		$CI->db->truncate('ref_mata_pelajaran');
	}
}
function clean_table($table){
	$CI =& get_instance();
	$CI->load->database();
	$CI->load->dbforge();
	if ($CI->db->table_exists($table)){
		$CI->dbforge->drop_table($table,TRUE);
	}
}
function delete_kurikulum(){
	$CI =& get_instance();
	if ($CI->db->table_exists('mata_pelajaran_kurikulum') && $CI->db->table_exists('ref_mata_pelajaran')){
		$CI->load->model('mata_pelajaran_kurikulum_model', 'mata_pelajaran_kurikulum');
		$delete_kurikulum = $CI->mata_pelajaran_kurikulum->find_all("mata_pelajaran_id NOT IN(SELECT id FROM ref_mata_pelajaran)");
		if($delete_kurikulum){
			foreach($delete_kurikulum as $del_kur){
				$CI->mata_pelajaran_kurikulum->delete($del_kur->id);
			}
		}
	}
	$file = APPPATH.'migrations/001_install_ion_auth.php';
	if(is_file($file)){
		chmod($file, 0777); 
		unlink($file);
	}
}
function check_row_nilai(){
	$CI =& get_instance();
	$CI->load->database();
	if ($CI->db->table_exists('nilais')){
		$count = $CI->db->count_all_results('nilais');
		if($count == 0){
			clean_table('nilais');
			clean_table('rencana_penilaians');
			clean_table('rencanas');
			//clean_table('data_mapels');
		}
	}
}
function reorder_kelompok(){
	$CI =& get_instance();
	$CI->load->database();
	$get_kelompok = $CI->kelompok->find_all("id IN(13,14,15)");
	if($get_kelompok){
		foreach($get_kelompok as $kelompok){
			if($kelompok->nama_kelompok != 'Produktif'){
				$CI->kelompok->delete($kelompok->id);
			}
		}
	}
	$get_kelompok = $CI->kelompok->get(13);
	if(!$get_kelompok){
		 $insert_kelompok = array('id' => '13','nama_kelompok' => 'Produktif','kurikulum' => '2006','created_at' => '2017-09-19 10:47:50','updated_at' => '2017-09-19 10:47:50');
		$CI->kelompok->insert($insert_kelompok);
	}
	$ref_a_rev = $CI->kelompok->get(6);
	if($ref_a_rev){
		if($ref_a_rev->nama_kelompok == 'Kelompok A (Wajib)'){
			 $CI->kelompok->update(6, array('nama_kelompok' => 'Kelompok A (Muatan Nasional)'));
		}
	}
	$ref_b_rev = $CI->kelompok->get(7);
	if($ref_b_rev){
		if($ref_b_rev->nama_kelompok == 'Kelompok B (Wajib)'){
			$CI->kelompok->update(7, array('nama_kelompok' => 'Kelompok B (Muatan Kewilayahan)'));
		}
	}
}
function get_version(){
	//global $CI;
	$CI = & get_instance();
	$get_version = $CI->settings->get(1);
	$result = ($get_version) ? $get_version->version : 0;
	return $result;
}
function limit_words($id,$string, $word_limit){
	$string = add_responsive_class($string);
	$words = explode(" ",$string);
	if(count($words)>$word_limit){
		return implode(" ",array_splice($words,0,$word_limit)).' <a href="'.site_url('admin/laporan/view_deskripsi_antar_mapel/'.$id).'">Selengkapnya &raquo;</a>';
	} else {
		return $string;
	}
}
function add_responsive_class($content){
	$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
	$document = new DOMDocument();
	libxml_use_internal_errors(true);
	$document->loadHTML(utf8_decode($content));
	$imgs = $document->getElementsByTagName('img');
	foreach ($imgs as $img){
		$img->setAttribute('class','gambar');
	}
	$html = $document->saveHTML();
	$html = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">','',$html);
	$html = str_replace('<html><body>','',$html);
	$html = str_replace('</body></html>','',$html);
	return $html;
}
function get_nama_ekskul($id){
	global $CI;
	$ekskul = $CI->ekstrakurikuler->get($id);
	$nama_ekskul = ($ekskul) ? $ekskul->nama_ekskul : '-';
	return $nama_ekskul;
}
function get_nilai_ekskul($id){
	if($id == 1){
		$nilai_ekskul = 'Sangat Baik';
	} elseif($id == 2){
		$nilai_ekskul = 'Baik';
	} elseif($id == 3){
		$nilai_ekskul = 'Cukup';
	} elseif($id == 4){
		$nilai_ekskul = 'Kurang';
	} else {
		$nilai_ekskul = '-';
	}
	return $nilai_ekskul;
}
function status_connect($status){
	if($status == 1) : 
		$label = '<span class="btn btn-sm btn-success active"> <strong>CONNECTED</strong> </span>';
	elseif ($status == 0) : 
		$label = '<span class="btn btn-sm btn-danger active"> <strong>DISCONNECTED</strong> </span>';
	endif;
	return $label;
}
function set_time_zone(){
	$CI = & get_instance();
	if ($CI->db->table_exists('settings')){
		$CI->load->model('settings_model', 'settings');
		$get_settings = $CI->settings->get(1);
		$get_timezone = ($get_settings) ? $get_settings->zona : 1;
		if($get_timezone == 1){
			date_default_timezone_set('Asia/Jakarta');
		}elseif($get_timezone == 2){
			date_default_timezone_set('Asia/Ujung_Pandang');
		}elseif($get_timezone == 3){
			date_default_timezone_set('Asia/Jayapura');
		}
	}
}
function save_to_png($data){
	$img = $_POST['img'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$file = MEDIAFOLDER . uniqid() . '.png';
	$success = file_put_contents($file, $data);
	//print $success ? $file : 'Unable to save the file.';
}
function base64ToImage($imageData){
	if (check_base64_image($imageData)) {
		$data = 'data:image/png;base64,AAAFBfj42Pj4';
		list($type, $imageData) = explode(';', $imageData);
		list(,$extension) = explode('/',$type);
		list(,$imageData)      = explode(',', $imageData);
		$fileName = MEDIAFOLDER . uniqid().'.'.$extension;
		$imageData = base64_decode($imageData);
		file_put_contents($fileName, $imageData);
		return $fileName;
	} else {
		return str_replace(base_url(),'',$imageData);
	}
}
function find_img($content){
	$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
	$document = new DOMDocument();
	libxml_use_internal_errors(true);
	$document->loadHTML(utf8_decode($content));
	$imgs = $document->getElementsByTagName('img');
	$images = array();
	foreach ($imgs as $img){
		$img1[] = base64ToImage($img->getAttribute('src'));
		$img2[] = $img->getAttribute('src');
		//$html = preg_replace("/<img[^>]+\>/i", '<img src="'.base64ToImage($img->getAttribute('src')).'">', $html); 
	}
	$html = $document->saveHTML();
	if(isset($img2)){
		foreach($img2 as $k=>$dua){
			$html = str_replace($dua,base_url($img1[$k]),$html);
		}
	}
	$html = str_replace('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">','',$html);
	$html = str_replace('<html><body>','',$html);
	$html = str_replace('</body></html>','',$html);
	return $html;
}
function check_base64_image($imageData) {
	$findme   = base_url();
	$pos = strpos($imageData, $findme);
	if ($pos !== false) {
     	return false;
	} else {
		return true;
	}
}
function get_pekerjaan($id){
	$CI = & get_instance();
	$CI->load->model('pekerjaan_model', 'pekerjaan');
	$query = $CI->pekerjaan->get($id);
	$result = ($query) ? $query->nama : '-';
	return $result;
}
function diterima_kelas($peserta_didik_id){
	$CI = & get_instance();
	$CI->_database->select('*');
	$CI->_database->from('anggota_rombel');
	$CI->_database->join('rombongan_belajar', 'rombongan_belajar.rombongan_belajar_id = anggota_rombel.rombongan_belajar_id');
	$CI->_database->where('peserta_didik_id', $peserta_didik_id);
	$CI->_database->where('jenis_pendaftaran_id', 1);
	$query = $CI->_database->get();
	$find_diterima_kelas = $query->row();
	$diterima_kelas = ($find_diterima_kelas) ? $find_diterima_kelas->nama : '-';
	return $diterima_kelas;
}
function update_version($versi){
	$CI = & get_instance();
	$settings 	= $CI->settings->get(1);
	if($settings->version < $versi){
		$CI->settings->update(1, array('version' => $versi));
	}
}
function get_menu(){
	$CI = & get_instance();
	$main_menu = $CI->db->get_where('menu', array('is_main_menu' => 0));
	$html = '';
	foreach ($main_menu->result() as $main) {
		$sub_menu = $CI->db->get_where('menu', array('is_main_menu' => $main->id));
		if ($sub_menu->num_rows() > 0) {
			$html .= "<li class='treeview'>" . anchor($main->link, '<i class="' . $main->icon . '"></i>' . $main->judul_menu . '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>');
			$html .= "<ul class='treeview-menu'>";
			foreach ($sub_menu->result() as $sub) {
				$html .= "<li>" . anchor($sub->link, '<i class="' . $sub->icon . '"></i>' . $sub->judul_menu) . "</li>";
			}
			$html .= "</ul></li>";
		} else {
			$html .= "<li>" . anchor($main->link, '<i class="' . $main->icon . '"></i>' . $main->judul_menu) . "</li>";
		}
	}
	return $html;
}
function get_nilai_tertinggi($semester_id, $kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $siswa_id){
	$CI = & get_instance();
	//$CI->db->select('siswa_id,kd_nilai_id,MAX(nilai) as nilai_tinggi');
	/*$CI->db->select('MAX(nilai) as nilai_tinggi');
	$CI->db->where('semester_id', $ajaran_id);
	$CI->db->where('kompetensi_id', $kompetensi_id);
	$CI->db->where('rombongan_belajar_id', $rombongan_belajar_id);
	$CI->db->where('mata_pelajaran_id', $mata_pelajaran_id);
	$CI->db->where('siswa_id', $siswa_id);
	$query = $CI->db->get('nilai');*/
	$query = $CI->db->query("select kd_nilai_id from nilai a where a.nilai = (select max(b.nilai) from nilai b where b.semester_id = $semester_id AND b.kompetensi_id = $kompetensi_id AND b.rombongan_belajar_id = '$rombongan_belajar_id' AND b.mata_pelajaran_id = $mata_pelajaran_id AND b.siswa_id = '$siswa_id') AND a.semester_id = $semester_id AND a.kompetensi_id = $kompetensi_id AND a.rombongan_belajar_id = '$rombongan_belajar_id' AND a.mata_pelajaran_id = $mata_pelajaran_id AND a.siswa_id = '$siswa_id' GROUP BY a.kd_nilai_id");
	$result = '';
	if ($query->num_rows() > 0){
		$result = $query->result();
	}
	return $result;
}
function get_nilai_terendah($semester_id, $kompetensi_id, $rombongan_belajar_id, $mata_pelajaran_id, $siswa_id){
	$CI = & get_instance();
	$query = $CI->db->query("select kd_nilai_id from nilai a where a.nilai = (select min(b.nilai) from nilai b where b.semester_id = $semester_id AND b.kompetensi_id = $kompetensi_id AND b.rombongan_belajar_id = '$rombongan_belajar_id' AND b.mata_pelajaran_id = $mata_pelajaran_id AND b.siswa_id = '$siswa_id') AND a.semester_id = $semester_id AND a.kompetensi_id = $kompetensi_id AND a.rombongan_belajar_id = '$rombongan_belajar_id' AND a.mata_pelajaran_id = $mata_pelajaran_id AND a.siswa_id = '$siswa_id' GROUP BY a.kd_nilai_id");
	$result = '';
	if ($query->num_rows() > 0){
		$result = $query->result();
	}
	return $result;
}
function update_sync(){
	$CI = & get_instance();
	$table_sync = array(
		1	=> 'absen',
		2	=> 'anggota_rombel',
		3	=> 'catatan_ppk',
		4	=> 'catatan_wali',
		5	=> 'deskripsi_mata_pelajaran',
		6	=> 'deskripsi_sikap',
		7	=> 'ekstrakurikuler',
		8	=> 'guru_terdaftar',
		9	=> 'jurusan_sp',
		10	=> 'kd_nilai',
		11	=> 'nilai',
		11	=> 'nilai_ekstrakurikuler',
		12	=> 'nilai_sikap',
		13	=> 'pembelajaran',
		14	=> 'prakerin',
		15	=> 'prestasi',
		16	=> 'ref_guru',
		17	=> 'ref_sekolah',
		18	=> 'ref_sikap',
		19	=> 'ref_siswa',
		20	=> 'remedial',
		21	=> 'rencana_penilaian',
		22	=> 'rombongan_belajar',
		23	=> 'teknik_penilaian',
	);
	foreach($table_sync as $sync){
		$CI->db->select('*');
		$CI->db->from($sync);
		$CI->db->where('last_sync IS NULL');
		$CI->db->limit(1); 
		$query = $CI->db->get();
		$result = $query->row();
		if($result){
			$CI->db->set('last_sync', date('Y-m-d H:i:s'));
			$CI->db->where('last_sync IS NULL');
			$CI->db->update($sync);
		}
	}
}
function kunci_nilai($rombel_id){
	$CI = & get_instance();
	$rombongan_belajar = $CI->rombongan_belajar->get($rombel_id);
	if($rombongan_belajar->kunci_nilai){
		$wali_kelas = get_nama_guru($rombongan_belajar->guru_id);
		echo '<div class="alert alert-danger">';
		echo "<b>Penilaian Ditutup! </b> Silahkan menghubungi wali kelas <strong>$rombongan_belajar->nama</strong>, <strong>$wali_kelas</strong> untuk membuka kembali penilaian di kelas ini.";
		echo '</div>';
		echo '<script>$("a#rerata").remove();</script>';
		echo '<script>$("a#rerata_remedial").remove();</script>';
		exit;
	}
}
function jenis_gtk($query){
	$data['tendik'] = '11,30,40,41,42,43,44,57,58,59';
	$data['guru'] = '3,4,5,6,7,8,9,10,12,13,14,20,25,26,51,52,53,54,56';
	$data['pengajar'] = '99';
	return $data[$query];
}
function get_pembelajaran($semester_id, $mata_pelajaran_id, $rombongan_belajar_id){
	$CI = & get_instance();
	$get_pembelajaran = $CI->pembelajaran->find("semester_id = $semester_id AND mata_pelajaran_id = $mata_pelajaran_id AND rombongan_belajar_id = '$rombongan_belajar_id'");
	$pembelajaran_id = ($get_pembelajaran) ? $get_pembelajaran->pembelajaran_id : 0;
	return $pembelajaran_id;
}
function cari_pembelajaran_id_kosong(){
	$CI = & get_instance();
	$get_perencanaan = $CI->rencana_penilaian->find_all("pembelajaran_id IS NULL");
	if($get_perencanaan){
		foreach($get_perencanaan as $perencanaan){
			$pembelajaran_id = get_pembelajaran($perencanaan->semester_id, $perencanaan->mata_pelajaran_id, $perencanaan->rombongan_belajar_id);
			if($pembelajaran_id){
				$CI->rencana_penilaian->update($perencanaan->rencana_penilaian_id, array('pembelajaran_id' => $pembelajaran_id));
			}
		}
	}
}
function check_2018(){
	$ajaran = get_ta();
	$tahun = substr($ajaran->tahun,0,4);
	if($tahun >= 2018){
		return true;
	} else {
		return false;
	}
}
function insert_pembelajaran_id_into_perencanaan($semester_id, $guru_id, $kompetensi_id){
	global $CI;
	$all_pembelajaran = $CI->pembelajaran->find_all("semester_id = $semester_id AND guru_id = '$guru_id' OR semester_id = $semester_id AND guru_pengajar_id = '$guru_id'");
	if($all_pembelajaran){
		foreach($all_pembelajaran as $pembelajaran){
			$rencana_penilaian = $CI->rencana_penilaian->find_all("semester_id = $semester_id AND kompetensi_id = $kompetensi_id AND rombongan_belajar_id = '$pembelajaran->rombongan_belajar_id' AND mata_pelajaran_id = '$pembelajaran->mata_pelajaran_id' AND pembelajaran_id IS NULL");
			if($rencana_penilaian){
				foreach($rencana_penilaian as $rencana){
					$CI->rencana_penilaian->update($rencana->rencana_penilaian_id, array('pembelajaran_id' => $pembelajaran->pembelajaran_id));
				}
			}
		}
	}
}
function penyebut($nilai) {
	$nilai = abs($nilai);
	$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	$temp = "";
	if ($nilai < 12) {
		$temp = " ". $huruf[$nilai];
	} else if ($nilai <20) {
		$temp = penyebut($nilai - 10). " belas";
	} else if ($nilai < 100) {
		$temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
	} else if ($nilai < 200) {
		$temp = " seratus" . penyebut($nilai - 100);
	} else if ($nilai < 1000) {
		$temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
	} else if ($nilai < 2000) {
		$temp = " seribu" . penyebut($nilai - 1000);
	} else if ($nilai < 1000000) {
		$temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
	} else if ($nilai < 1000000000) {
		$temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
	} else if ($nilai < 1000000000000) {
		$temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
	} else if ($nilai < 1000000000000000) {
		$temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
	}     
	return $temp;
}
function terbilang($nilai) {
	if($nilai<0) {
		$hasil = "minus ". trim(penyebut($nilai));
	} else {
		$hasil = trim(penyebut($nilai));
	}     		
	return ucwords($hasil);
}
function get_bobot_mapel_old($semester_id, $mata_pelajaran_id, $rombongan_belajar_id, $kompetensi_id){
	global $CI;
	$find_all_rencana = $CI->rencana_penilaian->find_all("mata_pelajaran_id = $mata_pelajaran_id AND semester_id = $semester_id AND rombongan_belajar_id = '$rombongan_belajar_id' AND kompetensi_id = $kompetensi_id");
	$bobot = 0;
	if($find_all_rencana){
		foreach($find_all_rencana as $all_rencana){
			$bobot += $all_rencana->bobot;
		}
	}
	return $bobot;
}
function get_bobot_mapel($semester_id, $mata_pelajaran_id, $rombongan_belajar_id, $kompetensi_id){
	global $CI;
	$pembelajaran = $CI->pembelajaran->find("semester_id = $semester_id AND mata_pelajaran_id = $mata_pelajaran_id AND rombongan_belajar_id = '$rombongan_belajar_id'");
	$result = '';
	if($pembelajaran){
		$result = ($kompetensi_id == 1) ? $pembelajaran->rasio_p : $pembelajaran->rasio_k;
	}
	if(!$result){
		$result = 50;
	}
	return $result;
}
function insert_indikator($sikap_id){
	global $CI;
	if($sikap_id == 1){
		$insert_indikator = array('Melindungi yang kecil dan tersisih', 'Taat beribadah', 'Menjalankan ajaran agama', 'Menjauhi larangan agama');
	} elseif($sikap_id == 7){
		$insert_indikator = array('Tangguh', 'Kerja keras', 'Kreatif', 'Keberanian', 'Pembelajar', 'Daya juang', 'Berwawasan informasi dan teknologi');
	} elseif($sikap_id == 19){
		$insert_indikator = array('Kesetiaan', 'Antikorupsi', 'Keteladanan', 'Keadilan', 'Menghargai martabat manusia');
	} elseif($sikap_id == 20){
		$insert_indikator = array('Rela berkorban', 'Taat hukum', 'Unggul', 'Disiplin', 'Berprestasi', 'Cinta damai');
	} elseif($sikap_id == 21){
		$insert_indikator = array('Musyawarah', 'Tolong menolong', 'Kerelawanan', 'Solidaritas', 'Anti diskriminasi');
	}
	foreach($insert_indikator as $indikator){
		$a_i = array(
			'sikap_id'		=> $sikap_id,
			'nama'			=> $indikator,
			'created_at'	=> date('Y-m-d H:i:s'),
			'updated_at'	=> date('Y-m-d H:i:s')
		);
		$CI->indikator_karakter->insert($a_i);
	}
}
function generate_teknik_keterampilan($sekolah_id){
	global $CI;
	$teknik_keterampilan_hapus = $CI->teknik_penilaian->find_all("kompetensi_id = 2 AND bobot = 0");
	if($teknik_keterampilan_hapus){
		foreach($teknik_keterampilan_hapus as $teknik_hapus){
			$CI->teknik_penilaian->delete($teknik_hapus->teknik_penilaian_id);
		}
	}
	$teknik_keterampilan_insert = $CI->teknik_penilaian->find_all("kompetensi_id = 2 AND bobot != 0");
	if(!$teknik_keterampilan_insert){
		$insert_teknik = array(
			array(
				'teknik_penilaian_id' 	=> gen_uuid(),
				'sekolah_id'			=> $sekolah_id,
				'kompetensi_id'			=> 2,
				'nama'					=> 'Portofolio',
				'bobot'					=> 1,
				'created_at'			=> date('Y-m-d H:i:s'),
				'updated_at'			=> date('Y-m-d H:i:s'),
			),
			array(
				'teknik_penilaian_id' 	=> gen_uuid(),
				'sekolah_id'			=> $sekolah_id,
				'kompetensi_id'			=> 2,
				'nama'					=> 'Proyek',
				'bobot'					=> 1,
				'created_at'			=> date('Y-m-d H:i:s'),
				'updated_at'			=> date('Y-m-d H:i:s'),
			),
			array(
				'teknik_penilaian_id' 	=> gen_uuid(),
				'sekolah_id'			=> $sekolah_id,
				'kompetensi_id'			=> 2,
				'nama'					=> 'Kinerja',
				'bobot'					=> 1,
				'created_at'			=> date('Y-m-d H:i:s'),
				'updated_at'			=> date('Y-m-d H:i:s'),
			),
		);
		//test($insert_teknik);
		$CI->db->insert_batch('teknik_penilaian', $insert_teknik);
	}
}
function generate_guru_id_nilai_sikap($sekolah_id){
	global $CI;
	$all_sikap = $CI->nilai_sikap->find_all("sekolah_id = '$sekolah_id' AND guru_id IS NULL");
	if($all_sikap){
		foreach($all_sikap as $sikap){
			$pembelajaran = $CI->pembelajaran->find("semester_id = $sikap->semester_id AND mata_pelajaran_id = $sikap->mata_pelajaran_id AND rombongan_belajar_id = '$sikap->rombongan_belajar_id'");
			if($pembelajaran){
				if($pembelajaran->guru_id){
					$guru_id = $pembelajaran->guru_id;
				} elseif($pembelajaran->guru_pengajar_id){
					$guru_id = $pembelajaran->guru_pengajar_id;
				} else {
					$guru_id = NULL;
				}
				$CI->nilai_sikap->update($sikap->nilai_sikap_id, array('guru_id' => $guru_id));
			}
		}
	}
}
function is_guru_sikap($guru_id, $semester_id){
	global $CI;
	$mapel_sikap = array(100011070, 100012050, 100013010, 100014140, 100015010, 100016010, 500050000, 200010000);
	$mapel_sikap = implode(', ',$mapel_sikap);
	//AND is_dapodik = 1 
	$find_pembelajaran = $CI->pembelajaran->find("semester_id = $semester_id AND mata_pelajaran_id IN($mapel_sikap) AND guru_id = '$guru_id' OR semester_id = $semester_id AND mata_pelajaran_id IN($mapel_sikap) AND guru_pengajar_id = '$guru_id'");
	//test($find_pembelajaran);
	if($find_pembelajaran){
		return true;
	} else {
		return false;
	}
}
function create_default_perencanaan($sekolah_id){
	global $CI;
	$data_penilaian = array(
		array(
			'teknik_penilaian_id'	=> gen_uuid(),
			'sekolah_id' 			=> $sekolah_id,
			'kompetensi_id' 		=> 1,
			'nama'					=> 'Tes Tertulis',
		),
		array(
			'teknik_penilaian_id'	=> gen_uuid(),
			'sekolah_id' 			=> $sekolah_id,
			'kompetensi_id' 		=> 1,
			'nama'					=> 'Tes Lisan',
		),
		array(
			'teknik_penilaian_id'	=> gen_uuid(),
			'sekolah_id' 			=> $sekolah_id,
			'kompetensi_id' 		=> 1,
			'nama'					=> 'Penugasan',
		),
		array(
			'teknik_penilaian_id'	=> gen_uuid(),
			'sekolah_id' 			=> $sekolah_id,
			'kompetensi_id' 		=> 1,
			'nama'					=> 'Portofolio',
		),
		array(
			'teknik_penilaian_id'	=> gen_uuid(),
			'sekolah_id' 			=> $sekolah_id,
			'kompetensi_id' 		=> 2,
			'nama'					=> 'Kinerja',
		),
		array(
			'teknik_penilaian_id'	=> gen_uuid(),
			'sekolah_id' 			=> $sekolah_id,
			'kompetensi_id' 		=> 2,
			'nama'					=> 'Proyek',
		),
		array(
			'teknik_penilaian_id'	=> gen_uuid(),
			'sekolah_id' 			=> $sekolah_id,
			'kompetensi_id' 		=> 2,
			'nama'					=> 'Portofolio',
		),
	);
	foreach($data_penilaian as $data){
		$CI->teknik_penilaian->insert($data);
	}
}