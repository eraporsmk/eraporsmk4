### Version {4.1.0}

Versi PHP yang dibutuhkan : **php5.6** (karena membutuhkan ioncube-loader5.6, dan library mpdf tidak didukung di php7). Contoh petunjuk instalasi php5.6 pada distro Ubuntu >16.04 LTS yang memiliki versi php default >5.6 : https://tecadmin.net/install-php5-on-ubuntu/

#### Install semua dependency yang dibutuhkan (contoh di Debian 9. Untuk di Ubuntu kurang lebih sama): 

<code># apt install openssh-server wget net-tools git postgresql postgresql-contrib apt-transport-https lsb-release ca-certificates</code>

#### Install semua modul/ekstensi php5.6 yang dibutuhkan eRapor SMK: 

<code># apt install php5.6 php5.6 libapache2-mod-php5.6 php5.6-pgsql php5.6-odbc php5.6-sqlite php5.6-bz2 php5.6-curl php5.6-gd php5.6-mbstring php5.6-xmlrpc php5.6-intl php5.6-soap php5.6-xml php5.6-zip</code>

#### Instalasi ioncube-loader: 

download IonCube Loader menyesuaikan arsitektur OS Anda 

64 bit: <code># cd /tmp && wget http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz</code>

32 bit: <code># cd /tmp && wget http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86.tar.gz</code>

Extract file yang sudah terdownload:  
<code># tar xfz ioncube_loaders_lin_*.gz</code>

Copy library ioncube yang diperuntukkan bagi php5.6 ke extension directory defaultnya    
<code># cp /tmp/ioncube/ioncube_loader_lin_5.6.so /usr/lib/php/20131226/</code>

Tambahkan ekstensi IonCube Loader ke php.ini yang digunakan di OS Anda, pada kelompok [PHP]. Sesuaikan pula max_excution_time agar aplikasi tidak mengalami timeout. Aplikasi juga membutuhkan loading extension pdo_pgsql agar dapat terkoneksi ke database.  
``` php
[PHP]

zend_extension = /usr/lib/php/20131226/ioncube_loader_lin_5.6.so

...

max_execution_time = 30000

extension=/usr/lib/php/20170718/pdo_pgsql.so
```

Pastikan untuk merestart webserver setelah langkah ini.

#### Instalasi eRapor SMK (diasumsikan menggunakan default virtualhost): 

<code>$ cd /var/www/html</code>

<code>$ git clone https://gitlab.com/eraporsmk/eraporsmk.git</code>

<code>$ mv -v eraporsmk/* /var/www/html</code>

<code>$ mv -v eraporsmk/.htaccess /var/www/html</code>

<code>$ git pull origin master</code>

<code>$ cd /var/www/html/application/config/</code>

<code>$ mv database.php.example database.php</code>

<code>$ nano database.php</code>

Edit bagian:

``` php?start_inline=1
'dsn'	=> 'pgsql:host=localhost;port=5432;dbname=your_database_name',
'hostname' => 'localhost:5432',
'username' => 'your_database_username',
'password' => 'your_database_password',
```
Sesuaikan **your_database_name**, **port** (jika perlu), **your_database_username** dan **your_database_password**, simpan, lalu akses dari web browser. Tunggu hingga proses migration benar-benar telah selesai dan tampil halaman untuk registrasi.

Sesuaikan konfigurasi VirtualHost di Apache, pada bagian directive <Directory>, tambahkan setting:

``` AllowOverride All```

setting ini dibutuhkan untuk membolehkan aplikasi eRapor SMK menggunakan .htaccess untuk keperluan redirection dan lain sebagainya.

**Catatan: meski versi yang terinstall nantinya adalah versi 4.1.0, aplikasi tetap dapat diperbarui hingga versi terbaru menggunakan fitur Cek Pembaharuan**